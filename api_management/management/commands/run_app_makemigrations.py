from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    help = 'Runs migrations for the apps configured at runtime'

    def handle(self, *args, **options):
        """
        Handle creation of super user
        :param args:
        :param options:
        :return:
        """
        apps = settings.INSTALLED_APPS
        assumed_app_labels = [app.split('.')[-1] for app in apps]
        self.stdout.write(
            '\n==== Running make migrations for apps:\n%s' % ' \n'.join(apps))
        self.stdout.write(
            '\n ==== Assuming app labels:\n%s'
            % '\n'.join(assumed_app_labels))
        call_command('makemigrations', *assumed_app_labels)
