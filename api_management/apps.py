from django.apps import AppConfig


class APIManagement(AppConfig):
    name = 'api_management'
    label = 'api_management'
