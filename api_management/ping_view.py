from django.conf import settings

from rest_framework.response import Response

from utils.api.base_view import BaseAuthView


class PingView(BaseAuthView):
    """
    Ping view to check if API is up
    """
    namespace = 'root'
    http_method_names = ['get']
    permission_classes = []

    def get(self, request):
        """
        Lists currently supported applications
        :param request: Request object
        :return: Response object
        """
        return Response()
