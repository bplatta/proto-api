from django.conf import settings
from django.test import tag
from django.urls import reverse
from rest_framework import status

from utils.testing.base_test_case import BaseAPIDBTestCase


@tag('db')
class TestRootViewTestCase(BaseAPIDBTestCase):
    """
    Test Root View operations
    """
    POST_FORMAT = 'json'
    view_namespace = 'root'

    @classmethod
    def root_endpoint(cls):
        return reverse(cls.view_namespace)

    def test_view_200_non_admin_anon(self):
        """
        Tests get request on root view for anon is empty
        """
        response = self.client.get(self.root_endpoint())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)

    def test_view_200_non_admin_authed(self):
        """
        Tests get request on root view for non admin is empty
        """
        user, creds = self.new_user()
        token = self.new_token(user)
        self.bearer_auth_client(token)
        response = self.client.get(self.root_endpoint())

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)

    def test_method_not_supported(self):
        """
        Tests Root method not supported request
        """
        user, creds = self.new_user()
        self.as_staff(user)
        response = self.client.post(self.root_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(response.data, {
            'detail': 'Method "POST" not allowed.'
        })
