from django.utils.encoding import force_text

from rest_framework import exceptions, status


class BaseAPIException(exceptions.APIException):
    """
    Base API Exception to override APIException default status_code (500)
    APIExceptions are handled by GenericAPIView.handle_exception
    """
    default_detail = ('Server error occurred', )
    default_status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

    def __init__(self, message=None, status_code=None):
        if status_code is not None:
            self.status_code = status_code
        else:
            self.status_code = self.default_status_code

        if message is not None:
            self.detail = {
                'error': force_text(message)
            }
        else:
            self.detail = {
                'error': force_text(self.default_detail)
            }

    def __str__(self):
        return self.detail['error']


class OperationNotSupported(BaseAPIException):
    """
    Operation not supported error - e.g. Read operation not supported (only
    list operation would be)
    """
    default_status_code = status.HTTP_400_BAD_REQUEST
    message = '%s operation not permitted on resource'

    def __init__(self, operation, opt_message=None):
        message = self.message % operation.capitalize()

        if opt_message:
            message = '%s. %s' % (message, opt_message.capitalize())

        super(OperationNotSupported, self).__init__(message=message)


class ExceptionMessage(str):
    ObjectNotFound = '%s with ID `%s` does not exist'
    EndpointNotFound = 'Endpoint does not exist'
