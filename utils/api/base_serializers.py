from rest_framework import serializers

from utils.api.validators import UpdateFieldsValidator


class BasePostSerializer(serializers.Serializer):
    """
    Serialize data for FORUM creation
    """

    # Populate with fields that are valid on Update calls
    # and an optional message for the validation error:
    # e.g. ['username'], 'Please try operation for X instead'
    invalid_update_fields = []
    invalid_update_message = None
    # Any custom validators for particular fields
    # see utils.api.validators
    custom_validators = []

    def get_parent_instance_from_view(self):
        """
        Helper for retrieving parent instance from the view
        """
        return self.context['view'].parent_instance

    def __init__(self, *args, **kwargs):

        self.validate_after = []
        self.validate_before = []

        if len(self.invalid_update_fields) > 0:
            self.custom_validators.append(UpdateFieldsValidator(
                invalid_fields=self.invalid_update_fields,
                optional_message=self.invalid_update_message
            ))

        for validator in self.custom_validators:
            if getattr(validator, 'before', False):
                self.validate_before.append(validator)
            else:
                self.validate_after.append(validator)

        super(BasePostSerializer, self).__init__(*args, **kwargs)

    def run_validators(self, value):
        """
        Override run_validators to hookin custom validators BEFORE
        :param value:
        :return:
        """
        self._run_custom_validators(self.validate_before, value)
        super(BasePostSerializer, self).run_validators(value)
        self._run_custom_validators(self.validate_after, value)

    def _run_custom_validators(self, validators, data):
        """
        Runs custom validators list against data
        :param data: {}
        :return: None or Raises validation error
        """
        for validator in validators:
            validator(data, instance=self.instance)
