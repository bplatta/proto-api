from django.http import Http404
from rest_framework import mixins, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from utils.exceptions import BaseAPIException, ExceptionMessage


def extends_base_view(obj):
    """
    Helper function for determing if class is based on the APIAllview
    :param obj: Object
    :return: Boolean
    """
    return issubclass(obj, APIAllView)


class BaseAuthView(GenericAPIView):
    """
    View used in conjunction with api_auth.middleware
    """
    def admin_from_webserver(self):
        """
        True if request was made by an admin from a priviledged webserver
        :return: Bool
        """
        return self.request.user.is_staff and self.request.webserver_referred

    def perform_authentication(self, request):
        """
        Call authorize method defined by middleware.
        :param request: Request object. Mutates request object and sets
            request.user = APIUser or AnonymousUser
        """
        user, token, api_key = request.authorize()

        setattr(request, 'user', user)
        if token is not None:
            setattr(request, 'authed_token', token)
        elif api_key is not None:
            setattr(request, 'api_key', api_key)


class APIAllView(
    BaseAuthView,
    mixins.ListModelMixin,   # defines a self.list method
    mixins.RetrieveModelMixin,  # defines a self.retrieve method
    mixins.CreateModelMixin,  # defines a self.create method
    mixins.UpdateModelMixin  # defines a self.update method
):
    """
    Generic Collection view for API
    """
    main_model = None
    # set kwarg to use as object lookup by self.get_object e.g. forum_id
    lookup_url_kwarg = None

    # Default operations - determines some view / test generation behavior
    http_method_names = ['get', 'post', 'delete']
    supported_operations = ['read', 'list', 'create', 'delete', 'delete']

    # Set pagination for endpoint
    # pagination_class_var = None

    # To be used by the utils.mixins.SubcollectionMixin if used
    parent_lookup_field = 'pk'
    parent_resource__model = None
    parent_lookup_url_kwarg = None
    parent_permission_classes = ()

    # utilized in url routing
    namespace = {
        'collection': None,
        'resource': None
    }

    # Allow different serializers per HTTP methods and whether
    # operation is performed on collection or a resource.
    # Set whole DICT:
    serializer_classes = None
    # Set with the following format
    #   {
    #       'post': {
    #           'default': None,
    #           'collection': None,  # CREATE operation
    #           'resource': None  # UPDATE operation
    #       },
    #       'get': {
    #           'default': None,
    #           'collection': None,  # LIST operation
    #           'resource': None  # READ operation
    #       }, ...
    #   }

    def get_serializer_class(self):
        """
        Override get_serializer_class for custom serializers
        """
        # Default to using cls.GenericAPIView serializer class if provided
        if self.serializer_class is not None:
            return self.serializer_class

        request_method = self.request.method.lower()

        # Assert method set on serializer_classes var
        assert (self.serializer_classes and
                request_method in self.serializer_classes), (
            "'%s' should provided method serializer class "
            "in self.serializer_classes['%s']" % (
                self.__class__.__name__, self.request.method.lower())
        )

        # Assert there is a default serializer for method
        assert self.serializer_classes[
                   request_method].get('default') is not None, (
                    "'%s' should provided default serializer class "
                    "in self.serializer_classes['%s']" % (
                        self.__class__.__name__, self.request.method.lower())
                )

        is_for_resource = (
            False if not self.lookup_url_kwarg else
            self.kwargs.get(self.lookup_url_kwarg) is not None
        )

        serializer_type = 'resource' if is_for_resource else 'collection'

        return self.serializer_classes[request_method].get(
            serializer_type, self.serializer_classes[request_method]['default']
        )

    @classmethod
    def lookup_id_in_args(cls, kwargs):
        return (cls.lookup_url_kwarg in kwargs and
                kwargs.get(cls.lookup_url_kwarg) is not None)

    def check_object_permissions(self, request, obj):
        """
        Check if the request should be permitted for a given object.
        Raises an appropriate exception if the request is not permitted.
        """
        for permission in self.get_permissions():
            if not permission.has_object_permission(request, self, obj):
                self.permission_denied(
                    request, message=getattr(permission, 'message', None)
                )

    def check_parent_object_permissions(self, request, obj):
        """
        Run object check on resource for parent collection
        :param request: django Request object
        :param obj: Model object
        :return: None or raises
        """

        # Check any permissions for the parent collection
        parent_permissions = [
            p_class() for p_class in self.parent_permission_classes]

        for permission in parent_permissions:
            if not permission.has_object_permission(request, self, obj):
                self.permission_denied(
                    request, message=getattr(permission, 'message', None)
                )

    def _handle_get_404(self):
        """
        Provide better 404 message for getobject fail
        """
        cln_resource_name = 'Object'
        obj_id = self.kwargs.get(self.lookup_url_kwarg)
        if self.lookup_url_kwarg and type(self.lookup_url_kwarg) is str:
            cln_resource_name_parts = self.lookup_url_kwarg.split(
                '_')
            if len(cln_resource_name_parts) == 1:
                cln_resource_name = ''.join(
                    cln_resource_name_parts).capitalize()
            else:
                cln_resource_name = ' '.join(
                    cln_resource_name_parts[:-1]).capitalize()

        raise BaseAPIException(
            status_code=status.HTTP_404_NOT_FOUND,
            message=ExceptionMessage.ObjectNotFound % (
                cln_resource_name, obj_id))

    def get_object(self):
        """
        Wrap get_object to provide better exception messaging
        """
        try:
            obj = super(APIAllView, self).get_object()
            self.check_parent_object_permissions(self.request, obj)
            return obj
        except Http404:
            self._handle_get_404()

    def initial(self, request, *args, **kwargs):
        """
        Override APIView.initial to hook in any other validation on request

        This is called in APIView.dispatch() method and any
        exceptions thrown will be handled by APIView.handle_exception()
        """

        # Call perform_authentication on GenericAPIView cls first
        super(APIAllView, self).initial(request, *args, **kwargs)

        # Check if View is sub-collection (mixins.SubCollectionMixin)
        # Sets parent_instance on view and checks parent permissions
        if hasattr(self, 'validate_parent_instance'):

            self.validate_parent_instance(**kwargs)

            # Check any permissions for the parent collection
            parent_permissions = [
                p_class() for p_class in self.parent_permission_classes]

            for permission in parent_permissions:
                if not permission.has_permission(request, self):
                    self.permission_denied(
                        request, message=getattr(permission, 'message', None)
                    )

    def _get_all_queryset(self):
        """
        Default get_queryset implementation
        :return: Queryset
        """
        return self.main_model.objects.all()

    def _get(self, request, **kwargs):
        """
        Default implementation of GET request
        """
        if self.lookup_id_in_args(kwargs):
            return self.retrieve(request, **kwargs)

        else:
            return self.list(request)

    def _post(self, request, **kwargs):
        """
        Default implementation of POST request
        """
        if self.lookup_id_in_args(kwargs):
            return self.partial_update(request, **kwargs)
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)

            # Uniformly return ID of created object
            return Response({
                'id': getattr(serializer.instance, self.lookup_field)
            }, status=status.HTTP_201_CREATED, headers=headers)

    def _delete(self, request, **kwargs):
        """
        Default implementation of DELETE request
        """
        if not self.lookup_id_in_args(kwargs):
            raise BaseAPIException(
                status_code=status.HTTP_404_NOT_FOUND,
                message='No {} was provided.'.format(self.lookup_url_kwarg))

        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
