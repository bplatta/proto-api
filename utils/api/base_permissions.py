class BaseRequestFacts(object):
    """
    Class housing facts about an endpoint's collection and request
    """

    def __init__(self, request, view, obj=None, is_resource_owner=None):
        self.request = request
        self.view = view
        self.resource = obj
        self.parent = getattr(view, 'parent_instance', None)
        self.is_sub_collection = self.parent is not None
        self._is_resource_owner = (
            is_resource_owner or self.default_is_resource_owner)
        self.user = request.user

    def is_admin(self):
        return self.user.is_staff

    @staticmethod
    def default_is_resource_owner(user, resource):
        return user == resource.user
