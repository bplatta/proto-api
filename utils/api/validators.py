from functools import partial

from rest_framework.serializers import ValidationError
from rest_framework.validators import UniqueTogetherValidator


class UpdateFieldsValidator(object):
    """
    Validate POST fields if sub-selection are allowed for update operation
    """
    # Run validation BEFORE regular validation flow
    before = True

    def __init__(self, invalid_fields, optional_message=None):
        self.validate_fields = partial(
            self.check_for_invalid_fields, invalid_fields)

        self.opt_message = (
            '' if not optional_message
            else ' %s' % optional_message.capitalize())

    @staticmethod
    def check_for_invalid_fields(known_invalid_fields, data):
        """
        Check if configured with invalid fields list
        :param known_invalid_fields: [<str>] of field names
        :param data: {}
        :return: [<str>]
        """

        invalid_fields = []
        for field in known_invalid_fields:
            if field in data:
                invalid_fields.append(field)

        return invalid_fields

    def __call__(self, data, instance=None):

        if not instance:
            return None

        invalid_fields = self.validate_fields(data)

        if len(invalid_fields) > 0:
            raise ValidationError({
                field_name: (
                    'Invalid field for update operation.%s' % self.opt_message
                )
                for field_name in invalid_fields
            })


class PivotConditionValidator(object):
    """
    Simple condition validator that can be sub-classed
    """
    for_field = None

    def __init__(self, if_func=None, then_func=None):
        # Validator: if pivot, then validate
        self.pivot = if_func or self._if
        self.then_validate = then_func or self._then

    @classmethod
    def _if(cls, data):
        """
        Expects data {}
        :param data: {}
        :return: Boolean
        """
        return (
            cls.for_field in data
            if cls.for_field is not None
            else False)

    @staticmethod
    def _then(data):
        """
        Expects data {}
        :param data: {}
        :return: None or raise
        """
        return None

    def __call__(self, data, **kwargs):

        if self.pivot(data) is True:
            self.then_validate(data)
