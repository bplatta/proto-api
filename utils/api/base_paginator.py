from collections import OrderedDict
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


def pagination_for_collection(collection_name):
    """
    Builds pagination class for a collection
    :param collection_name: str
    :return: PageNumberPagination Cls
    """

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next_page', self.get_next_link()),
            ('previous_page', self.get_previous_link()),
            (collection_name, data)
        ]))

    return type(
        '%sPagination' % collection_name.capitalize(),
        (PageNumberPagination,), {
            'get_paginated_response': get_paginated_response
        })
