import logging
from random import choice

logger = logging.getLogger(__name__)


class TestCaseDataManager(object):
    """
    Mixin for caching and deleting data for tests
    """
    def __init__(self, cache_settings={}):
        """
        Initialize manager with cache structure
        :param cache_settings: {}
            {
                keyname<str>: <type>
            }

            e.g. { 'forum': list }

            creates getters and setters:
                get_{keyname} and set_{keyname}
        """
        self._objects_to_delete = []
        self._cache = {}

        for key, d_type in cache_settings.items():
            self.new_cache_key(key, d_type)

    @property
    def cache(self):
        return self._cache

    def queue_for_delete(self, obj):
        self._objects_to_delete.append(obj)

    @staticmethod
    def list_set(cache):
        def set(data):
            cache.append(data)
        return set

    @staticmethod
    def dict_set(cache):
        def set(k, data):
            cache[k] = data
        return set

    @staticmethod
    def dict_get(cache):
        def get(k, default=None):
            return cache.get(k, default)
        return get

    @staticmethod
    def list_get(cache):
        def get(index=None):
            if index:
                return cache[index]

            if len(cache) == 0:
                return None
            return choice(cache)

        return get

    def cleanup_data(self):
        logger.info(
            '=== Cleaning up %s objects ...' %
            str(len(self._objects_to_delete)))
        for o in self._objects_to_delete:
            try:
                o.delete()
            except AssertionError as e:
                logger.info(
                    'Failed to delete `%s` instance: %s' % (type(o), str(e)))

    def new_cache_key(self, k, d_type):
        key = k.lower()
        self._cache[k] = d_type()
        cache = self._cache[key]
        if d_type == list:
            setattr(self, 'set_%s' % key, self.list_set(cache))
            setattr(self, 'get_%s' % key, self.list_get(cache))
        # assume its a dict
        else:
            setattr(self, 'set_%s' % key, self.dict_set(cache))
            setattr(self, 'get_%s' % key, self.dict_get(cache))

        setattr(self, '%s_cache' % key, lambda: cache)

    def count(self, k):
        return len(self._cache[k])
