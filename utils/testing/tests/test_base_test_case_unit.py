import unittest
from django.test import tag
from rest_framework import serializers

from utils.api.base_view import APIAllView
from utils.testing.base_test_case import BaseAPIAllViewTestCase


@tag('unit')
class TestPostGenerationTestCase(unittest.TestCase):
    """
    Test the testing base class for test generation
    """

    def test_case_class_validation_success(self):
        """
        Verifies a valid BaseViewTestCaseOverrideSettings class is validated
        """
        view_under_test = type('FakeView', (APIAllView,), {})
        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        try:
            TestCase.validate()
        except RuntimeError as e:
            self.fail('Valid TestCase failed validation! %s' % e)

    def test_case_class_validation_missing_req_fields(self):
        """
        Verify TestCase class validation fails if required fields missing
        """
        TestCase = type('NewTestCase', (BaseAPIAllViewTestCase,), {})

        with self.assertRaises(RuntimeError):
            TestCase.validate()

    def test_case_class_validation_view_incorrect_type(self):
        """
        Verify TestCase class validation fails if view not derived from base
        """
        class NotGonnaDoIt:
            pass

        TestCase = type('NewTestCase', (BaseAPIAllViewTestCase,), {
            'view_under_test': NotGonnaDoIt
        })

        with self.assertRaises(RuntimeError):
            TestCase.validate()

    def test_base_class_is_sub_collection(self):
        """
        Tests is sub collection method returns correct boolean
        """
        view_under_test = type('FakeView', (APIAllView,), {
            'parent_resource__model': 1
        })

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        test_case = TestCase()
        self.assertTrue(test_case.is_sub_collection)

    def test_base_class_resource_name(self):
        """
        Tests is sub collection method returns correct boolean
        """
        view_under_test = type('FakeView', (APIAllView,), {
            'namespace': {
                'resource': 'abc-def'
            }
        })

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        test_case = TestCase()
        self.assertEqual(test_case.resource_name, 'Abc def')

    def test_get_serializer_fields(self):
        """
        Test retrieval of serializer fields for view
        """
        class FakeSerializer(serializers.Serializer):
            field_char = serializers.CharField(max_length=10)
            field_int = serializers.IntegerField()

        view_under_test = type('FakeView', (APIAllView,), {
           'serializer_classes': {
                'get': {
                    'default': FakeSerializer
                }
            }
        })

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        test_case = TestCase()
        field_data = test_case.serializer_fields('get')
        self.assertIn('collection', field_data)
        self.assertIn('resource', field_data)

        fields = [
            ('field_int', serializers.IntegerField),
            ('field_char', serializers.CharField)
        ]

        for target in ['resource', 'collection']:
            for field_name, field_type in fields:
                self.assertIn(field_name, field_data[target])
                self.assertIsInstance(
                    field_data[target][field_name], field_type)

    def test_get_resource_uid(self):
        """
        Test retrieve resource uid from data with configured view
        """
        view_under_test = type('FakeView', (APIAllView,), {
            'lookup_field': 'uid'
        })

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        test_case = TestCase()
        expected_uid = '1234'
        data = {
            'uid': expected_uid
        }
        self.assertEqual(test_case.get_resource_uid(data), expected_uid)

    def test_get_parent_uid(self):
        """
        Test retrieve parent uid from data with configured view
        """
        view_under_test = type('FakeView', (APIAllView,), {
            'parent_lookup_field': 'uid'
        })

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        test_case = TestCase()
        expected_uid = '1234'
        data = {
            'uid': expected_uid
        }
        self.assertEqual(test_case.get_parent_uid(data), expected_uid)

    def test_endpoint_url_args_with_data_only(self):
        """
        Test correctly configures kwargs dict with data
        """
        api_view_settings = {
            'lookup_url_kwarg': 'daughter_id',
            'parent_lookup_url_kwarg': 'parent_id',
            'lookup_field': 'uidlookie',
            'parent_lookup_field': 'uidmeto'
        }
        view_under_test = type('FakeView', (APIAllView,), api_view_settings)

        TestCase = type(
            'NewTestCase',
            (BaseAPIAllViewTestCase,),
            {
                'view_under_test': view_under_test
            }
        )

        resource_id = '123'
        resource_data = {
            api_view_settings['lookup_field']: resource_id
        }
        parent_id = '456'
        parent_data = {
            api_view_settings['parent_lookup_field']: parent_id
        }
        expected_kwargs = {
            api_view_settings['lookup_url_kwarg']: resource_id,
            api_view_settings['parent_lookup_url_kwarg']: parent_id
        }

        test_case = TestCase()

        self.assertEqual(test_case.get_endpoint_url_args(
            collection_data=parent_data,
            resource_data=resource_data
        ), expected_kwargs)
