import unittest
from django.test import tag

from utils.testing.utils import Randomizer, get_serializers


@tag('unit')
class UtilsFuncsTestCase(unittest.TestCase):
    """
    Test testing utils functions
    """
    def test_get_serializers_from_view(self):
        """
        Test successful parsing of serializers from view
        """
        method = 'somefrigginmethod'
        def_cls = 'whatdoesitmatter'
        coll_cls = 'lifeismeaningless'
        res_cls = 'comewatchtv'
        serializers = {
            method: {
                'default': def_cls,
                'collection': coll_cls,
                'resource': res_cls
            }
        }

        fake_view = type('SHhhhView', (), {
            'serializer_classes': serializers
        })

        got_classes = get_serializers(method, fake_view)
        self.assertEqual(got_classes['collection'], coll_cls)
        self.assertEqual(got_classes['resource'], res_cls)

    def test_get_serializers_from_view_with_default(self):
        """
        Test successful parsing of serializers from view with defaults
        """
        method = 'somefrigginmethod'
        def_cls = 'whatdoesitmatter'
        res_cls = 'comewatchtv'
        serializers = {
            method: {
                'default': def_cls,
                'resource': res_cls
            }
        }

        fake_view = type('SHhhhView', (), {
            'serializer_classes': serializers
        })

        got_classes = get_serializers(method, fake_view)
        self.assertEqual(got_classes['collection'], def_cls)
        self.assertEqual(got_classes['resource'], res_cls)


@tag('unit')
class TestRandomizerTestCase(unittest.TestCase):
    """
    Test Case for testing randomizer object functions
    """
    randomizer = Randomizer()

    def test_randomizer_generate_str_field(self):
        """
        Tests randomizer can generate a string field
        """
        field_type = str
        self.assertTrue(
            type(self.randomizer.generate_field(field_type)) is field_type,
            'Generated field not of expected type `%s`' % field_type)

    def test_randomizer_generate_int_field(self):
        """
        Tests randomizer can generate a int field
        """
        field_type = int
        self.assertTrue(
            type(self.randomizer.generate_field(field_type)) is field_type,
            'Generated field not of expected type `%s`' % field_type)

    def test_randomizer_generate_email_field(self):
        """
        Tests randomizer can generate a email field
        """
        field_type = 'email'
        expected_type = str
        generated_field = self.randomizer.generate_field(field_type)
        self.assertTrue(
            type(generated_field) is expected_type,
            'Generated field not of expected type `%s`' % expected_type)
        self.assertIn('@', generated_field)

    def test_randomizer_generate_url_field(self):
        """
        Tests randomizer can generate a string field
        """
        field_type = 'url'
        expected_type = str
        generated_field = self.randomizer.generate_field(field_type)
        self.assertTrue(
            type(generated_field) is expected_type,
            'Generated field not of expected type `%s`' % expected_type)
        self.assertIn('http', generated_field)
        self.assertIn('.com', generated_field)

    def test_randomizer_generate_bool_field(self):
        """
        Tests randomizer can generate a string field
        """
        field_type = bool
        self.assertTrue(
            type(self.randomizer.generate_field(field_type)) is field_type,
            'Generated field not of expected type `%s`' % field_type)
