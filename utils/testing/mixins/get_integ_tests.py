from rest_framework import status


class GetIntegTestsMixin(object):
    """
    Some basic get tests

    Requires Mixin with
        utils.testing.base_test_case.BaseAPIAllViewTestCase
    """
    @property
    def collection_fields(self):
        return self.serializer_fields('get')['collection']

    @property
    def resource_fields(self):
        return self.serializer_fields('get')['resource']

    def validate_fields_in_data(self, fields, data):
        """
        Basic containment assertion on data given fields
        :param fields:
        :param data:
        """
        for field, field_cls in fields.items():
            # Assert field in returned data
            self.assertIn(field, data)

    def test_read_resource_does_not_exist(self):
        """
        Test GET on resource that DNE returns 404
        """
        dne_id = 11111111
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=self.create_parent_resource(
                    **{'user': self.user}),
                resource_uid=dne_id))
        response = client.get(endpoint)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {
            "error": "%s with ID `%s` does not exist" % (
                self.resource_name, dne_id)
        })

    def test_read_resource_valid(self):
        """
        Test GET on resource succeeds with 200
        """
        resource_data = self.create_resources(**{'user': self.user})
        resource, parent = resource_data[0]
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=parent, resource_data=resource))
        response = client.get(endpoint)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.validate_fields_in_data(self.resource_fields, response.data)

    def test_list_collection_valid(self):
        """
        Test GET on collection for list operation returns 200
        """
        min_count = 3
        create_kwargs = {'user': self.user, 'count': min_count}
        resource_data = self.create_resources(**create_kwargs)
        resource, parent = resource_data[0]
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(collection_data=parent))
        response = client.get(endpoint)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Just need to validate the first result
        self.validate_fields_in_data(
            self.collection_fields, response.data['results'][0])
        self.assertGreaterEqual(response.data['count'], min_count)
