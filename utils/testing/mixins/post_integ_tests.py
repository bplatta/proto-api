from rest_framework import status


class PostIntegTestsMixin(object):
    """
    Some basic post tests

    Requires Mixin with
        utils.testing.base_test_case.BaseAPIAllViewTestCase
    """

    POST_FORMAT = 'json'

    def generate_post_args(self):
        raise NotImplementedError(
            'generate_post_args is not implemented for PostIntegTest case')

    def test_update_resource_does_not_exist(self):
        """
        Test POST on resource that does not exist 404's
        """
        dne_id = 111111111
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=self.create_parent_resource(
                    **{'user': self.user}),
                resource_uid=dne_id
            ))
        response = client.post(endpoint, data={}, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_resource_valid_params(self):
        """
        Test a POST create with valid data returns 201
        """
        post_args = self.generate_post_args()
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=self.create_parent_resource(
                    **{'user': self.user})))
        response = client.post(
            endpoint, data=post_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)

    def test_create_resource_parent_does_not_exist(self):
        """
        Test a POST create with valid data returns 201
        """
        # Only run this test if the view is for a sub-collection
        if not self.is_sub_collection:
            return True

        parent_uid_dne = 111111111
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_uid=parent_uid_dne))
        response = client.post(
            endpoint, data={}, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, '')

    def _test_create_invalid_param(self, field_name, field_cls):
        """
        Helper function for running a test on creating with invalid param
        :param field_name: string
        :param field_cls: serializer Field
        """
        post_args = self.generate_post_args()
        post_args[field_name] = self.randomizer.generate_from_serializer_field(
            field_cls, invalid=True)
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=self.create_parent_resource(
                    **{'user': self.user})))
        response = client.post(
            endpoint, data=post_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST,
            response.data)
        self.assertIn(field_name, response.data)

    def _test_update_invalid_param(self, field_name, field_cls):
        """
        Helper function for running a test on updating an invalid param
        :param field_name: string
        :param field_cls: serializer Field
        """
        bad_arg = {
            field_name: self.randomizer.generate_from_serializer_field(
                field_cls, invalid=True)
        }
        resource_data = self.create_resources(**{'user': self.user})
        resource, parent = resource_data[0]
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=parent, resource_data=resource))
        response = client.post(endpoint, data=bad_arg, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn(field_name, response.data)

    def _test_update_valid_param(self, field_name, field_cls):
        """
        Helper function for running a test on updating with valid param
        :param field_name: string
        :param field_cls: serializer Field
        """
        new_arg = {
            field_name: self.randomizer.generate_from_serializer_field(
                field_cls)
        }
        resource_data = self.create_resources(**{'user': self.user})
        resource, parent = resource_data[0]
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=parent, resource_data=resource))
        response = client.post(endpoint, data=new_arg, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
