from rest_framework import status


class DeleteIntegTestsMixin(object):
    """
    Some basic deletion tests

    Requires Mixin with
        utils.testing.base_test_case.BaseAPIAllViewTestCase
    """

    def test_delete_valid_request(self):
        """
        Test valid deletion of of resource
        """
        resource_data = self.create_resources(**{
            'user': self.user, 'to_delete': True
        })
        resource, parent = resource_data[0]
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=parent, resource_data=resource))
        response = client.delete(endpoint)

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)

        # Verify resource is not in database
        self.assertFalse(
            self.view_under_test.main_model.objects.filter(**{
                self.view_under_test.lookup_field: self.get_resource_uid(
                    resource)
            }).exists())

    def test_delete_resource_does_not_exist(self):
        """
        Test for issuing DELETE request on nonexistent resource
        :param self: test_case
        """
        bad_id = 11111111
        client = self.bearer_auth_client(self.token)
        endpoint = self.get_endpoint(
            self.get_endpoint_url_args(
                collection_data=self.create_parent_resource(
                    **{'user': self.user}),
                resource_uid=bad_id
            ))
        response = client.delete(endpoint)

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, {
            "error": "%s with ID `%s` does not exist" % (
                self.resource_name, bad_id)
        })
