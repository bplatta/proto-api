from random import randint, choice
from uuid import uuid4

from rest_framework.serializers import (
    CharField, ChoiceField, IntegerField, EmailField, URLField, BooleanField)


class Randomizer:
    """
    Helper class for generating types of vars
    """
    @classmethod
    def generate_params_from_fields(cls, fields_dict):
        """
        Generates random values for each field in dict
        :param fields_dict: { field_name : Field class }
        :return: { field_name : random value }
        """
        generated_params = {}
        for field_name, field_class in fields_dict.items():
            generated_params[field_name] = cls.generate_from_serializer_field(
                field_class)
        return generated_params

    @classmethod
    def generate_field(cls, field_type, invalid=False, **kwargs):
        """
        Generates random values from python data types
        :param field_type: python data type or string
        :param invalid: Boolean
        :param kwargs: optional {} to pass to value generator
        :return: field value
        """

        if field_type == str:
            return cls._char(invalid=invalid)
        elif field_type == int:
            return cls._integer(invalid=invalid, **kwargs)
        elif field_type == 'email':
            return cls._email(invalid=invalid)
        elif field_type == 'url':
            return cls._url(invalid=invalid)
        elif field_type == bool:
            return cls._bool(invalid=invalid)
        else:
            raise RuntimeError(
                'Field generation for type %s not implemented' % field_type)

    @classmethod
    def generate_from_serializer_field(cls, field, invalid=False):
        field_type = type(field)

        if field_type is CharField:
            return cls._char(invalid=invalid)
        elif field_type is EmailField:
            return cls._email(invalid=invalid)
        elif field_type is IntegerField:
            return cls._integer(invalid=invalid)
        elif field_type is ChoiceField:
            return (
                choice(list(field.choices))
                if not invalid else 'defnotavalidchoice'
            )
        elif field_type is URLField:
            return cls._url(invalid=invalid)
        elif field_type is BooleanField:
            return cls._bool(invalid=invalid)
        else:
            raise RuntimeError(
                'Testing suite not set up to support '
                'fields of type `%s` yet' % field_type
            )

    @classmethod
    def _bool(cls, invalid=False):
        return cls._random_string() if invalid else choice([True, False])

    @classmethod
    def _url(cls, invalid=False):
        return (
            cls._random_string() if invalid
            else 'https://%s.com' % cls._random_string()
        )

    @classmethod
    def _email(cls, invalid=False):
        return (
            '%s@gmail.com' % cls._random_string()
            if not invalid else cls._random_string()
        )

    @classmethod
    def _char(cls, invalid=False):
        return cls._random_string() if not invalid else 1111

    @staticmethod
    def _random_string():
        return str(uuid4())[:13]

    @staticmethod
    def _integer(invalid=False, min_value=0, max_value=10000):
        return (
            randint(min_value, max_value)
            if not invalid else 'thiswontflymarty')
