import base64
import logging

from rest_framework.test import APIClient, APIRequestFactory

from utils.testing.utils import (
    generate_user, generate_token, generate_api_key)
from utils.testing.test_data_manager import TestCaseDataManager

logger = logging.getLogger(__name__)


class TestCaseAuthManager(object):
    """
    Common TestCase functionality for creating and authing users
    """
    request_factory = APIRequestFactory()

    auth_data_manager = TestCaseDataManager()

    @classmethod
    def cleanup_auth(cls):
        logger.info('\n= Cleaning up Auth Data =')
        cls.auth_data_manager.cleanup_data()

    @staticmethod
    def _bearer_auth_header(token):
        """
        Constructs Bearer Auth header
        :param token: Token object or str
        :return: {}
        """
        return {
            'HTTP_AUTHORIZATION': 'Bearer %s' % (
                token if type(token) is str else token.key)
        }

    @staticmethod
    def _basic_auth_header(api_key):
        """
        Constructs Basic Auth header
        :param api_key: ApiAppKey object or str
        :return: {}
        """
        api_key_is_str = type(api_key) is str

        creds = base64.b64encode(
            (api_key if api_key_is_str else api_key.key).encode()
        ).decode()

        return {'HTTP_AUTHORIZATION': 'Basic %s' % creds}

    def new_request(
            self, route, method='get', auth_header=None, extra_headers=None):
        """
        Helper method for creating requests with factor
        :param route: /this/route/ etc
        :param method: POST, GET etc
        :param auth_header: optional {} of Authentication headers
        :param extra_headers: optional {} of extra headers
        :return: request object
        """
        headers = auth_header or {}
        if extra_headers is not None:
            headers.update(extra_headers)
        # retrieve method from factory and pass request params
        return getattr(self.request_factory, method.lower())(route, **headers)

    ########################
    # Auth Data generators #
    ########################

    @classmethod
    def new_user(cls):
        user, creds = generate_user()
        cls.auth_data_manager.queue_for_delete(user)
        return user, creds

    @classmethod
    def new_token(cls, user):
        token = generate_token(user)
        cls.auth_data_manager.queue_for_delete(token)
        return token

    @classmethod
    def new_api_key(cls, user):
        api_key = generate_api_key(user)
        cls.auth_data_manager.queue_for_delete(api_key)
        return api_key

    ########################
    # Test Context Methods #
    ########################

    def reset_user(self, user):
        """
        Resets user to active and non staff
        :param user: User object
        :return: User object
        """
        self.as_non_staff(
            self.as_active_user(
                user
            )
        )

    @staticmethod
    def as_staff(user):
        """
        Helper function that updates user to admin status
        :param user: User object
        :return: User object
        """
        if user.is_staff:
            return user

        user.is_staff = True
        user.save()
        return user

    @staticmethod
    def as_non_staff(user):
        """
        Helper function that updates user to admin status
        :param user: User object
        :return: User object
        """
        if not user.is_staff:
            return user

        user.is_staff = False
        user.save()
        return user

    @staticmethod
    def as_active_user(user):
        """
        Helper function that updates user to admin status
        :param user: User object
        :return: User object
        """
        if user.is_active:
            return user

        user.is_active = True
        user.save()
        return user

    @staticmethod
    def as_inactive_user(user):
        """
        Helper function that updates user to admin status
        :param user: User object
        :return: User object
        """
        if not user.is_active:
            return user

        user.is_active = False
        user.save()
        return user

    # Manage Authentication Headers #
    def bearer_auth_client(self, token_or_str):
        """
        Sets client with proper Auth headerAPIClient
        :param token_or_str: Token Object with key or string
        :return: test Client
        """
        self.client = APIClient(**self._bearer_auth_header(token_or_str))
        return self.client

    def basic_auth_client(self, api_key, user_uid=None):
        """
        Sets client with proper Auth header
        :param api_key: ApiAppKey Object
        :param user_uid: str of user uid
        :return: test Client
        """
        self.client = APIClient(**self._basic_auth_header(api_key))
        return self.client

    def de_auth_client(self, client):
        """
        Clears client credentials
        :param client: django test Client
        :return: django test Client
        """
        # Set header defaults to None
        client.defaults = {}
        return client
