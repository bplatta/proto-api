from users.models import APIUser
from api_auth.models import ExpiringToken, ApiAppKey
from utils.testing.randomizer import Randomizer

randomizer = Randomizer()


def generate_user():
    """
    Helper function for generating api user
    :return:
    """
    creds = {
        'email': randomizer.generate_field('email'),
        'first_name': randomizer.generate_field(str),
        'last_name': randomizer.generate_field(str),
        'password': randomizer.generate_field(str)
    }

    APIUser.objects.create_user(**creds)

    # Have to re-retrieve because the id is set to None
    return APIUser.objects.get(email=creds['email']), creds


def generate_token(user):
    """
    Generates an expiring token
    :param user: auth model api user
    :return: Expriring Token
    """
    return ExpiringToken.objects.create(user=user)


def generate_api_key(user):
    """
    Generates an API App Key
    :param user: auth model api user
    :return: ApiAppKey
    """
    return ApiAppKey.objects.create(user=user)


def get_serializers(method, view):
    """
    Helper method for retrieving relevant serializers
    :param method: get, post, delete, etc.
    :param view: APIView
    :return: {}
    """
    default_serializer = view.serializer_classes[method].get('default')
    return {
        'collection': view.serializer_classes[method].get(
            'collection', default_serializer),
        'resource': view.serializer_classes[method].get(
            'resource', default_serializer)
    }


def get_serializer_fields(method, view):
    """
    Helper method for retrieving relevant serializer fields
    :param method: get, post, delete, etc.
    :param view: APIView
    :return: {}
    """
    return {
        resource_type: serializer().fields
        for resource_type, serializer in
        get_serializers(method, view).items()
    }
