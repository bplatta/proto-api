from django.urls import reverse
from django.http import QueryDict
from rest_framework.test import APISimpleTestCase

from utils.testing.utils import get_serializer_fields
from utils.api.base_view import extends_base_view
from utils.testing.test_auth_manager import TestCaseAuthManager


class BaseAPIDBTestCase(APISimpleTestCase, TestCaseAuthManager):
    """
    Base Test Case for APIViews
    """
    allow_database_queries = True

    def setUp(self):
        """
        Resets user for test
        """
        self.reset_user(self.user)

    @classmethod
    def setUpClass(cls):
        """
        Provide initial Auth objects
        """
        cls.user, cls.creds = cls.new_user()
        cls.token = cls.new_token(cls.user)
        cls.api_key = cls.new_api_key(cls.user)
        super(BaseAPIDBTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.cleanup_auth()
        super(BaseAPIDBTestCase, cls).tearDownClass()


class BaseAPIAllViewTestCase(APISimpleTestCase, TestCaseAuthManager):
    """
    Base test case for proto-api views. Currently the only added functionality
    with this test case is overridden settings. This class can be used
    as the base for tests.

    This was designed to be used with utils.testing.mixins classes
    """

    # Allow talking to database
    allow_database_queries = True

    # View under test. Expected to be a utils.api.base_view.APIAllView
    view_under_test = None

    # Namespace for django reverse
    app_namespace = None

    def setUp(self):
        """
        Resets user for test (from AuthTestCaseHelpers)
        """
        self.reset_user(self.user)

    @classmethod
    def setUpClass(cls):
        """
        Perform some basic class validation
        """
        cls.validate()
        cls.user, cls.creds = cls.new_user()
        cls.token = cls.new_token(cls.user)
        cls.api_key = cls.new_api_key(cls.user)
        super(BaseAPIAllViewTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.cleanup_auth()
        super(BaseAPIAllViewTestCase, cls).tearDownClass()

    @classmethod
    def validate(cls):
        """
        Validates the cls is defined with the expected class variables that
        will be used with the decorators
        :param cls: APITestCase class
        :return: None or raises
        """
        short_circuit_field = 'has_req_fields'

        if getattr(cls, short_circuit_field, False) is True:
            return cls

        required_view_field = 'view_under_test'

        if (not hasattr(cls, required_view_field) or
                not getattr(cls, required_view_field)):
            raise RuntimeError(
                'TestCase cls should define `view_under_test` property')

        if not extends_base_view(getattr(cls, required_view_field)):
            raise RuntimeError(
                '`view_under_test` needs to be an instance of '
                '`utils.api.base_view.APIAllView')

        setattr(cls, short_circuit_field, True)

        return cls

    def _endoint(self, target, kwargs={}, query_args={}):
        view_namespace = self.view_under_test.namespace[target]
        endpoint = (
            reverse(view_namespace, kwargs=kwargs)
            if not self.app_namespace else
            reverse(
                '%s:%s' % (self.app_namespace, view_namespace),
                kwargs=kwargs)
        )

        if query_args != {}:
            q = QueryDict(mutable=True)
            q.update(query_args)
            return '%s?%s' % (endpoint, q.urlencode())
        return endpoint

    def resource_endpoint(self, **kwargs):
        # if query provided, remove args separately
        query_args = kwargs.pop('q', {})

        # if data provided with lookup field instead of keyword argument
        # swap for endpoint
        if self.view_under_test.lookup_field in kwargs:
            kwargs[self.view_under_test.lookup_url_kwarg] = kwargs.pop(
                self.view_under_test.lookup_field)
        return self._endoint('resource', kwargs=kwargs, query_args=query_args)

    def collection_endpoint(self, **kwargs):
        # if query provided, remove args to pass separately
        query_args = kwargs.pop('q', {})
        return self._endoint(
            'collection', kwargs=kwargs, query_args=query_args)

    ##############
    # View Facts #
    ##############

    @property
    def is_sub_collection(self):
        return getattr(
            self.view_under_test, 'parent_resource__model', None) is not None

    @property
    def resource_name(self):
        return ' '.join(
            self.view_under_test.namespace['resource'].split('-')).capitalize()

    def serializer_fields(self, method):
        return get_serializer_fields(method, self.view_under_test)

    ########################
    # Data parsing helpers #
    ########################

    @classmethod
    def get_resource_uid(cls, data):
        return data[cls.view_under_test.lookup_field]

    @classmethod
    def get_parent_uid(cls, data):
        return data[cls.view_under_test.parent_lookup_field]

    ################################
    # Endpoint constructor helpers #
    ################################

    @classmethod
    def get_endpoint(cls, kwargs):
        """
        Build endpoint for kwargs data
        :param kwargs: {}
        :return: django reverse endpoint str
        """

        # Indicates data targeted is resource
        if cls.view_under_test.lookup_url_kwarg in kwargs:
            return cls.resource_endpoint(kwargs)
        return cls.collection_endpoint(kwargs)

    def get_endpoint_url_args(
            self, collection_data=None, resource_data=None,
            parent_uid=None, resource_uid=None):
        """
        Builds Kwarg args for django reverse function
        :param parent_uid: parent UID to pass as url arg
        :param resource_uid: resource UID to pass as url arg
        :return: {}
        """
        kwargs = {}
        skip_collection_data, skip_resource_data = False, False

        if parent_uid:
            kwargs[self.view_under_test.parent_lookup_url_kwarg] = parent_uid
            skip_collection_data = True

        if resource_uid:
            kwargs[self.view_under_test.lookup_url_kwarg] = resource_uid
            skip_resource_data = True

        if collection_data and not skip_collection_data:
            kwargs[self.view_under_test.parent_lookup_url_kwarg] = \
                collection_data[self.view_under_test.parent_lookup_field]

        if resource_data and not skip_resource_data:
            kwargs[self.view_under_test.lookup_url_kwarg] = resource_data[
                self.view_under_test.lookup_field]

        return kwargs

    #########################
    # Data creation helpers #
    #########################

    def create_resources(self, **kwargs):
        raise NotImplementedError(
            'create_resource not implemented for test_case')

    def create_parent_resource(self, **kwargs):
        raise NotImplementedError(
            'create_parent_resource not implemented for test_case')
