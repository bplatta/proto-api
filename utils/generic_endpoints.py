from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError

from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST


def route_request(method_handler_list, request, resource_id=None):
    """
    Helper router func
    :param method_handler_list: list of tuples -
        [(<method str>, <handler func>), ...]
    :param request: HTTPRequest object
    :param resource_id: ID of resource
    :return:
    """
    for method, handler in method_handler_list:
        if request.method.lower() == method:
            return handler(request, resource_id)


def get(get_queryset, plural_resource_name, serializer,
        filter_class, request, resource_id=None):
    """
    Generic GET request method
    :param get_queryset: func - method that returns queryset
    :param plural_resource_name: str - name of resource pluralized
    :param serializer: django serializer
    :param filter_class: django_filters.FilterSet or None
    :param request: HTTPRequest object
    :param resource_id: ID of resource
    :return: Response
    """
    if resource_id:
        instance = get_queryset().get(pk=resource_id)
        serialized_obj = serializer(instance)
        return Response(data=serialized_obj.data)

    else:
        if not filter_class:
            data = get_queryset()
        else:
            queryset = get_queryset()
            try:
                data = filter_class(
                    request.query_params,
                    queryset=queryset).qs
            except ValidationError as e:
                raise

        if data:
            serialized_objs = serializer(data=data, many=True)
            if not serialized_objs.is_valid():
                return Response(status=HTTP_400_BAD_REQUEST)
            data_final = serialized_objs.data
        else:
            data_final = []

        return Response(data={
            plural_resource_name: data_final
        })


def post(get_queryset, resource_name, serializer, request, resource_id=None):
    """
    Generic POST request method
    :param get_queryset: func - method that returns queryset
    :param resource_name: str - name of resource
    :param serializer: django serializer
    :param request: HTTPRequest object
    :param resource_id: ID of resource
    """
    if resource_id:
        try:
            resource = get_queryset().get(pk=resource_id)
        except ObjectDoesNotExist:
            data = {
                'error': '{} with ID {} does not exist'.format(
                    resource_name, resource_id)
            }
            return Response(
                data=data,
                status=HTTP_400_BAD_REQUEST)

        serialized_obj = serializer(resource, data=request.data)

        if serialized_obj.is_valid():
            serialized_obj.save()
            return Response()
        else:
            return Response(
                data=serialized_obj.errors,
                status=HTTP_400_BAD_REQUEST)
    else:
        serialized_obj = serializer(data=request.data)

        if serialized_obj.is_valid():
            instance = serialized_obj.save()
            data = {'id': instance.id}
            return Response(data=data)
        else:
            return Response(
                data=serialized_obj.errors,
                status=HTTP_400_BAD_REQUEST)


def delete(get_queryset, resource_name, request, resource_id=None):
    """
    Generic DELETE request method
    :param get_queryset: func - method that returns queryset
    :param resource_name: str - name of resource
    :param request: HTTPRequest object
    :param resource_id: ID of resource
    """
    if not resource_id:
        error = {'error': 'No resource ID provided for deletion'}
        return Response(
            data=error,
            status=HTTP_400_BAD_REQUEST)
    else:
        try:
            resource = get_queryset.get(pk=resource_id)
        except ObjectDoesNotExist:
            error = {
                'error': '{} with ID {} does not exist'.format(
                    resource_name, resource_id)
            }
            return Response(
                data=error,
                status=HTTP_400_BAD_REQUEST)

        resource.delete()

        return Response()


def put(resource_name, serializer, request, resource_id=None):
    """
    Generic PUT request method
    :param resource_name: str - name of resource
    :param serializer: django serializer
    :param request: HTTPRequest object
    :param resource_id: ID of resource
    """
    if resource_id:
        error = {
            'error': 'Invalid METHOD for resource. Cannot update {} '
                     'with ID {}'.format(resource_name.capitalize(),
                                         resource_id)
        }
        return Response(data=error, status=HTTP_400_BAD_REQUEST)
    else:
        serialized_obj = serializer(data=request.data)

        if serialized_obj.is_valid():
            instance = serialized_obj.save()
            data = {'id': instance.id}
            return Response(data=data)
        else:
            return Response(
                data=serialized_obj.errors,
                status=HTTP_400_BAD_REQUEST)
