class TestSettings:
    """
    Test settings class - used by manage.py to override settings
    """
    PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher',)

    all = {
        'PASSWORD_HASHERS': ','.join(PASSWORD_HASHERS),
        'MAX_RESOURCES_PER_FORUM': str(3)
    }


# Instantiate Singleton
TEST_SETTINGS = TestSettings()
