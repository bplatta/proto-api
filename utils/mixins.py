from django.db.models import Model
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status

from .exceptions import BaseAPIException, ExceptionMessage


class SubcollectionMixin(object):
    """
    Mixin for doing validation on the sub-collection's parent resource
    """
    parent_lookup_field = 'pk'
    parent_resource__model = None
    parent_lookup_url_kwarg = None

    def filter_queryset_by_parent(self, queryset):
        """
        Simple method for filtering queryset on parent data
        :param queryset: django Queryset
        :return: django queryset
        """
        return queryset.filter(**{
            self.parent_lookup_url_kwarg: self.parent_instance.id
        })

    @property
    def parent_request_arg(self):
        """
        Assumes parent request Arg is model class name
        :return: string
        """
        return self.parent_resource__model.__name__.lower()

    def validate_parent_instance(self, **kwargs):
        """
        Helper function for validating the ID for the parent resource is
        valid
        :param kwargs: request kwargs
        :return: object instance
        """
        lookup_arg = kwargs.pop(self.parent_lookup_url_kwarg, None)

        # this indicates a programmer error in design of a View
        if (self.parent_resource__model is None or
                not issubclass(self.parent_resource__model, Model)):
            raise BaseAPIException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                message='Base-Collection parent resource validation failed')

        try:
            # Set on view instance
            setattr(
                self, 'parent_instance',
                self.parent_resource__model.objects.get(
                    **{
                        self.parent_lookup_field: lookup_arg
                    }
                )
            )
        except ObjectDoesNotExist:
            raise BaseAPIException(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=ExceptionMessage.ObjectNotFound % (
                    self.parent_resource__model.__name__, lookup_arg)
            )

    def set_parent_on_data(self):
        """
        Set parent ID passed on request data for serializers. Should
        only be called when request.data exists
        """
        if not hasattr(self, 'parent_instance'):
            self.validate_parent_instance()

        pk = self.kwargs.get(self.parent_lookup_url_kwarg)

        # this indicates a programmer error in design of a View
        if not pk:
            raise BaseAPIException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                message='Base-Collection parent resource validation failed'
            )

        self.request.data[self.parent_request_arg] = self.kwargs[
            self.parent_lookup_url_kwarg]
