#!/usr/bin/env python
import os
import sys
import time

from django.db.utils import OperationalError

MAX_TRIES = 10
SLEEP_BETWEEN_TRIES = 5


def try_command(
        args, attempts=0, sleep_time=SLEEP_BETWEEN_TRIES, max_tries=MAX_TRIES):
    """
    Attempt to execute or retry
    :param args: sys argv
    :param attempts: int
    :param sleep_time: int
    :param max_tries: int
    """
    try:
        execute_from_command_line(args)
    except OperationalError as e:
        if attempts >= max_tries:
            print('\nMax retries attempted for connecting to container. '
                  'Is there one already running? Try stopping that first. '
                  '(Likely postgres container. Try stopped with `make stop`)')
            raise e
        else:
            attempts += 1
            print(
                '\nCommand failed due to DB Operational Error. '
                'Retrying in %s seconds ...' % sleep_time)
            time.sleep(sleep_time)
            try_command(args, attempts)


if __name__ == "__main__":

    # All for test setting overrides (needs to happen prior to test case setup)
    if 'test' in sys.argv:
        from utils.constants import TEST_SETTINGS

        for key, value in TEST_SETTINGS.all.items():
            os.environ[key] = value

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "api.settings")

    from django.core.management import execute_from_command_line

    try_command(sys.argv)
