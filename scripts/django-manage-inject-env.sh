#!/usr/bin/env bash

manage_func=$1
params=$2

if [ $(pwd | grep manage.py) ]; then
  echo "Please call this script from the application root";
  exit 1;
fi

DJANGO_SECRET_KEY="8gcphs9k+8!*r!a!7b!9f^%7bw!gljzyb!+#lhs9!1c*2hy&d=" DJANGO_DEBUG=true \
	POSTGRES_HOST="127.0.0.1" POSTGRES_DB=apidb POSTGRES_USER=api_user POSTGRES_PASSWORD=testtest POSTGRES_PORT=5432 python3 \
    manage.py $manage_func $params;
