#!/bin/bash

tags=$1

if [ $(pwd | grep scripts) ]; then
  echo "Please call this script from the application root";
  exit 1;
fi

echo "Running tasks with tags :: $tags";
printf "Using build/api/local-build-vars.json :: \n$(cat build/api/local-build-vars.json)"

exec ansible-playbook \
    --private-key=.vagrant/machines/default/virtualbox/private_key \
    -u vagrant -b \
    -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory \
    --extra-vars "@build/api/local-build-vars.json" \
    --tags "${tags}" \
    provisioning/vagrant.yaml

exit 0
