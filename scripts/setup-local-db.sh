#!/bin/bash

if [ -z "$(which createdb)" ]; then
  echo "Please install postgresd on your metal"
  exit 0
fi

if [ $(pwd | grep scripts) ]; then
  echo "Please call this script from the application root";
  exit 1;
fi

usage() {
    echo "Options are:
        -r: drop db and user first (default false)\n
        -d: api db name (default 'apidb')\n
        -u: db username (default 'api_user')\n
        -p: db username password (default 'testtest')";
    exit 1;
}

DROP=0
MIGRATE_DB=0
API_DB=apidb
API_DB_USER=api_user
API_DB_PASS=testtest
MODIFIED_LOCAL_VARS=0

while getopts "hrd:u:p:m" opt; do
    case $opt in
        r)
            DROP=1 ;;
        d)
            API_DB=$OPTARG ;;
        u)
            API_DB_USER=$OPTARG ;;
        p)
            API_DB_PASS=$OPTARG ;;
        m)
            MIGRATE_DB=1 ;;
        h | *)
            usage ;;
    esac
done

echo "Using default postgres connection settings. Set with env to change."

if [ ${DROP} -eq 1 ]; then
   echo "= Dropping local API DB"
   dropdb ${API_DB};
   echo "= Dropping local user"
   dropuser ${API_DB_USER}
fi

echo "= Creating DB '${API_DB}'"
createdb ${API_DB};
echo "= Creating User '${API_DB_USER}' ..."
psql -c "CREATE USER ${API_DB_USER} WITH PASSWORD '${API_DB_PASS}';" &>/dev/null;
echo "= Granting user '${API_DB_USER}' all privileges on '${API_DB}'"
psql -c "GRANT ALL PRIVILEGES ON DATABASE ${API_DB} to ${API_DB_USER};" &>/dev/null;

if [ ${MIGRATE_DB} -eq 1 ]; then

    echo "Migrating local DB with ansible";

    if [ -z $(cat provisioning/vars.local | grep run_django_db_migrations) ]; then
        sed -i '' -e '$a\' provisioning/vars.local
        echo "run_django_db_migrations: true" >> provisioning/vars.local;
        MODIFIED_LOCAL_VARS=1
    fi

    cat provisioning/vars.local
    echo "Running ansible playbook with tags 'inital_db_setup'"
    exec scripts/local-ansible-run-tags.sh inital_db_setup

    if [ ${MODIFIED_LOCAL_VARS} -eq 1 ]; then
        sed -i '' '/^run_django_db_migrations/d' provisioning/vars.local;
    fi
fi

echo "DONE"
