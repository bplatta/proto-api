from rest_framework import serializers

from users.models import APIUser, APIUserData
from utils.api.base_serializers import BasePostSerializer


class UserPOSTSerializer(BasePostSerializer):
    """
    User form handling creation of new APIUsers
    """
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128)
    first_name = serializers.CharField(
        max_length=30, required=False, allow_blank=True)
    last_name = serializers.CharField(
        max_length=100, required=False, allow_blank=True)

    invalid_update_fields = ['password']

    def _validate_unique_email(self, email):
        """
        Validate uniqueness of email
        :param email: str
        :return: None or raises
        """
        if APIUser.objects.filter(email=email).exists():
            raise serializers.ValidationError({
                'email': 'User with email `%s` ready exists' % email
            })

    def validate(self, attrs):
        """
        User data validation
        :param attrs: {}
        :return: {}
        """
        if 'email' in attrs:
            self._validate_unique_email(attrs['email'])

        return attrs

    def create(self, validated_data):
        """
        Create new user in DB
        :param validated_data: {}
        :return: APIUser
        """
        email = validated_data.pop('email')
        password = validated_data.pop('password')
        return APIUser.objects.create_user(email, password, **validated_data)

    def update(self, instance, validated_data):
        """
        Update User information
        :param instance:
        :param validated_data:
        """
        instance.first_name = validated_data.get(
            'first_name', instance.first_name)
        instance.last_name = validated_data.get(
            'last_name', instance.last_name)

        instance.save()
        return instance


class UserREADSerializer(serializers.ModelSerializer):
    """
    User related data
    """
    id = serializers.CharField(source='uid')

    class Meta:
        model = APIUser
        fields = ['first_name', 'last_name', 'email', 'id']


class UserLISTSerializer(serializers.ModelSerializer):
    """
    User LIST serializer
    """
    id = serializers.CharField(source='uid')

    class Meta:
        model = APIUser
        fields = ['id', 'first_name', 'last_name', 'email']
