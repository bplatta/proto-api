from rest_framework import status
from rest_framework.response import Response

from users.models import APIUser
from users.permissions import UserPermissions
from users.serializers import (
    UserPOSTSerializer, UserLISTSerializer, UserREADSerializer)

from utils.api.base_view import APIAllView


class UsersView(APIAllView):
    """
    Main view for handling interactions with the Forums
    """
    main_model = APIUser
    lookup_url_kwarg = 'user_id'
    lookup_field = 'uid'
    permission_classes = (UserPermissions,)
    http_method_names = ['get', 'post', 'delete']
    supported_operations = ['read', 'list', 'create', 'delete', 'update']

    namespace = {
        'collection': 'users',
        'resource': 'user'
    }

    serializer_classes = {
        'post': {
            'default': UserPOSTSerializer
        },
        'get': {
            'default': UserLISTSerializer,
            'collection': UserLISTSerializer,
            'resource': UserREADSerializer
        }
    }

    def get_queryset(self):
        """
        Filter Users based on admin status
        :return: QuerySet
        """
        if self.request.user.is_staff:
            return APIUser.objects.all()

        return APIUser.objects.filter(is_active=True)

    def post(self, request, user_id=None):
        # Permissioning handled by UserPermissions cls
        return self._post(request, user_id=user_id)

    def get(self, request, user_id=None):
        # only allow staff users to list users for now
        if user_id is None and not request.user.is_staff:
            return self.permission_denied(request)

        return self._get(request, user_id=user_id)

    def delete(self, request, user_id=None):

        # If staff, delete the user object for real
        if request.user.is_staff:
            return self._delete(request, user_id=user_id)

        # If issued by the user, deactivate the user
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
