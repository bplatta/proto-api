from rest_framework.permissions import BasePermission

from utils.api.base_permissions import BaseRequestFacts


class UserFacts(BaseRequestFacts):
    """
    Facts about Forum data given the request
    """
    def is_admin(self):
        return self.request.user and self.request.user.is_staff

    def is_self(self):
        """
        Ie. operating on user's User resource
        :return:
        """
        return self.request.user == self.resource


class UserPermissions(BasePermission):
    """
    Permissions for the User collection endpoint

    Assumes:
        - authentication happens first

        GET:
            list: Only admin can list all - view filters queryset
            read:
                can read if admin or self
        POST:
            create: Anon can create
            update: only creator can update self
        DELETE:
            delete: only owner can delete self
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        if request.method == 'POST':
            # only admin can create new users
            return request.user.is_staff
        else:
            return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        """
        Permissions on User resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = UserFacts(request, view, obj)

        if Facts.is_admin():
            return True

        # For Read, Update or Delete operations
        return Facts.is_self()
