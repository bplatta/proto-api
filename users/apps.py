from django.apps import AppConfig


class UsersApp(AppConfig):
    name = 'users'
    label = 'users'

    def ready(self):
        """
        Setup module
        """
        # Setup signal listeners by importing them at runtime
        import users.signal_listeners
