from __future__ import unicode_literals

import uuid

from django.contrib.postgres import fields
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as lazy_translate
from django.contrib.auth.models import PermissionsMixin

USER_UID_LENGTH = 18


def generate_uuid_func(name, length):
    """
    Creates a function for generating UUID of certain length, setting func name
    :param name: str
    :param length: int
    :return: func
    """
    def gen_uuid():
        return str(uuid.uuid4())[:length]
    gen_uuid.__name__ = name
    return gen_uuid

user_uid = generate_uuid_func('user_uid', USER_UID_LENGTH)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


#
# API User model
#
class APIAbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Email and password are required. Other fields are optional.
    """
    MAX_NICKNAMES = 5

    uid = models.CharField(
        unique=True, editable=False, default=user_uid, max_length=55)
    email = models.EmailField(
        lazy_translate('email address'),
        unique=True,
        help_text=lazy_translate(
            'Required. Please provide a valid email address'),
        error_messages={
            'unique': lazy_translate(
                'A user with that email already exists.')
        }
    )
    names = fields.ArrayField(
        models.CharField(max_length=50), size=MAX_NICKNAMES, default=list),
    first_name = models.CharField(
        lazy_translate('first name'), max_length=30, blank=True)
    last_name = models.CharField(
        lazy_translate('last name'), max_length=100, blank=True)
    is_staff = models.BooleanField(
        lazy_translate('staff status'),
        default=False,
        help_text=lazy_translate(
            'Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        lazy_translate('active'),
        default=True,
        help_text=lazy_translate(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_banned = models.BooleanField(
        lazy_translate('banned'),
        default=False
    )
    date_joined = models.DateTimeField(
        lazy_translate('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = lazy_translate('apiuser')
        verbose_name_plural = lazy_translate('apiusers')
        abstract = True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name


class APIUser(APIAbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.

    Password and email are required. Other fields are optional.
    """
    class Meta(APIAbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'


class APIUserData(models.Model):
    """
    Table for arbitrary user data
    """
    user = models.ForeignKey(APIUser, related_name='user_data')
    data = fields.JSONField()
