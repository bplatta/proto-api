import os

from django.core.management.base import BaseCommand, CommandError

from users.models import APIUser


class Command(BaseCommand):
    help = 'Creates a super user with default credentials'

    DEFAULT_PASSWORD = 'testpass'
    DEFAULT_EMAIL = 'root@root.com'

    def add_arguments(self, parser):
        """
        Allow passing email or password to command
        :param parser: argparse.ArgumentParser
        """
        parser.add_argument(
            '--email',
            dest='email',
            default=self.DEFAULT_EMAIL,
            help='Provide email for superuser creation',
        )
        parser.add_argument(
            '--password',
            dest='password',
            default=self.DEFAULT_PASSWORD,
            help='Provide password for superuser creation',
        )

    def handle(self, *args, **options):
        """
        Handle creation of super user
        :param args:
        :param options:
        :return:
        """
        if os.environ.get('POSTGRES_HOST') != 'postgres':
            raise CommandError(
                'Can only use create_api_superuser on local container.')

        email = options.get('email')
        password = options.get('password')
        APIUser.objects.create_superuser(email, password)
        self.stdout.write(
            'API superuser created with email `%s` and password `%s`'
            % (email, password))
