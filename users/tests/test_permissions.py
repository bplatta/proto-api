import unittest
from django.test import tag

from users.permissions import UserPermissions


class FakeUser(object):
    def __init__(self, staff, authed):
        self.is_staff = staff
        self.is_authenticated = authed


class FakeRequest(object):
    def __init__(self, method, user=None, staff=None, authed=None):
        self.method = method.upper()

        if user:
            self.user = user
        else:
            self.user = FakeUser(staff, authed)


@tag('unit')
class UserPermissionstestCase(unittest.TestCase):

    def test_user_permission_post_request_non_admin(self):
        """
        Test correct permissions enforced for POST request for non staff
        """
        permission_class = UserPermissions()
        request = FakeRequest(method='POST', staff=False, authed=True)
        null_view = {}
        self.assertFalse(permission_class.has_permission(request, null_view))

    def test_user_permission_post_request_admin(self):
        """
        Test correct permissions enforced for POST request for staff
        """
        permission_class = UserPermissions()
        request = FakeRequest(method='POST', staff=True, authed=True)
        null_view = {}
        self.assertTrue(permission_class.has_permission(request, null_view))

    def test_user_permission_non_post_request_non_admin(self):
        """
        Test correct permissions enforced for non-POST request for non staff
        """
        permission_class = UserPermissions()
        request = FakeRequest(method='GET', staff=False, authed=True)
        null_view = {}
        self.assertTrue(permission_class.has_permission(request, null_view))

    def test_user_obj_permission_not_self(self):
        """
        Test user object access limited to own user
        """
        permission_class = UserPermissions()
        user = FakeUser(staff=False, authed=True)
        resource_user = FakeUser(staff=False, authed=True)
        request = FakeRequest(method='GET', user=user)
        null_view = {}
        self.assertFalse(permission_class.has_object_permission(
            request, null_view, resource_user))

    def test_user_obj_permission_self_success(self):
        """
        Test user object access limited to own user
        """
        permission_class = UserPermissions()
        user = FakeUser(staff=False, authed=True)
        request = FakeRequest(method='GET', user=user)
        null_view = {}
        resource = user
        self.assertTrue(permission_class.has_object_permission(
            request, null_view, resource))

    def test_user_obj_permission_admin(self):
        """
        Test admin can access all users
        """
        permission_class = UserPermissions()
        user = FakeUser(staff=True, authed=True)
        resource_user = FakeUser(staff=False, authed=True)
        request = FakeRequest(method='GET', user=user)
        null_view = {}
        self.assertTrue(permission_class.has_object_permission(
            request, null_view, resource_user))
