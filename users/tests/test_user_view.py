from django.test import tag
from django.contrib.auth.hashers import check_password
from rest_framework import status

from users.models import APIUser
from users.views import UsersView
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from utils.testing.randomizer import Randomizer


@tag('db')
class UserViewTestcase(BaseAPIAllViewTestCase):
    """
    Test User View handler
    """
    POST_FORMAT = 'json'

    view_under_test = UsersView

    app_namespace = 'Users'

    randomizer = Randomizer()

    def test_create_user_invalid_permissions(self):
        """
        Test non admin user can't create a user
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)
        user_args = {
            'email': self.randomizer.generate_field('email'),
            'password': self.randomizer.generate_field(str),
            'first_name': self.randomizer.generate_field(str)
        }
        response = self.client.post(
            self.collection_endpoint(),
            data=user_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.'})

        # Verify resource is not in database
        self.assertFalse(
            APIUser.objects.filter(
                email=user_args.get('email')
            ).exists()
        )

    def test_create_user_success(self):
        """
        Test admin create user succeeds
        """
        self.as_staff(self.user)
        # Testing with Basic Auth as that is the expected authentication for
        # Admin user creation
        self.basic_auth_client(self.api_key)
        user_args = {
            'email': self.randomizer.generate_field('email'),
            'password': self.randomizer.generate_field(str),
            'first_name': '',  # should allow blank
            'last_name': self.randomizer.generate_field(str)
        }
        response = self.client.post(
            self.collection_endpoint(),
            data=user_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        user_id = response.data.get('id')

        # Verify resource is in database
        self.assertTrue(
            APIUser.objects.filter(
                uid=user_id
            ).exists()
        )

        # Assert that the password was hashed properly on save
        self.assertTrue(
            check_password(
                user_args['password'],
                APIUser.objects.get(uid=user_id).password
            )
        )

    def test_create_user_duplicate_email__negative(self):
        """
        Test admin create user with duplicate email fails
        """
        self.as_staff(self.user)
        # Testing with Basic Auth as that is the expected authentication for
        # Admin user creation
        self.basic_auth_client(self.api_key)
        user_args = {
            'email': self.creds['email'],
            'password': self.randomizer.generate_field(str)
        }
        response = self.client.post(
            self.collection_endpoint(),
            data=user_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(response.data, {
            'email': [
                'User with email `%s` ready exists' % self.creds['email']]
        })

    def test_create_user_invalid_params(self):
        """
        Test admin create user fails for invalid parameters
        """

        self.as_staff(self.user)
        self.basic_auth_client(self.api_key)
        # Email field is only a string!
        user_args = {
            'email': self.randomizer.generate_field(str),
            'password': self.randomizer.generate_field(str),
            'first_name': self.randomizer.generate_field(str),
            'last_name': ''
        }
        response = self.client.post(
            self.collection_endpoint(),
            data=user_args, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(response.data, {
            'email': ['Enter a valid email address.']
        })

    def test_read_user_admin(self):
        """
        Test READ request for random user succeeds for admin
        """
        self.as_staff(self.user)
        self.basic_auth_client(self.api_key)

        other_user, other_creds = self.new_user()
        response = self.client.get(
            self.resource_endpoint(user_id=other_user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        self.assertEqual(response.data, {
            'email': other_creds['email'],
            'id': other_user.uid,
            'first_name': other_creds['first_name'],
            'last_name': other_creds['last_name']
        }, response.data)

    def test_read_user_permission_denied(self):
        """
        Test READ request for non admin user on non-self fails
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)

        other_user, _ = self.new_user()
        response = self.client.get(
            self.resource_endpoint(user_id=other_user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        self.assertEqual(
            response.data,
            {'detail': 'You do not have permission to perform this action.'})

    def test_read_user_non_staff_self_success(self):
        """
        Test READ request on user self resource succeeds
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)

        response = self.client.get(
            self.resource_endpoint(user_id=self.user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data, {
            'email': self.creds['email'],
            'id': self.user.uid,
            'first_name': self.creds['first_name'],
            'last_name': self.creds['last_name']
        }, response.data)

    def test_list_users_permission_denied(self):
        """
        Test LIST request by non admin fails
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)

        response = self.client.get(
            self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        self.assertEqual(
            response.data,
            {'detail': 'You do not have permission to perform this action.'})

    def test_list_users_admin_success(self):
        """
        Test LIST request by admin succeeds
        """
        self.as_staff(self.user)
        self.basic_auth_client(self.api_key)

        response = self.client.get(
            self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        self.assertGreaterEqual(response.data.get('count'), 1)
        self.assertIn('results', response.data)

    def test_delete_user_success_admin(self):
        """
        Test DELETE request on user by admin actually deletes resource from DB
        """
        self.as_staff(self.user)
        self.basic_auth_client(self.api_key)

        other_user, _ = self.new_user()
        response = self.client.delete(
            self.resource_endpoint(user_id=other_user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertFalse(
            APIUser.objects.filter(
                uid=other_user.uid
            ).exists()
        )

    def test_deactivate_user_success(self):
        """
        Test DELETE request by user deactivates user
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)

        response = self.client.delete(
            self.resource_endpoint(user_id=self.user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertFalse(
            APIUser.objects.get(uid=self.user.uid).is_active)

        # Reactivate the test case user!
        self.token.user.is_active = True
        self.token.user.save()

    def test_delete_user_permission_denied(self):
        """
        Test DELETE request on user resource by non self fails
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)

        other_user, _ = self.new_user()
        response = self.client.delete(
            self.resource_endpoint(user_id=other_user.uid))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        self.assertEqual(
            response.data,
            {'detail': 'You do not have permission to perform this action.'})
