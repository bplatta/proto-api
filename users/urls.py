from django.conf.urls import url

from users.views import UsersView

urlpatterns = [
    url(  # Users collection
        r'^$',
        UsersView.as_view(),
        name=UsersView.namespace['collection']
    ),

    url(  # User as resource
        r'^(?P<user_id>[0-9a-z-]+)/$',
        UsersView.as_view(),
        name=UsersView.namespace['resource']
    )
]
