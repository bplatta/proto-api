from django.dispatch import receiver
from django.utils import timezone

from api_auth.signals import user_token_generated


# Signal connectors - require import in users.apps
@receiver(user_token_generated)
def update_last_login(sender, user, **kwargs):
    """
    A signal receiver which updates the last_login date for the user
    generating a token for data access
    """
    user.last_login = timezone.now()
    user.save(update_fields=['last_login'])
