# AUTH: Operational Definitions

### Authentication
The process of verifying a user is who they claim to be. (authenticate/login/logout)

### Authorization
The process of verifying a user can do what they want to do. (authorize token)

### Implementations
 - Bearer auth with expiring tokens
 - Basic auth with API keys