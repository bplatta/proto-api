from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as lazy_translate

from rest_framework import serializers


class AuthenticationSerializer(serializers.Serializer):
    """
    Handles validation of user credentials
    """
    email = serializers.CharField(label=lazy_translate("Email"))
    password = serializers.CharField(label=lazy_translate("Password"))

    def validate(self, data):
        """
        Authentiates the user's credentials if valid
        :return: data with user object
        """
        email = data.get('email')
        password = data.get('password')

        if email and password:
            user = authenticate(email=email, password=password)

            if user:
                if not user.is_active:
                    msg = lazy_translate('User is disabled.')
                    raise serializers.ValidationError({'error': msg})
            else:
                msg = lazy_translate(
                    'Unable to authenticate with provided credentials.')
                raise serializers.ValidationError({'error': msg})
        else:
            msg = lazy_translate(
                'Must include "email" and "password".')
            raise serializers.ValidationError({'error': msg})

        data['user'] = user
        return data
