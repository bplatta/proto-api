from __future__ import unicode_literals

import binascii
import os
import pytz
from datetime import datetime, timedelta
from django.db import models
from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as lazy_translate


def get_token_expiration():
    return datetime.now(pytz.utc) + timedelta(
        seconds=settings.TOKEN_EXPIRATION_SECONDS)


@python_2_unicode_compatible
class ExpiringToken(models.Model):
    """
    The default authorization token model.
    """
    DEFAULT_KEY_LENGTH = 30

    key = models.CharField(
        lazy_translate("Key"), max_length=100, primary_key=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name='expiring_token',
        on_delete=models.CASCADE, verbose_name=lazy_translate("APIUser")
    )
    created = models.DateTimeField(
        lazy_translate("Created"), auto_now_add=True)
    invalidation_time = models.DateTimeField(
        lazy_translate("Invalidation Time"), default=get_token_expiration)
    expired = models.BooleanField(default=False)

    class Meta:
        verbose_name = lazy_translate("Expiring Token")
        verbose_name_plural = lazy_translate("Expiring Tokens")

    @property
    def default_expiration_seconds(self):
        return settings.TOKEN_EXPIRATION_SECONDS

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(ExpiringToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(self.DEFAULT_KEY_LENGTH)).decode()

    def __str_lazy_translate(self):
        return self.key

    def is_expired(self):
        if self.expired:
            return self.expired
        self.expired = datetime.now(pytz.utc) > self.invalidation_time
        self.save()
        return self.expired

    def expire(self):
        self.expired = True
        self.save()

    def reset_expiration(self):
        self.invalidation_time = get_token_expiration()
        self.expired = False
        self.save()


@python_2_unicode_compatible
class ApiAppKey(models.Model):
    """
    API key model - another way to authenticate requests
    """
    DEFAULT_KEY_LENGTH = 30

    key = models.CharField(
        lazy_translate("Key"), max_length=100, unique=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name='api_key',
        on_delete=models.CASCADE, verbose_name=lazy_translate("APIUser")
    )
    created = models.DateTimeField(
        lazy_translate("Created"), auto_now_add=True)

    class Meta:
        verbose_name = lazy_translate("API Key")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(ApiAppKey, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(self.DEFAULT_KEY_LENGTH)).decode()

    def regenerate_key(self):
        self.key = self.generate_key()
        self.save()
        return self.key
