from django.apps import AppConfig


class APIAuthConfig(AppConfig):
    name = 'api_auth'

    def ready(self):
        """
        Setup application
        """
        import api_auth.signals
