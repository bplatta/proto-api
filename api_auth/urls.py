from django.conf.urls import url
from api_auth.views.token_view import TokenView
from api_auth.views.api_key_view import ApiKeyView

urlpatterns = [
    url(r'^token/', TokenView.as_view(), name=TokenView.namespace),
    url(r'^api-key/', ApiKeyView.as_view(), name=ApiKeyView.namespace),
]
