from django.dispatch import Signal

user_token_generated = Signal(providing_args=['request', 'user'])
user_authorization_fail = Signal(providing_args=['credentials'])
user_token_deleted = Signal(providing_args=['request', 'user'])
