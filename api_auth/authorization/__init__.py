import base64
import logging


from django.utils.six import text_type
from rest_framework import status, HTTP_HEADER_ENCODING

from api_auth.models import ExpiringToken, ApiAppKey
from utils.exceptions import BaseAPIException


logger = logging.getLogger(__name__)

AUTHORIZATION_HEADER = 'HTTP_AUTHORIZATION'
HOST_HEADER = 'HTTP_HOST'


class AuthTypes:
    BASIC = 'basic'
    BEARER = 'bearer'

    @classmethod
    def supported(cls):
        return [cls.BEARER, cls.BASIC]

    @classmethod
    def b_supported(cls):
        return [cls.BEARER.encode(), cls.BASIC.encode()]

    @classmethod
    def get_model(cls, auth_type):
        return ExpiringToken if auth_type == cls.BEARER else ApiAppKey


class AuthenticationBreak(Exception):
    """
    Simple Auth Exception to catch
    """
    def __init__(self, response_message='No authentication'):
        self.message = response_message

    def __str__(self):
        return 'Authentication Failure: `%s`' % self.message


def get_header(request, header):
    """
    Return request's header, as a bytestring.

    Hide some test client ickyness where the header can be unicode.
    """
    auth = request.META.get(header, b'')

    if isinstance(auth, text_type):
        # Work around django test client oddness
        auth = auth.encode(HTTP_HEADER_ENCODING)
    return auth


def parse_auth_header(request):
    """
    Parse the authentication token from the request Auth header
    :param request: HttpRequest
    :return: string
    """
    return decode_parts(
        validate_header_parts(
            split_header(
                get_header(request, AUTHORIZATION_HEADER)  # returns bytestring
            )
        )
    )


def from_webserver(request, webserver_host):
    """
    Determines if request from webserver based on host header
    :param request: HttpRequest
    :param webserver_host: str
    :return: Bool
    """
    if webserver_host is None:
        return False

    return (
        decode_to_str(get_header(request, HOST_HEADER)) ==
        webserver_host)


def split_header(header_str):
    """
    split header on space
    :param header_str: str
    :return: []
    """
    return [part for part in header_str.split(b' ') if part != b'']


def validate_header_parts(header_parts, auth_types=AuthTypes.b_supported()):
    """
    Validate parts of the header, determine if fatal exception
    :param header_parts: []
    :param auth_types: tuple of strings
    :return: header_parts or raise
    """
    if len(header_parts) == 0:
        return None, None
    elif header_parts[0].lower() not in auth_types:
        raise AuthenticationBreak(
            'Invalid Authorization header. '
            '`Bearer <Token string>` required.')
    elif len(header_parts) == 1:
        raise AuthenticationBreak(
            'Invalid Authorization header. '
            '`Bearer <Token string>` required.')
    elif len(header_parts) > 2:
        raise AuthenticationBreak(
            'Invalid credentials provided. Token should not contain spaces.')
    return header_parts


def decode_parts(header_parts):
    """
    Decode the parsed parts depending on Auth type
    :param header_parts: list of str
    :return: tuple: (<auth_type str>, <auth_data dict>)
    """
    if header_parts[0] is None:
        return None, None

    auth_type = decode_to_str(header_parts[0]).lower()
    if auth_type == AuthTypes.BEARER:
        return (
            auth_type,
            {'key': decode_to_str(header_parts[1])}
        )

    # BASIC authentication requires base64 decoding
    return (
        auth_type,
        decode_basic_auth_creds(header_parts[1])
    )


def decode_basic_auth_creds(auth_cred_bytestring):
    """
    Base64Decode basic auth creds
    :param auth_cred_bytestring: b64 encoded bytestring of creds
    :return: {} with user_uid and api_key
    """
    try:
        raw_creds = base64.b64decode(auth_cred_bytestring).decode()

    # Im typically against general exception handling like this. THis seems
    # pretty scoped to the decoding errors though
    except Exception as e:
        raise AuthenticationBreak(
            'Invalid authentication for basic auth. '
            'Credential must be base64 encoded.')

    cred_parts = raw_creds.split(':')
    logger.debug('CREDENTIAL PARTS: %s' % cred_parts)

    return {
        'key': cred_parts[0]
    }


def decode_to_str(candidate):
    """
    Convert to string
    :param candidate: bytestring
    :return: tuple of decoded strings (from bytestring) or None's
    """
    if not candidate:
        return None
    try:
        return candidate.decode()
    except UnicodeError:
        raise AuthenticationBreak('Str contains invalid characters')


def validate_token(token):
    """
    Checks if token is valid authentication for user
    :param token: ExpiringToken object
    :return: raises or returns token
    """
    if token.is_expired() is True:
        raise BaseAPIException(
            message='Token has expired. Acquire new token with POST '
                    'to /auth/token/ endpoint, providing username '
                    'and password',
            status_code=status.HTTP_400_BAD_REQUEST)

    if not token.user.is_active:
        raise BaseAPIException(
            message='Invalid token provided',
            status_code=status.HTTP_400_BAD_REQUEST)

    return token


def validate_api_key(api_key):
    """
    Checks if API key is valid auth for user
    :param api_key: ApiAppKey object
    :return: raises or returns key
    """
    if not api_key.user.is_active:
        raise BaseAPIException(
            message='Invalid API key provided',
            status_code=status.HTTP_400_BAD_REQUEST)

    return api_key


def retrieve_user_and_auth(auth_data, auth_type):
    """
    Retrieve the token from the database with the given model. Should
    be called in the context of an APIView
    :param auth_data: auth data such as the token key to query model
    :param auth_type: string for given authentication method
    :return: User, Token, ApiKey
    """
    if auth_data is None:
        from django.contrib.auth.models import AnonymousUser
        return AnonymousUser(), None, None

    auth_model = AuthTypes.get_model(auth_type)
    try:
        auth_key = auth_model.objects.select_related('user').get(**auth_data)
    except auth_model.DoesNotExist:
        logger.debug('Invalid Authentication %s with auth data: %s' % (
            auth_type, auth_data))
        raise BaseAPIException(
            message='Invalid authentication provided',
            status_code=status.HTTP_400_BAD_REQUEST)

    token, api_key = (None,)*2
    if auth_type == AuthTypes.BEARER:
        token = validate_token(auth_key)
    else:
        api_key = validate_api_key(auth_key)

    return auth_key.user, token, api_key
