import json
import logging

from functools import partial

from django.conf import settings
from django.http import HttpResponse
from django.utils.translation import ugettext as translate
from rest_framework import status
from api_auth.authorization import (
    parse_auth_header, retrieve_user_and_auth, from_webserver,
    AuthenticationBreak)

logger = logging.getLogger(__name__)


def parse_token_and_bind_auth_call(request):
    """
    Parses token from the request Header. Fails early if a fatal parse error
    occurs
    :param request: HttpRequest
    :return: None
    """
    try:
        auth_type, auth_data_or_none = parse_auth_header(request)
        webserver_referred = from_webserver(
            request, settings.WEBSERVER_HOST)
    except AuthenticationBreak as e:
        return e.message

    # set bool on request if referred by the webserver
    setattr(request, 'webserver_referred', webserver_referred)

    # Bind token to retrieval of user and token from DB
    get_user_and_auth = partial(
        retrieve_user_and_auth, auth_data_or_none, auth_type)

    def authorize():
        # return user, token, api_key tuple
        # (one, or both of the latter will be None)
        return get_user_and_auth()

    request.authorize = authorize


def api_auth_middleware(get_response):
    """
    Authentication middleware that maps request to a user based on Token
    in the header
    :param get_response: method pass request onto next middleware
    :return: response
    """

    def middleware(request):
        """
        Simply set request.user as lazily evaluated field on request. This
        corresponds to what the APIView is expecting when it performs
        authentication
        :param request: Request
        :return: Response
        """
        # Parses token if it exists and binds request.authenticate() method
        # to be called by view
        error_response = parse_token_and_bind_auth_call(request)
        # If parsing error, short circuit request
        if error_response:
            logger.info(
                'Request error: %s Headers: %s' % (
                    error_response, request.META))
            return HttpResponse(
                json.dumps({
                    'error': translate(error_response)
                }),
                status=status.HTTP_400_BAD_REQUEST,
                content_type='application/json')

        response = get_response(request)

        # If request was made with a token, reset the ttl on the token. This
        # is essentially a session implementation with tokens. `authed_token`
        # is set by the view `perform_authentication` method
        if (hasattr(request, 'authed_token') and
                request.authed_token is not None):
            request.authed_token.reset_expiration()

        return response

    return middleware
