import logging

from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

from api_auth.models import ApiAppKey
from utils.api.base_view import BaseAuthView
from utils.exceptions import BaseAPIException

from api_auth.permissions import ApiAppKeyPermissions


class ApiKeyView(BaseAuthView):
    """
    View handling operations on ApiKeys resources: auth.models.ApiAppKey
    """
    logger = logging.getLogger(__name__)
    namespace = 'api-key'
    model = ApiAppKey
    http_method_names = ['post', 'delete', 'get']
    # Currently only let Admin have access to API Keys
    permission_classes = [IsAdminUser]
    # permission_classes = [ApiAppKeyPermissions]

    def post(self, request):
        """
        Handles creation of an API application key
        :param request: Request object
        :return: Response object with key
        """

        user = request.user
        if self.model.objects.filter(user=user).exists():
            raise BaseAPIException(
                status_code=status.HTTP_400_BAD_REQUEST,
                message='API Key already exists. Please delete API Key '
                        'before re-issuing this request '
                        '(DELETE /auth/api-key/)'
            )

        # create new token
        api_key = self.model.objects.create(user=user)
        self.logger.info('API Key created for user `%s`' % user.uid)
        return Response({
            'api_key': api_key.key,
            'message': 'WARNING: This key does not expire so please '
                       'keep it safe. You can delete or regenerate keys '
                       'with DELETE or POST requests to /auth/api-key/'
        }, status=status.HTTP_201_CREATED)

    def get(self, request):
        """
        Reads the API Key for the current user
        :param request: Request object
        :return: Response object
        """
        user = request.user
        try:
            api_key = self.model.objects.get(user=user).key
            return Response({
                'api_key': api_key
            })
        except self.model.DoesNotExist:
            return Response({
                'api_key': None,
                'message': 'No API Key exists for user. Create with '
                           'POST /auth/api-key/'
            })

    def delete(self, request):
        """
        Delete api-key associated with the user
        :param request: Request Object
        :return: Response
        """
        if request.user.api_key:
            request.user.api_key.delete()
        return Response()
