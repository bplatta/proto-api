import logging

from rest_framework import status
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from api_auth.models import ExpiringToken
from api_auth.signals import user_token_deleted, user_token_generated
from api_auth.authentication.serializer import AuthenticationSerializer
from utils.api.base_view import BaseAuthView

from api_auth.permissions import TokenPermissions


class TokenView(BaseAuthView):
    """
    View handling operations on Token resources: auth.models.ExpiringToken
    """
    logger = logging.getLogger(__name__)
    namespace = 'token'
    token_model = ExpiringToken
    http_method_names = ['post', 'delete']
    permission_classes = [TokenPermissions]

    def post(self, request):
        """
        Handles creation of a Token
        :param request: Request object
        :return: Response object with Token
        """
        # authenticate user credentials
        authentication_form = AuthenticationSerializer(data=request.data)
        authentication_form.is_valid(raise_exception=True)
        user = authentication_form.validated_data['user']
        # delete any tokens associated with this user
        self.token_model.objects.filter(user=user).delete()
        # create new token
        token = self.token_model.objects.create(user=user)
        user_token_generated.send(self.__class__, request=request, user=user)

        self.logger.info('API Access Token created for user `%s`' % user.uid)
        return Response({
            'access_token': token.key,
            'expires_in__sec':  token.default_expiration_seconds
        }, status=status.HTTP_201_CREATED)

    def delete(self, request):
        """
        Delete token associated with the user
        :param request: Request Object
        :return: Response
        """
        request.user.expiring_token.delete()
        user_token_deleted.send(
            self.__class__, request=request, user=request.user)
        return Response()
