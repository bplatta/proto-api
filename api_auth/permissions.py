
class TokenPermissions(object):
    """
    Permissions for the Tokens collection endpoint

    Assumes:
        POST:
            create: any can create token
        DELETE:
            delete: only owner can delete a token
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        method = request.method.lower()
        # Any user can attempt to create a token with creds passed
        if method == 'post':
            return True
        elif method == 'delete':
            return request.user.is_authenticated

        # Other methods not supported, but that is caught downstream in view
        return True

    def has_object_permission(self, request, view, obj):
        """
        No resource tokens can be targeted specifically
        """
        return False


class ApiAppKeyPermissions(object):
    """
    Permissions for the API key collection endpoint

    Assumes:
        POST:
            create: authenticated can create an API key
        DELETE:
            delete: only owner can delete an api key
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        """
        No resource api-keys can be targeted specifically
        """
        return False
