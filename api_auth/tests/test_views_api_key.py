from django.test import tag
from django.urls import reverse
from rest_framework import status

from utils.testing.base_test_case import BaseAPIDBTestCase
from api_auth.models import ApiAppKey


@tag('db')
class TestApiKeyViewTestCase(BaseAPIDBTestCase):
    """
    Test ApiKey View operations
    """
    POST_FORMAT = 'json'
    view_namespace = 'api-key'
    app_namespace = 'Auth'
    reverse_route = '%s:%s' % (app_namespace, view_namespace)

    @classmethod
    def api_key_endpoint(cls):
        return reverse(cls.reverse_route)

    def test_successful_api_key_generation(self):
        """
        Tests Successful generation of api_key for requests
        """
        user, creds = self.new_user()
        self.as_staff(user)
        token = self.new_token(user)

        self.bearer_auth_client(token)
        response = self.client.post(
            self.api_key_endpoint(), format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('api_key', response.data)
        self.assertTrue(
            ApiAppKey.objects.filter(
                key=response.data.get('api_key')
            ).exists()
        )
        self.assertEqual(
            response.data.get('message'),
            'WARNING: This key does not expire so please keep it safe. '
            'You can delete or regenerate keys with DELETE or POST '
            'requests to /auth/api-key/'
        )

    def test_api_key_post_fail_already_exists(self):
        """
        Tests API Key POST request fails if a key already exists
        """
        self.as_staff(self.user)
        self.bearer_auth_client(self.token)
        # API key created during setup of class
        response = self.client.post(
            self.api_key_endpoint(), format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': 'API Key already exists. Please delete API Key '
                     'before re-issuing this request (DELETE /auth/api-key/)'
        })

    def test_successful_delete_api_key_with_token_auth(self):
        """
        Tests Successful deletion of api key with Bearer auth
        """
        user, creds = self.new_user()
        self.as_staff(user)
        api_key = self.new_api_key(user)
        token = self.new_token(user)

        self.bearer_auth_client(token)
        response = self.client.delete(
            self.api_key_endpoint(), format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)
        self.assertFalse(
            ApiAppKey.objects.filter(
                key=api_key.key
            ).exists()
        )

    def test_successful_delete_api_key_with_api_key_auth(self):
        """
        Tests Successful deletion of api key with Basic auth
        """
        user, creds = self.new_user()
        self.as_staff(user)
        api_key = self.new_api_key(user)

        self.basic_auth_client(api_key)
        response = self.client.delete(
            self.api_key_endpoint(), format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)
        self.assertFalse(
            ApiAppKey.objects.filter(
                key=api_key.key
            ).exists()
        )

    def test_read_api_key_method(self):
        """
        Tests HTTP GET for api key
        """
        self.as_staff(self.user)
        self.bearer_auth_client(self.token)

        response = self.client.get(self.api_key_endpoint())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, {
            'api_key': self.api_key.key
        })

    def test_create_api_key_permission_denied(self):
        """
        Tests request to endpoint fails for non admin user
        """
        self.as_non_staff(self.user)
        self.bearer_auth_client(self.token)
        response = self.client.post(
            self.api_key_endpoint(), format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.'
        })
