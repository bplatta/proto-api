import unittest
from django.test import tag
from rest_framework.exceptions import ValidationError

from utils.testing.base_test_case import BaseAPIDBTestCase
from api_auth.authentication.serializer import AuthenticationSerializer


@tag('db')
class AuthenticationTestCaseDB(BaseAPIDBTestCase):
    """
    Test authorization of request with DB hits
    """

    def test_serializer_authenticate_valid_email_password(self):
        """
        Test authentication with valid email password combination
        """
        serializer = AuthenticationSerializer(data=self.creds)
        serializer.is_valid()
        # self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data['user'].id, self.user.id)

    def test_serializer_authenticate_email_password_dne(self):
        """
        Test authentication email, password combination does not exist
        """
        creds = {
            'email': 'idont@exist.com',
            'password': 'whywontyoucallme'
        }
        serializer = AuthenticationSerializer(data=creds)
        try:
            serializer.is_valid(raise_exception=True)
            self.fail('Serializer did not raise validation error')
        except ValidationError as e:
            self.assertEqual(
                e.detail, {
                    'error': [
                        'Unable to authenticate with provided credentials.']
                })

    def test_serializer_authenticate_user_is_not_active(self):
        """
        Test authentication email, password combination does not exist
        """
        self.as_inactive_user(self.user)
        serializer = AuthenticationSerializer(data=self.creds)
        try:
            serializer.is_valid(raise_exception=True)
            self.fail('Serializer did not raise validation error')
        except ValidationError as e:
            self.assertEqual(
                e.detail, {
                    'error': [
                        'Unable to authenticate with provided credentials.']
                })


@tag('unit')
class AuthenticationTestCaseUnit(unittest.TestCase):
    """
    Test testing authentication serializer pre-db cases
    """

    def test_authentication_no_email(self):
        """
        Test authenticaton with no email fails
        """
        creds = {
            'password': 'whywontyoucallme'
        }
        serializer = AuthenticationSerializer(data=creds)
        try:
            serializer.is_valid(raise_exception=True)
            self.fail('Serializer did not raise validation error')
        except ValidationError as e:
            self.assertEqual(
                e.detail, {
                    'email': ['This field is required.']
                })

    def test_authentication_no_password(self):
        """
        Test authenticaton with no email fails
        """
        creds = {
            'email': 'this@will.com'
        }
        serializer = AuthenticationSerializer(data=creds)
        try:
            serializer.is_valid(raise_exception=True)
            self.fail('Serializer did not raise validation error')
        except ValidationError as e:
            self.assertEqual(
                e.detail, {
                    'password': ['This field is required.']
                })
