from django.conf import settings
from django.test import tag
from django.urls import reverse
from rest_framework import status

from utils.testing.base_test_case import BaseAPIDBTestCase
from api_auth.models import ExpiringToken


@tag('db')
class TestTokenViewTestCase(BaseAPIDBTestCase):
    """
    Test Token View operations
    """
    POST_FORMAT = 'json'
    view_namespace = 'token'
    app_namespace = 'Auth'
    reverse_route = '%s:%s' % (app_namespace, view_namespace)

    @classmethod
    def token_endpoint(cls):
        return reverse(cls.reverse_route)

    def test_successful_token_generation(self):
        """
        Tests Successful generation of token for requests from auth
        """
        user, creds = self.new_user()
        response = self.client.post(
            self.token_endpoint(), data=creds, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('access_token', response.data)
        self.assertTrue(
            ExpiringToken.objects.filter(
                key=response.data.get('access_token')
            ).exists()
        )
        self.assertEqual(
            response.data.get('expires_in__sec'),
            settings.TOKEN_EXPIRATION_SECONDS)

    def test_successful_reset_token_generation(self):
        """
        Tests Successful generation of token and deletion of old token
        """
        user, creds = self.new_user()
        # associate a token with this user. Should be deleted with new POST
        # to /auth/token/
        token = self.new_token(user)

        response = self.client.post(
            self.token_endpoint(), data=creds, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('access_token', response.data)
        # assert the new key exists
        self.assertTrue(
            ExpiringToken.objects.filter(
                key=response.data.get('access_token')
            ).exists()
        )
        # Assert the old key was deleted
        self.assertFalse(
            ExpiringToken.objects.filter(
                key=token.key
            ).exists()
        )
        self.assertEqual(
            response.data.get('expires_in__sec'),
            settings.TOKEN_EXPIRATION_SECONDS)

    def test_token_method_not_supported(self):
        """
        Tests HTTP method not supported returns 405
        """
        response = self.client.get(self.token_endpoint())
        self.assertEqual(response.data, {
            'detail': 'Method "GET" not allowed.'
        })
        self.assertEqual(
            response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_invalid_authentication_for_token(self):
        """
        Tests invalid credentials results in 400
        """
        creds = self.creds.copy()
        # change password to wrong password
        creds['password'] = 'thewrongpas'
        response = self.client.post(
            self.token_endpoint(), data=creds, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': ['Unable to authenticate with provided credentials.']
        })

    def test_authentication_with_deactivated_user(self):
        """
        Tests authentication with in-active user returns 400
        """
        self.as_inactive_user(self.user)
        response = self.client.post(
            self.token_endpoint(), data=self.creds, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': ['Unable to authenticate with provided credentials.']
        })

    def test_token_deletion_success(self):
        """
        Tests Successful deletion of user token
        """
        user, creds = self.new_user()
        token = self.new_token(user)

        self.bearer_auth_client(token)
        response = self.client.delete(self.token_endpoint())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNone(response.data)
        self.assertFalse(
            ExpiringToken.objects.filter(
                key=token.key
            ).exists()
        )
        self.de_auth_client(self.client)

    def test_token_deletion_permission_denied(self):
        """
        Tests permission denied error for DELETE request of Anon user
        """
        self.de_auth_client(self.client)
        response = self.client.delete(self.token_endpoint())

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.'
        })
