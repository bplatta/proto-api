from unittest.mock import MagicMock

from django.test import tag, override_settings
from django.contrib.auth.models import AnonymousUser
from rest_framework import status

from utils.testing.base_test_case import BaseAPIDBTestCase
from utils.exceptions import BaseAPIException

from api_auth.middleware import api_auth_middleware


@tag('db')
class AuthMiddlewareTestCase(BaseAPIDBTestCase):
    """
    Test authorization of request with DB hits
    """

    def test_auth_request_valid_token(self):
        """
        Test request with valid bearer auth retrieves user and token
        """
        request = self.new_request(
            '/some/route', auth_header=self._bearer_auth_header(self.token))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        req_user, req_token, req_api_key = final_request.authorize()

        self.assertEqual(req_user.uid, self.user.uid)
        self.assertEqual(req_token.key, self.token.key)
        self.assertIsNone(req_api_key)

    def test_auth_request_from_webserver_set(self):
        """
        Test request has attr webserver_referred set by middleware
        """
        request = self.new_request(
            '/some/route', auth_header=self._bearer_auth_header(self.token))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'webserver_referred'))
        self.assertFalse(final_request.webserver_referred)

    @override_settings(WEBSERVER_HOST='hostname')
    def test_auth_request_from_webserver_set_true(self):
        """
        Test request has attr webserver_referred set by middleware as true
        """
        request = self.new_request(
            '/some/route', auth_header=self._bearer_auth_header(
                self.token), extra_headers={
                'HTTP_HOST': 'hostname'
            })

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'webserver_referred'))
        self.assertTrue(final_request.webserver_referred)

    def test_auth_request_expired_token(self):
        """
        Test request with expired bearer auth token returns 400
        """
        user, creds = self.new_user()
        token = self.new_token(user)
        token.expire()
        request = self.new_request(
            '/some/route', auth_header=self._bearer_auth_header(token))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        try:
            # Call upstream auth
            final_request.authorize()
        except BaseAPIException as e:
            self.assertEqual(e.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(e.detail, {
                'error': 'Token has expired. Acquire new token with POST '
                         'to /auth/token/ endpoint, providing username '
                         'and password'
            })

    def test_auth_request_valid_api_key(self):
        """
        Test request with valid basic auth retrieves user and token
        """
        request = self.new_request(
            '/some/route', auth_header=self._basic_auth_header(self.api_key))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        req_user, req_token, req_api_key = final_request.authorize()

        self.assertEqual(req_user.uid, self.user.uid)
        self.assertEqual(req_api_key.key, self.api_key.key)
        self.assertIsNone(req_token)

    def test_auth_inactive_user_bearer_auth(self):
        """
        Test request with a valid bearer token but inactive user returns 400
        """
        user, creds = self.new_user()
        user.is_active = False
        user.save()
        token = self.new_token(user)
        request = self.new_request(
            '/meaningless/', auth_header=self._bearer_auth_header(token))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        try:
            final_request.authorize()
            self.fail('Authorize did not fail with inactive user')
        except BaseAPIException as e:
            self.assertEqual(e.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(e.detail, {
                'error': 'Invalid token provided'
            })

    def test_auth_inactive_user_basic_auth(self):
        """
        Test request with a valid bearer token but inactive user returns 400
        """
        user, creds = self.new_user()
        user.is_active = False
        user.save()
        api_key = self.new_api_key(user)
        request = self.new_request(
            '/meaningless/', auth_header=self._basic_auth_header(api_key))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        try:
            final_request.authorize()
            self.fail('Authorize did not fail with inactive user')
        except BaseAPIException as e:
            self.assertEqual(e.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(e.detail, {
                'error': 'Invalid API key provided'
            })

    def test_auth_request_invalid_token(self):
        """
        Test request with an invalid token raises
        """
        request = self.new_request(
            '/meaningless/', auth_header=self._bearer_auth_header('notreal'))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        try:
            final_request.authorize()
            self.fail('Authorize did not fail with invalid token')
        except BaseAPIException as e:
            self.assertEqual(e.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(e.detail, {
                'error': 'Invalid authentication provided'
            })

    def test_auth_request_invalid_api_key(self):
        """
        Test request with an invalid api key raises
        """
        request = self.new_request(
            '/meaningless/', auth_header=self._basic_auth_header('notathing'))

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        try:
            final_request.authorize()
            self.fail('Authorize did not fail with invalid api key')
        except BaseAPIException as e:
            self.assertEqual(e.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(e.detail, {
                'error': 'Invalid authentication provided'
            })

    def test_auth_anon_user(self):
        """
        Test request with no authentication sets user as Anonymous
        """
        request = self.new_request('/some/route')

        spy = MagicMock()
        run_middleware = api_auth_middleware(spy)
        response = run_middleware(request)

        # get_response called once
        self.assertEqual(len(spy.call_args_list), 1)

        # authorize method set on request
        final_request = spy.call_args_list[0][0][0]
        self.assertTrue(hasattr(final_request, 'authorize'))

        # Call upstream auth
        req_user, req_token, req_api_key = final_request.authorize()

        self.assertIsInstance(req_user, AnonymousUser)
        self.assertIsNone(req_token)
        self.assertIsNone(req_api_key)
