import base64
import unittest
from django.test import tag, RequestFactory

from api_auth.authorization import (
    parse_auth_header,
    decode_to_str,
    validate_header_parts,
    split_header,
    get_header,
    decode_parts,
    AuthenticationBreak,
    from_webserver,
    AUTHORIZATION_HEADER,
    HOST_HEADER)


@tag('unit')
class AuthorizationTestCaseUnit(unittest.TestCase):
    """
    Test authorization helper functions
    """
    @classmethod
    def setUpClass(cls):
        cls.request_factory = RequestFactory()

    def test_retrieve_authorization_header_from_request(self):
        """
        Test successful parsing of Auth Header from request
        """
        header = 'Bearer knasdfknajsndfkjankjfas'
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        parsed_header = get_header(request, AUTHORIZATION_HEADER)
        self.assertEqual(parsed_header, header.encode())  # expect bytestring

    def test_retrieve_host_header_from_request(self):
        """
        Test successful parsing of host Header from request
        """
        header = 'localhost'
        request = self.request_factory.get(
            '/some/route', HTTP_HOST=header)
        parsed_header = get_header(request, HOST_HEADER)
        self.assertEqual(parsed_header, header.encode())  # expect bytestring

    def test_request_from_webserver(self):
        """
        Test successful bool check if request from webserver
        """
        header = 'localhost'
        webserver_host = header
        request = self.request_factory.get(
            '/some/route', HTTP_HOST=header)
        self.assertTrue(
            from_webserver(request, webserver_host=webserver_host))

    def test_request_from_webserver_none(self):
        """
        Test successful bool check if request not from webserver
        """
        header = 'localhost'
        request = self.request_factory.get(
            '/some/route', HTTP_HOST=header)
        self.assertFalse(
            from_webserver(request, webserver_host=None))

    def test_decode_token_success(self):
        """
        Test decoding of token success
        """
        byte_token = b'lknasdfnlkasdlfklaksdnfllkasdfkl088h31o'
        str_token = 'lknasdfnlkasdlfklaksdnfllkasdfkl088h31o'
        self.assertEqual(decode_to_str(byte_token), str_token)

    def test_decode_token_fail(self):
        """
        Test decoding of token failure
        """
        bad_token = b'lnl\x80kasdlfklaksdnfllkasdfkl088h31o'
        try:
            decode_to_str(bad_token)
            self.fail('`decode_token` did not raise with bad character')
        except AuthenticationBreak as e:
            self.assertEqual(e.message, 'Str contains invalid characters')

    def test_invalid_header_keyword(self):
        """
        Test invalid header keyword for Bearer token
        """
        header = b'Token knasdfknajsndfkjankjfas'
        try:
            validate_header_parts(split_header(header))
            self.fail('`validate_header_parts` did not raise')
        except AuthenticationBreak as e:
            self.assertEqual(
                e.message,
                'Invalid Authorization header. '
                '`Bearer <Token string>` required.')

    def test_header_no_creds_none(self):
        """
        Test header with no creds returns None for token string
        """
        request = self.request_factory.get('/some/route')
        auth_type, auth_data = parse_auth_header(request)
        self.assertIsNone(auth_type)
        self.assertIsNone(auth_data)

    def test_header_empty_creds_none(self):
        """
        Test header with empty cred string returns None for token string
        """
        header = ''
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        auth_type, auth_data = parse_auth_header(request)
        self.assertIsNone(auth_type)
        self.assertIsNone(auth_data)

    def test_header_empty_bearer_cred_fails(self):
        """
        Test header with empty cred string returns None for token string
        """
        header = 'Bearer'
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        try:
            parse_auth_header(request)
            self.fail('Empty bearer credential did not throw')
        except AuthenticationBreak as e:
            self.assertEqual(
                e.message,
                'Invalid Authorization header. '
                '`Bearer <Token string>` required.')

    def test_header_empty_basic_auth_cred_fails(self):
        """
        Test header with empty cred string for Basic authentication fails
        """
        header = 'Basic'
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        try:
            parse_auth_header(request)
            self.fail('Empty bearer credential did not throw')
        except AuthenticationBreak as e:
            self.assertEqual(
                e.message,
                'Invalid Authorization header. '
                '`Bearer <Token string>` required.')

    def test_invalid_bearer_credential_string(self):
        """
        Test invalid credential token raises
        """
        header = b'Bearer knasdfknajs ndfkja nkjfas'
        try:
            validate_header_parts(split_header(header))
            self.fail('`validate_header_parts` did not raise')
        except AuthenticationBreak as e:
            self.assertEqual(
                e.message,
                'Invalid credentials provided. '
                'Token should not contain spaces.')

    def test_valid_header_parts_bearer(self):
        """
        Test valid token header parsing for Bearer token
        """

        header = b'Bearer xyz123'
        bearer, cred = validate_header_parts(split_header(header))
        self.assertEqual(bearer, b'Bearer')
        self.assertEqual(cred, b'xyz123')

    def test_valid_header_parts_basic(self):
        """
        Test valid token header parsing for Basic Auth
        """

        header = b'Basic asdfasdj920j2ef='
        bearer, cred = validate_header_parts(split_header(header))
        self.assertEqual(bearer, b'Basic')
        self.assertEqual(cred, b'asdfasdj920j2ef=')

    def test_invalid_basic_auth_creds_non_base64encoded(self):
        """
        Test invalid Basic Auth credentials not base64 encoded
        """

        header_parts = [b'Basic', b'asd']
        try:
            decode_parts(header_parts)
            self.fail('Basic Auth decoding did not throw exception')
        except AuthenticationBreak as e:
            self.assertEqual(
                e.message,
                'Invalid authentication for basic auth. '
                'Credential must be base64 encoded.'
            )

    def test_valid_decode_header_bearer_auth(self):
        """
        Test valid decoding of Bearer auth header data
        """
        header_parts = [b'Bearer', b'tokenkey']
        auth_type, auth_data = decode_parts(header_parts)
        self.assertEqual(auth_type, 'bearer')
        self.assertEqual(auth_data, {
            'key': 'tokenkey'
        })

    def test_valid_parse_header_cred_bearer_auth(self):
        """
        Tests successful parsing of key and auth type for Bearer Header
        """
        header = 'Bearer knasdfknajsndfkjankjfas'
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        auth_type, auth_data = parse_auth_header(request)
        self.assertEqual(auth_type, 'bearer')
        self.assertEqual(auth_data, {
            'key': 'knasdfknajsndfkjankjfas'
        })

    def test_valid_parse_header_cred_basic_auth(self):
        """
        Tests successful parsing of key and auth type for BasicAuth header
        """
        creds = base64.b64encode(b'10239012390a').decode()
        header = 'Basic %s' % creds
        request = self.request_factory.get(
            '/some/route', HTTP_AUTHORIZATION=header)
        auth_type, auth_data = parse_auth_header(request)
        self.assertEqual(auth_type, 'basic')
        self.assertEqual(auth_data, {
            'key': '10239012390a'
        })
