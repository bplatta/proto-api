.PHONY: help test clean build build-db run start restart stop sandbox test-unit lint

major_version := 0
minor_version := 1
build_number := 0
version := $(major_version).$(minor_version).$(build_number)

help:
	@echo
	@echo "PROTO-API makefile"
	@echo "usage: make [target] ..."
	@echo
	@egrep "^# target:" [Mm]akefile
	@echo

# target: build - Build a development environment.
build:
	$(MAKE) --file=build/Makefile version=$(version) build-dev

# target: build-db - Migrate database container.
build-db:
	$(MAKE) --file=build/Makefile version=$(version) build-db
	make stop

# target: start - Run local API with dependencies (postgres and nginx)
start:
	docker-compose -f deploy/docker-compose.dev.yaml up -d && docker logs deploy_api_1 -f;

# target: logs - Gather API Logs from running api container
logs:
	docker logs deploy_api_1 -f

# target: restart - Restart API stack
restart:
	docker-compose -f deploy/docker-compose.dev.yaml restart && docker logs deploy_api_1 -f;

# target: stop - stop current API stack
stop:
	docker-compose -f deploy/docker-compose.dev.yaml stop

# target: sandbox - jump into an api sandbox with ports open
sandbox:
	docker-compose -f deploy/docker-compose.dev.yaml run --service-ports api /bin/bash

# target: lint - Lint project with pep8 standards
lint:
	docker-compose -f deploy/docker-compose.test.yaml run api-unit pep8 --exclude=env,__pycache__,**/migrations/* --count .

# target: test-unit - Run the applications unit tests only
test-unit:
	@echo
	@echo "Running application unit tests ======= "
	@echo
	docker-compose -f deploy/docker-compose.test.yaml run -e DJANGO_LOG_LEVEL=ERROR api-unit \
	python3 manage.py test --testrunner=utils.testing.no_db_setup_runner.NoDbSetupTestRunner --parallel --tag=unit

# target: test - Run all tests for API
test:
	@echo
	@echo "Running django API tests ======================="
	@echo
	docker-compose -f deploy/docker-compose.dev.yaml run -e DJANGO_LOG_LEVEL=ERROR api \
	bash -c "python3 manage.py test --testrunner=utils.testing.no_db_setup_runner.NoDbSetupTestRunner --parallel --tag=unit && python3 manage.py test --parallel --exclude-tag=unit --keepdb"
	make stop

# target: clean - Remove local postgres data
clean:
	@echo
	@echo "Cleaning local pg-data. Hope you meant to do this!"
	@echo
	rm -rf pgdata-local/*