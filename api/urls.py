from django.conf import settings
from django.conf.urls import url, include
from django.views.generic import RedirectView
from django.utils.module_loading import import_string

from api_management.ping_view import PingView


def add_app_routes(routes):
    """
    Imports the application routes using settings route
    :return: list of url() routes
    """
    apps = settings.API_APPS_FOR_DEPLOY

    for app in apps:
        routes += import_string('.'.join([app, 'urls', 'urlpatterns']))

    return routes

#########################
# API Server URL routes #
#########################
urlpatterns = [
    url(r'^$', PingView.as_view(), name='root'),
    url(r'^auth/', include('api_auth.urls', namespace='Auth')),
    url(r'^users/', include('users.urls', namespace='Users'))
]

# Add Application routes
urlpatterns = add_app_routes(urlpatterns)
