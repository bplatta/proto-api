
class AppSettings:
    """
    Application settings object
    """
    def __init__(
            self, app_env_name, app_modules_list, extra_settings_file=None):
        self.app_env_name = app_env_name  # loaded for os.environ['APP']
        self.apps = app_modules_list
        self.extra_settings_file = extra_settings_file

############################
# Currently supported Apps #
############################

FruitfulForums = AppSettings(
    'fruitful_forums',
    [
        'fruitful_forums',
        'fruitful_forums.forums',
        'fruitful_forums.experts'
    ],
    'fruitful_forums.settings'
)


def configure_for_app(*app_names):
    """
    Provides app specific settings information
    :param app_names: list of str
    :return: [{
        app_name: AppSettings object,
        ...
    }, ... ]
    """
    applications = {
        FruitfulForums.app_env_name: FruitfulForums
    }

    for app_name in app_names:
        assert app_name in applications, (
            'App `%s` is not configured for deployment' % app_name)

    return [applications.get(app_name) for app_name in app_names]
