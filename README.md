# Prototyping API - 0.1

The proto-api is a django rest API built for easy prototyping of restful collections

#### Versioning: x.y.z
x - major release;
y - minor release;
z - build number

#### Project Dependencies
- [docker](https://docs.docker.com/engine/understanding-docker/)
 - This includes: docker engine, docker machine and docker compose
 - disclaimer: only been tested with OSX
- [postgres](https://www.postgresql.org/download/)
 - isn't absolutely necessary as db is run in a container
 - I installed with homebrew
 - using [postgres 9.5.5](https://www.postgresql.org/docs/9.5/static/)
 
## Adding new [application settings](api/proto_apps.py)
- Add a new AppSettings object
- Add to applications map in configure_for_app

## Local development stack - view make commands with command `make` 
#### `make build`
Builds local API stack

- builds a postgres db for the api on your machine
- builds django api container image
- builds nginx container image
- spins up postgres container
- migrates postgres DB
- creates admin user: u - root@root.com, p - testpass

#### `make start` 
Starts local API stack with docker compose

#### `make logs` 
Tail logs of the locally running API container

#### `make restart` 
Restarts local API stack with docker compose

#### `make stop` 
Stops local API stack with docker compose

#### `make sandbox` 
Create and jump into a running API container, with ports open, accompanied by postgres container

#### `make test` and `make test-unit`
Spins up local API container with a DB and runs unit and integration tests. 
`test-unit` only runs unit tests. `test` runs both unit and integration tests

#### `make lint` 
Spins up local API container and pep8 lints

#### Troubleshooting local dev
- known bugs are in comments - can search project for `_BUG_` string
- rebuilding the DB can sometimes not work because the postgres container
  doesnt spin up fast enough, or at all. I have the django manage.py commands wrapped to retry 
  on account of DB operational failures. It will sometimes take a few seconds. This is because although there is a 
  docker dependency, docker doesnt do anything like ensure the process is up on postgres before spinning up the api. 
  Usually I do the following, then rerun:
 - `make stop`  # stop all containers
 - remove all stopped containers (see below for command)
 - remove local data `rm -rf pgdata-local/*`
- packer can sometimes fail unexpectedly, if this happens, I suggest just rerunning
- if docker fails due to a port being taken, restart Docker engine
- restarting Docker Engine is a good step with any Docker related problems

#### Useful commands
- `make clean && make build-db` - delete local DB and rebuild fresh DB, adding 1 root user
- `docker ps -a -q | xargs docker rm` - remove all stopped containers
- `docker images -q -a -f dangling=true | xargs docker rmi` - remove dangling docker images
- `dockerbash {container ID or name}` - jump into a running container:
```
dockerbash () {
        docker exec -it $1 /bin/bash
}
```

## Repo structure

### build
Contains dockerfiles for the containers needed to run an API

### deploy
Contains docker compose configs and any necessary deploy scripts

## Spec'ing software: ApiaryIO apiblueprint

Markdown tutorial: https://apiblueprint.org/documentation/tutorial.html

Publishing updated versions of the spec is very easy:
1. Install apiary gem `gem install apiaryio`
2. Make sure APIARY_API_KEY is set in env
3. Make updates to specs/yourmarkdown.md
4. Publish with `apiary publish --api-name=fruitfulforums --path=specs/fruitfulforums.apib`

## Current Prototypes

#### FruitfulForums

 - Forum application that encourages fruitful dialogue
 - docs: http://docs.fruitfulforums.apiary.io/
 - spec: specs/fruitfulforums.md
 - Can create local data by jumping onto box (`make sandbox`) and running `python manage.py ff_populate_local_db`