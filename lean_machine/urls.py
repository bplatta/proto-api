from django.conf.urls import url
from lean_machine.views.apps_view import AppsView

urlpatterns = [
    url(r'^apps/', AppsView.as_view(), name=AppsView.namespace)
]
