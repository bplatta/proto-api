from django.conf import settings

from rest_framework.response import Response

from utils.api.base_view import BaseAuthView


class AppsView(BaseAuthView):
    """
    Apps View - manages Lean Application data
    """
    namespace = 'lean_machine'
    http_method_names = ['get']
    permission_classes = []

    def get(self, request):
        """
        Lists currently supported applications
        :param request: Request object
        :return: Response object
        """
        return Response()
