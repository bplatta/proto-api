from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class LeanApps(models.Model):
    """
    Lean Application model - reference table for applications
    """
    pass
