from django.apps import AppConfig


class LeanMachineConfig(AppConfig):
    name = 'lean_machine'
