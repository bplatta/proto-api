from django.conf.urls import url, include

from fruitful_forums.experts.urls import NAMESPACE as EXPERTS_NAMESPACE
from fruitful_forums.forums.urls import NAMESPACE as FORUMS_NAMESPACE

urlpatterns = [
    url(r'^forums/',
        include('fruitful_forums.forums.urls', namespace=FORUMS_NAMESPACE)),
    url(r'^experts/',
        include('fruitful_forums.experts.urls', namespace=EXPERTS_NAMESPACE))
]
