from django.test import tag
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_resources_view import (
    ForumResourcesView)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumResourcesGetIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forum Resources Collection GET operations
    """
    parent_none = None

    view_under_test = ForumResourcesView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumResourcesGetIntegTestCase, cls).tearDownClass()

    def test_list_forum_resources_owner_success(self):
        """
        Test successful LIST on forum resources
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        # create a couple forum resources
        resource1 = self.forum_data.create_forum_resource(forum['obj'])
        resource2 = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        resources_resp = dict(response.data)
        self.assertIn('resources', resources_resp)
        self.assertEqual(resources_resp['count'], 2)
        resources = [r['url'] for r in resources_resp['resources']]
        for r in [resource1, resource2]:
            self.assertIn(r['url'], resources)

    def test_read_forum_resource_not_supported(self):
        """
        Test reading a forum resource is not supported
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.resource_endpoint(
                forum_id=forum['uid'], resource_number='123'))

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(response.data, {
            'error': 'Read operation not permitted on resource. '
                     '`get` request only allowed on '
                     'forum-resource collection.'
        })

    def test_list_forum_resources_unauthed_fail(self):
        """
        Test unauthenticated user can not view forum resources
        """
        forum = self.forum_data.get_any_forum()
        client = self.de_auth_client(self.client)

        response = client.get(self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_list_forum_resources_public_nonowner_success(self):
        """
        Test nonowner of forum cant view resources
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__public(new_user)
        resource = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        resources_resp = dict(response.data)
        self.assertIn('resources', resources_resp)
        self.assertEqual(resources_resp['count'], 1)
        self.assertEqual(dict(resources_resp['resources'][0]), {
            'resource_number': 1,
            'url': resource['url'],
            'name': resource['name'],
            'summary': resource['summary']
        })

    def test_list_forum_resources_private_nonowner_fail(self):
        """
        Test nonowner of forum can't view resources of private forum
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__private(new_user)
        resource = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)

    def test_list_forum_resources_private_nonowner_password_success(self):
        """
        Test nonowner of forum can view resources of private forum with ps
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__private(new_user)
        resource = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(
                forum_id=forum['uid'],
                q={'password': forum['password']}
            )
        )
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        resources_resp = dict(response.data)
        self.assertIn('resources', resources_resp)
        self.assertEqual(resources_resp['count'], 1)
        self.assertEqual(dict(resources_resp['resources'][0]), {
            'resource_number': 1,
            'url': resource['url'],
            'name': resource['name'],
            'summary': resource['summary']
        })
