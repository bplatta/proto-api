from django.test import tag
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_participants_view import (
    ForumParticipantsView)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumParticipantsGetIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forum Participants Collection GET operations
    """
    parent_none = None

    view_under_test = ForumParticipantsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumParticipantsGetIntegTestCase, cls).tearDownClass()

    def test_list_forum_participants_owner_success(self):
        """
        Test successful LIST on forum participants
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])
        expected_participants = [
            (1, self.user.get_full_name()),
            (2, participant2['name'])
        ]
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        resources_resp = dict(response.data)
        self.assertIn('participants', resources_resp)
        self.assertEqual(resources_resp['count'], 2)

        participants = [
            (p['participant_number'], p['name'])
            for p in resources_resp['participants']
        ]

        for ep in expected_participants:
            self.assertIn(ep, participants)

    def test_list_forum_participants_nonowner_public_success(self):
        """
        Test listing forum participants for public forum succeeds for nonowner
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__public(
            new_user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])
        expected_participants = [
            (1, new_user.get_full_name()),
            (2, participant2['name'])
        ]
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        resources_resp = dict(response.data)
        self.assertIn('participants', resources_resp)
        self.assertEqual(resources_resp['count'], 2)

        participants = [
            (p['participant_number'], p['name'])
            for p in resources_resp['participants']
        ]

        for ep in expected_participants:
            self.assertIn(ep, participants)

    def test_list_forum_participants_nonowner_private_fails(self):
        """
        Test listing forum participants for private forum fails for nonowner
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__private(
            new_user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)

    def test_list_forum_participants_unauthed_fail(self):
        """
        Test listing forum participants if unauthed fails
        """
        forum = self.forum_data.get_any_forum()
        client = self.de_auth_client(self.client)

        response = client.get(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_list_private_forum_participants_nonowner_password_succeeds(self):
        """
        Test listing forum participants for private forum fails for password
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__private(new_user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])
        expected_participants = [
            (1, new_user.get_full_name()),
            (2, participant2['name'])
        ]
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(
                forum_id=forum['uid'],
                q={'password': forum['password']}
            )
        )
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        resources_resp = dict(response.data)
        self.assertIn('participants', resources_resp)
        self.assertEqual(resources_resp['count'], 2)

        participants = [
            (p['participant_number'], p['name'])
            for p in resources_resp['participants']
        ]

        for ep in expected_participants:
            self.assertIn(ep, participants)

    def test_list_private_forum_participants_participant_succeeds(self):
        """
        Test listing private forum participants works for participant
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__private(new_user)
        self.forum_data.create_forum_participant(forum['obj'], user=self.user)
        expected_participants = [
            (1, new_user.get_full_name()),
            (2, self.user.get_full_name())
        ]
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(
                forum_id=forum['uid']
            )
        )
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        resources_resp = dict(response.data)
        self.assertIn('participants', resources_resp)
        self.assertEqual(resources_resp['count'], 2)

        participants = [
            (p['participant_number'], p['name'])
            for p in resources_resp['participants']
        ]

        for ep in expected_participants:
            self.assertIn(ep, participants)

    def test_read_forum_participant_not_supported(self):
        """
        Test read on forum participant fail not supported
        """
        forum = self.forum_data.get_any_forum()
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        # participant number doesnt matter, should 400 before checking
        response = self.client.get(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=1
            )
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': 'Read operation not permitted on resource. '
                     '`get` request only allowed on '
                     'forum-participant collection.'
        })
