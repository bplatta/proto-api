from django.test import tag
from fruitful_forums.forums.models import Forum
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forums_view import ForumsView
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumsDeleteIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forums Collection Delete operations
    """
    parent_none = None

    view_under_test = ForumsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumsDeleteIntegTestCase, cls).tearDownClass()

    def test_delete_resource_fail_unauthed_user(self):
        """
        Test unauthenticated user DELETE request fails
        """
        client = self.de_auth_client(self.client)
        response = client.delete(
            self.resource_endpoint(uid='notgonnamatter'))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_valid_request(self):
        """
        Test valid deletion of a forum resource
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertFalse(
            Forum.objects.filter(
                uid=forum['uid']
            ).exists()
        )

    def test_delete_forum_nonowner_fail(self):
        """
        Test forum delete failure for forum that DNE
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        new_user, creds = self.new_user()
        token = self.new_token(new_user)
        self.bearer_auth_client(token)
        self.as_non_staff(new_user)

        response = self.client.delete(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_does_not_exist(self):
        """
        Test user that is not the creator of forum cant delete it
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(uid='111dontexist'))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, {
            'error': 'Forum with ID `111dontexist` does not exist'
        })

    def test_delete_forum_nonowner_admin_user_success(self):
        """
        Test a nonowner admin can delete any forum
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__public(
            new_user)
        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
