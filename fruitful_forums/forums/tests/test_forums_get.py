from django.test import tag, override_settings
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forums_view import ForumsView
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from fruitful_forums.utils.asserter import TestAssertionHelper
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumsGetIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forums Collection GET operations
    """
    parent_none = None

    view_under_test = ForumsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    assert_helper = TestAssertionHelper()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumsGetIntegTestCase, cls).tearDownClass()

    def test_list_forums_fail_unauthed_user(self):
        """
        Test unauthenticated user can not view forums
        """
        client = self.de_auth_client(self.client)

        response = client.get(self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_read_public_forum__positive(self):
        """
        Test successful GET on forum resource
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__public(
            new_user)
        validate_data = self.assert_helper.assert_read_forum(
            test_case=self, forum_data=forum, participant_data=[participant])
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        validate_data(response.data)

    def test_read_forum_unauthed_user__negative(self):
        """
        Test unauthenticated user GET request fails
        """
        client = self.de_auth_client(self.client)
        response = client.get(
            self.resource_endpoint(uid='notgonnamatter'))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_read_forum_private_nonparticipant__negative(self):
        """
        Tests that private forum cant be viewed by nonparticipant
        """
        forum, participant = self.forum_data.create_single_forum__private(
            self.user)
        new_user, creds = self.new_user()
        new_token = self.new_token(new_user)
        self.bearer_auth_client(new_token)
        self.as_non_staff(new_user)

        response = self.client.get(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)

    def test_read_forum_private_as_participant__positive(self):
        """
        Test successful read of private forum if participant
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__private(
            new_user)
        # make user a participant
        participant2 = self.forum_data.create_forum_participant(
            forum['obj'], self.user)
        validate_data = self.assert_helper.assert_read_forum(
            test_case=self, forum_data=forum,
            participant_data=[participant, participant2])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        validate_data(response.data)

    def test_read_forum_private_password__positive(self):
        """
        Test successful read of private forum with password
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__private(
            new_user)
        validate_data = self.assert_helper.assert_read_forum(
            test_case=self, forum_data=forum, participant_data=[participant])
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(
            self.resource_endpoint(
                uid=forum['uid'], q={'password': forum['password']}))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        validate_data(response.data)

    def test_read_private_forum_as_admin__negative(self):
        """
        Test private forum cant be read by any admin user either
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__private(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        response = self.client.get(
            self.resource_endpoint(uid=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)

    def test_list_forums__positive(self):
        """
        Test LIST request displays proper subset of forum data
        """
        new_user, creds = self.new_user()
        self.forum_data.assure_forums(new_user, 2)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        self.assert_helper.assert_list_forum_data_structure(
            self, response.data, min_expected_count=2)

    @override_settings(WEBSERVER_HOST='hostname')
    def test_list_forums_as_admin_fromwebserver(self):
        """
        Test LIST displays subset of forum data if admin and from webserver
        """
        # this forum should not show up in results
        forum_p, participant = self.forum_data.create_single_forum__private(
            self.user)
        new_user, creds = self.new_user()
        self.forum_data.assure_forums(new_user, 2)

        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        response = self.client.get(
            self.collection_endpoint(), HTTP_HOST='hostname')

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        self.assert_helper.assert_list_forum_data_structure(
            self, response.data, min_expected_count=2)

        # assert none of the forums returned were were staff and private
        for forum in response.data['forums']:
            self.assertFalse(forum['id'] == forum_p['uid'])

    def test_list_forums_filter_private__positive(self):
        """
        Test LIST request displays filters private forums
        """
        new_user, creds = self.new_user()
        min_public_forums = 2
        # create a private forum
        private_forum, p1 = self.forum_data.create_single_forum__private(
            new_user)
        # assure public forums exist
        self.forum_data.assure_forums(new_user, min_count=min_public_forums)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)
        self.assert_helper.assert_list_forum_data_structure(
            self, response.data, min_expected_count=min_public_forums)

        forum_ids = [f['id'] for f in response.data['forums']]
        self.assertNotIn(private_forum['uid'], forum_ids)

    def test_list_forums_private_as_owner__positive(self):
        """
        Test LIST request displays private forum if user is owner
        """
        new_user, creds = self.new_user()
        private_forum, p1 = self.forum_data.create_single_forum__private(
            self.user)
        min_public_forums = 2
        self.forum_data.assure_forums(new_user, min_count=min_public_forums)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.get(self.collection_endpoint())

        self.assertEqual(
            response.status_code, status.HTTP_200_OK, response.data)

        self.assert_helper.assert_list_forum_data_structure(
            self, response.data, min_expected_count=min_public_forums + 1)

        forum_ids = [f['id'] for f in response.data['forums']]
        self.assertIn(private_forum['uid'], forum_ids)
