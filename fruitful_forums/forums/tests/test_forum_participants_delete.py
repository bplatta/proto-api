from django.test import tag
from fruitful_forums.forums.models import ForumParticipant
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_participants_view import (
    ForumParticipantsView)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumParticipantsDeleteIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forum participants Collection Delete operations
    """
    parent_none = None

    view_under_test = ForumParticipantsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumParticipantsDeleteIntegTestCase, cls).tearDownClass()

    def test_delete_forum_participant_owner_success(self):
        """
        Test successful Delete on forum participant by forum owner
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant2['participant_number']
            )
        )

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertFalse(ForumParticipant.objects.filter(
            id=participant2['id']
        ).exists())

    def test_delete_forum_participant_owner_self_fail(self):
        """
        Test Delete of first forum participant (owner) fails
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=1
            )
        )

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(response.data, {
            'error': 'Forum participant `1` is the forum owner '
                     'and may not be deleted'
        })

    def test_delete_forum_participant_nonowner_fail(self):
        """
        Test deleting forum participant by nonowner fails
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__public(
            new_user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant2['participant_number']
            )
        )

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_participant_unauthed_fail(self):
        """
        Test deleting a forum participant when unauthed fails
        """
        client = self.de_auth_client(self.client)

        response = client.delete(
            self.resource_endpoint(
                forum_id='12903-123lk0lk',
                participant_number='123'))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_participant_does_not_exist_404(self):
        """
        Test deleting a forum participantthat does not exist fails
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=100))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, {
            'error': 'Participant with ID `100` does not exist'
        })

    def test_delete_forum_participant_none_provided(self):
        """
        Test deleting request on participant collection fails
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.collection_endpoint(forum_id=forum['uid']))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, {
            'error': 'No participant_number was provided.'
        })
