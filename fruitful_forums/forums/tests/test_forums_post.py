from random import choice
from django.contrib.auth.hashers import check_password
from django.test import tag
from rest_framework import status

from fruitful_forums.forums.models import Forum
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forums_view import ForumsView
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from fruitful_forums.utils.asserter import TestAssertionHelper
from utils.testing.base_test_case import BaseAPIAllViewTestCase


@tag('db')
class ForumsPostIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forums Collection POST operations
    """
    parent_none = None

    POST_FORMAT = 'json'

    view_under_test = ForumsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    assert_helper = TestAssertionHelper()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumsPostIntegTestCase, cls).tearDownClass()

    def test_create_forum_unauthed_failed(self):
        """
        Test creating forum fails if user is unauthed
        """
        client = self.de_auth_client(self.client)

        response = client.post(
            self.collection_endpoint(), data={}, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_create_forum_success_authed_user(self):
        """
        Test creating a forum for an authed api user succeeds
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_post_data()
        post_data['resources'] = [
            self.forum_data.rand_forum_resource_data(resource_number=None)
        ]
        assert_on_create = self.assert_helper.assert_post_forum(
            self, post_data)
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        uid = response.data['id']

        new_forum = Forum.objects.get(uid=uid)
        creator = new_forum.creator_participant
        assert_on_create(new_forum)
        self.assertEqual(creator.user.uid, self.user.uid)

    def test_create_forum_success_anon_admin_user(self):
        """
        Test creating a forum for anonymous user by admin
        """
        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        post_data = self.forum_data.rand_forum_post_data(anon_user=True)
        assert_on_create = self.assert_helper.assert_post_forum(
            self, post_data)
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        uid = response.data['id']

        new_forum = Forum.objects.get(uid=uid)
        assert_on_create(new_forum, new_forum.creator_participant)

    def test_create_forum_fail_for_anon_non_admin(self):
        """
        Test creating a forum for anonymous user by non-admin fails
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_post_data(anon_user=True)
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': ['Only admin can create Forums on behalf '
                      'of anonymous users.']
        })

    def test_create_forum_missing_req_fields(self):
        """
        Test creating a forum fails if missing required fields
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        pop_fields = ['topic', 'goal', 'type']

        missing_field = choice(pop_fields)
        post_data = self.forum_data.rand_forum_post_data()
        post_data.pop(missing_field)
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            missing_field: ['This field is required.']
        })

    def test_create_forum_anon_user_missing_fields(self):
        """
        Test creating a forum for anonymous user fails if missing fields
        """
        self.bearer_auth_client(self.token)
        self.as_staff(self.user)
        pop_fields = ['name', 'user_password']

        missing_field = choice(pop_fields)
        post_data = self.forum_data.rand_forum_post_data(anon_user=True)
        post_data.pop(missing_field)
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            missing_field: ['This field is required if creating forum on '
                            'behalf of unauthenticated user.']
        })

    def test_create_private_forum_missing_password(self):
        """
        Test creating a private forum fails if missing password
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        post_data = self.forum_data.rand_forum_post_data(
            forum_type=Forum.PRIVATE_FORUM)
        post_data.pop('forum_password')
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'forum_password': ['A `forum_password` must be provided if '
                               'forum type is private']
        })

    def test_create_forum_bad_data_types(self):
        """
        Test creating a forum fails with bad data types
        """
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        post_data = self.forum_data.rand_forum_post_data(
            forum_type=Forum.PRIVATE_FORUM)
        post_data['max_participants'] = 'notaninteger'
        response = self.client.post(
            self.collection_endpoint(),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'max_participants': ['A valid integer is required.']
        })

    def test_update_forum_invalid_fields(self):
        """
        Test updating invalid fields for a forum fails
        """
        forum, p = self.forum_data.create_single_forum__public(user=self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        invalid_field = choice([
            'name', 'phone_number', 'user_password', 'email'])
        post_data = {
            invalid_field: self.forum_data.randomizer.generate_field(str)
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            invalid_field: ['Invalid field for update operation. '
                            'Please update (post) the '
                            'relevant related forum resource.']

        }, post_data)

    def test_update_forum_fields_success(self):
        """
        Test updating valid update fields succeeds
        """
        forum, p = self.forum_data.create_single_forum__public(user=self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        valid_field = choice([
            ('topic', str), ('goal', str),
            ('description', str), ('max_participants', int)
        ])
        post_data = {
            valid_field[0]: self.forum_data.randomizer.generate_field(
                valid_field[1])
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_forum_tags_success(self):
        """
        Test updating tags for forum succeeds
        """
        forum, p = self.forum_data.create_single_forum__public(user=self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = {
            'tags': [
                self.forum_data.randomizer.generate_field(str),
                self.forum_data.randomizer.generate_field(str)
            ]
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        forum = Forum.objects.get(uid=forum['uid'])
        self.assertEqual(forum.tags, post_data['tags'])

    def test_update_forum_private_no_password_fails(self):
        """
        Test updating type of forum to private without password fails
        """
        forum, p = self.forum_data.create_single_forum__public(user=self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        post_data = {
            'type': Forum.PRIVATE_FORUM
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'forum_password': ['A `forum_password` must be provided if '
                               'forum type is private']
        })

    def test_update_forum_password_succeeds(self):
        """
        Test updating password for forum succeeds
        """
        forum, p = self.forum_data.create_single_forum__private(user=self.user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        post_data = {
            'forum_password': self.forum_data.randomizer.generate_field(str)
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        forum_instance = Forum.objects.get(uid=forum['uid'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(check_password(
            post_data['forum_password'],
            forum_instance.password
        ))

    def test_update_forum_non_owner_fails(self):
        """
        Test updating a forum by non-owner fails
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__public(user=new_user)
        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)
        post_data = {
            'topic': self.forum_data.randomizer.generate_field(str)
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_forum_non_owner_as_admin_succeeds(self):
        """
        Test updating a forum by non-owner admin succeeds
        """
        new_user, creds = self.new_user()
        forum, p = self.forum_data.create_single_forum__public(user=new_user)
        self.bearer_auth_client(self.token)
        self.as_staff(self.user)
        post_data = {
            'topic': self.forum_data.randomizer.generate_field(str)
        }

        response = self.client.post(
            self.resource_endpoint(uid=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            Forum.objects.get(uid=forum['uid']).topic,
            post_data['topic'])
