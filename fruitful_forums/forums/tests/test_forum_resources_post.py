from random import choice

from django.test import tag
from rest_framework import status

from fruitful_forums.forums.models import ForumResource
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_resources_view import (
    ForumResourcesView
)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase


@tag('db')
class ForumResourcesPostIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forums participation Collection POST operations
    """
    parent_none = None

    POST_FORMAT = 'json'

    view_under_test = ForumResourcesView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumResourcesPostIntegTestCase, cls).tearDownClass()

    def test_create_forum_resource_unauthed__negative(self):
        """
        Test creating forum resource fails if user is unauthed
        """
        client = self.de_auth_client(self.client)

        response = client.post(
            self.collection_endpoint(forum_id='123-123-123'),
            data={'url': 'dont', 'name': 'matter'}, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_create_forum_resource_owner__positive(self):
        """
        Test creating a forum resource by the owner succeeds
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        self.assertTrue(ForumResource.objects.filter(
            forum__uid=forum['uid'],
            resource_number=response.data['id']
        ).exists())

    def test_create_private_forum_resource_owner__positive(self):
        """
        Test owner can create resource for a private forum without password
        """
        forum, participant1 = self.forum_data.create_single_forum__private(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        self.assertTrue(ForumResource.objects.filter(
            forum__uid=forum['uid'],
            resource_number=response.data['id']
        ).exists())

    def test_create_forum_resource_admin__positive(self):
        """
        Test a resource can be created by admin
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        self.assertTrue(ForumResource.objects.filter(
            forum__uid=forum['uid'],
            resource_number=response.data['id']
        ).exists())

    def test_create_forum_resource_nonowner__negative(self):
        """
        Test a resource cant be created by nonowner (of forum)
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def create_forum_resource_max_reached__negative(self):
        """
        Test fourm resource cant be created if max number reached
        """
        self.fail()

    def test_create_forum_resource_participant__negative(self):
        """
        Test resource cant be created by participant with proper err message
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)
        participant2 = self.forum_data.create_forum_participant(
            forum['obj'], user=self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_update_forum_resource_owner__negative(self):
        """
        Test resource cant be updated even by owner with proper err message
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)
        resource = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_forum_resource_data(
            resource_number=None)
        response = self.client.post(
            self.resource_endpoint(
                forum_id=forum['uid'],
                resource_number=1
            ),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(response.data, {
            'error': 'Update operation not permitted on resource. '
                     'Please delete resource and create a new one.'
        })
