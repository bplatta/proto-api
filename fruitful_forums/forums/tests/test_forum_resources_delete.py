from django.test import tag
from fruitful_forums.forums.models import ForumResource
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_resources_view import (
    ForumResourcesView)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.base_test_case import BaseAPIAllViewTestCase
from rest_framework import status


@tag('db')
class ForumResourcesDeleteIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forum Resources Collection Delete operations
    """
    parent_none = None

    view_under_test = ForumResourcesView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumResourcesDeleteIntegTestCase, cls).tearDownClass()

    def test_delete_forum_resource_success(self):
        """
        Test successful Delete on forum resource
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)
        resource1 = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                resource_number=1
            )
        )

        self.assertEqual(
            response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertFalse(ForumResource.objects.filter(
            forum=forum['obj'],
            resource_number=1
        ).exists())

    def test_delete_forum_resources_nonowner_fail(self):
        """
        Test deleting forum resource by nonowner fails
        """
        new_user, creds = self.new_user()
        forum, participant = self.forum_data.create_single_forum__public(
            new_user)
        resource1 = self.forum_data.create_forum_resource(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'],
                resource_number=1
            )
        )

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_resources_unauthed_fail(self):
        """
        Test delete request by unauth fails
        """
        client = self.de_auth_client(self.client)

        response = client.delete(
            self.resource_endpoint(
                forum_id='12903-123lk0lk', resource_number='123'))

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_delete_forum_resource_does_not_exist_404(self):
        """
        Test deleting a forum resource that does not exist fails
        """
        forum, participant = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.delete(
            self.resource_endpoint(
                forum_id=forum['uid'], resource_number='123'))

        self.assertEqual(
            response.status_code, status.HTTP_404_NOT_FOUND, response.data)
        self.assertEqual(response.data, {
            'error': 'Resource with ID `123` does not exist'
        })
