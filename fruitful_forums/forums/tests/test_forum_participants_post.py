from random import choice

from django.test import tag
from django.contrib.auth.hashers import check_password
from rest_framework import status

from fruitful_forums.forums.models import ForumParticipant
from fruitful_forums.forums.urls import NAMESPACE as FORUM_NAMESPACE
from fruitful_forums.forums.views.forum_participants_view import (
    ForumParticipantsView
)
from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from fruitful_forums.utils.asserter import TestAssertionHelper
from utils.testing.base_test_case import BaseAPIAllViewTestCase


@tag('db')
class ForumParticipantsPostIntegTestCase(BaseAPIAllViewTestCase):
    """
    Test Case for the Forums participation Collection POST operations
    """
    parent_none = None

    POST_FORMAT = 'json'

    view_under_test = ForumParticipantsView

    app_namespace = FORUM_NAMESPACE

    forum_data = ForumsDataManager()

    assert_helper = TestAssertionHelper()

    @classmethod
    def tearDownClass(cls):
        """
        Cleanup data created for tests
        """
        cls.forum_data.cleanup()
        super(ForumParticipantsPostIntegTestCase, cls).tearDownClass()

    def test_create_forum_participant_unauthed__negative(self):
        """
        Test creating forum participant fails if user is unauthed
        """
        client = self.de_auth_client(self.client)

        response = client.post(
            self.collection_endpoint(forum_id='123-123-123'),
            data={}, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_create_anon_forum_participant_user_as_owner__positive(self):
        """
        Test creating a anonymous forum participant by forum owner, nonadmin
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_participant_post_data()
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        participant_number = response.data['id']

        new_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        self.assertEqual(new_participant.name, post_data['name'])
        self.assertEqual(new_participant.email, post_data['email'])
        self.assertTrue(check_password(
            post_data['password'],
            new_participant.password
        ))

    def test_create_anon_forum_participant_user_as_admin__positive(self):
        """
        Test creating a anonymous forum participant by admin user
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)

        self.basic_auth_client(self.api_key)
        self.as_staff(self.user)

        post_data = self.forum_data.rand_participant_post_data()
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertIn('id', response.data)
        participant_number = response.data['id']

        new_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        self.assertEqual(new_participant.name, post_data['name'])
        self.assertEqual(new_participant.email, post_data['email'])
        self.assertTrue(check_password(
            post_data['password'],
            new_participant.password
        ))

    def test_create_anon_forum_participant_nonowner__negative(self):
        """
        Test creating a anonymous forum participant by nonowner fails
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = self.forum_data.rand_participant_post_data()
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': ['Only forum owners may create anonymous users.']
        })

    def test_create_forum_participant_self__positive(self):
        """
        Test creating a public forum participant for authenticated user
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data={}, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        participant_number = response.data['id']

        new_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        self.assertEqual(new_participant.get_name(), self.user.get_full_name())
        self.assertEqual(new_participant.get_email(), self.user.email)

    def test_create_forum_participant_max_reached__negative(self):
        """
        Test creating a forum participant fails if max participants reached
        """
        max_participants = 2
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user, overrides={'max_participants': max_participants})
        participant2 = self.forum_data.create_forum_participant(forum['obj'])

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data={}, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': 'Max participants reached for forum.'
        })

    def test_create_private_forum_participant_password__positive(self):
        """
        Test creating a private forum participant succeeds with password
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__private(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = {
            'forum_password': forum['password']
        }
        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', response.data)
        participant_number = response.data['id']

        new_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        self.assertEqual(new_participant.get_name(), self.user.get_full_name())
        self.assertEqual(new_participant.get_email(), self.user.email)

    def test_create_private_forum_participant__negative(self):
        """
        Test creating a private forum participant without password fails
        """
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__private(
            new_user)

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        response = self.client.post(
            self.collection_endpoint(forum_id=forum['uid']),
            data={}, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_forum_participant_same_email__negative(self):
        """
        Test creating participant with same name fails
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)
        user_email = self.creds['email']

        post_data = self.forum_data.rand_participant_post_data()
        post_data['email'] = user_email

        self.bearer_auth_client(self.token)
        self.as_staff(self.user)

        response = self.client.post(
            self.collection_endpoint(
                forum_id=forum['uid']
            ),
            data=post_data, format=self.POST_FORMAT
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'email': ['A participant with that email already exists.']
        })

    def test_update_forum_anon_participant_as_owner__positive(self):
        """
        Test forum owner can update anon forum participant data
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])
        participant_number = participant2['participant_number']

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        random_update_field = choice(['email', 'name', 'password'])
        data = self.forum_data.rand_participant_post_data()
        post_data = {
            random_update_field: data.pop(random_update_field)
        }
        response = self.client.post(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant_number
            ),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        updated_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        expected_data = {
            'name': participant2['name'],
            'email': participant2['email'],
            'password': participant2['password'],
        }
        expected_data.update(post_data)

        self.assertEqual(updated_participant.name, expected_data['name'])
        self.assertEqual(updated_participant.email, expected_data['email'])
        self.assertTrue(check_password(
            expected_data['password'],
            updated_participant.password
        ))

    def test_update_forum_anon_participant_as_admin__positive(self):
        """
        Test forum owner can update anon forum participant data
        """
        # Create forum for different user
        new_user, creds = self.new_user()
        forum, participant1 = self.forum_data.create_single_forum__public(
            new_user)
        # create another participant to update and retrieve number (2)
        participant2 = self.forum_data.create_forum_participant(forum['obj'])
        participant_number = participant2['participant_number']

        self.basic_auth_client(self.api_key)
        self.as_staff(self.user)  # as admin

        # randomly generate a anonymous field to update
        random_update_field = choice(['email', 'name', 'password'])
        data = self.forum_data.rand_participant_post_data()
        post_data = {
            random_update_field: data.pop(random_update_field)
        }
        response = self.client.post(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant_number
            ),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        updated_participant = ForumParticipant.objects.get(
            forum__uid=forum['uid'], participant_number=participant_number)

        expected_data = {
            'name': participant2['name'],
            'email': participant2['email'],
            'password': participant2['password'],
        }
        expected_data.update(post_data)

        # Validate fields are the same or changed accordingly
        self.assertEqual(updated_participant.name, expected_data['name'])
        self.assertEqual(updated_participant.email, expected_data['email'])
        self.assertTrue(check_password(
            expected_data['password'],
            updated_participant.password
        ))

    def test_update_forum_participant_as_owner__negative(self):
        """
        Test forum owner cant update non-anon forum participant
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)

        # create another participant to try update associate with APIuser
        new_user, creds = self.new_user()
        participant2 = self.forum_data.create_forum_participant(
            forum['obj'], new_user)
        participant_number = participant2['participant_number']

        self.bearer_auth_client(self.token)
        self.as_non_staff(self.user)

        post_data = {
            'name': 'should fail'
        }
        response = self.client.post(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant_number
            ),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'error': ['Unable to update forum participant associated with '
                      'user `%s`. User must update information via '
                      '`/users/` endpoint.' % new_user.uid]
        })

    def test_update_public_forum_participant_data__negative(self):
        """
        Test forum participant cant update their data (nothing to update)
        """
        forum, participant1 = self.forum_data.create_single_forum__public(
            self.user)

        # create another participant to try update associate with APIuser
        new_user, creds = self.new_user()
        new_token = self.new_token(new_user)
        participant2 = self.forum_data.create_forum_participant(
            forum['uid'], new_user)
        participant_number = participant2['participant_number']

        self.bearer_auth_client(new_token)
        self.as_non_staff(new_user)

        post_data = {
            'name': 'should fail'
        }
        response = self.client.post(
            self.resource_endpoint(
                forum_id=forum['uid'],
                participant_number=participant_number
            ),
            data=post_data, format=self.POST_FORMAT)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(response.data, {
            'error': ['Unable to update forum participant '
                      'associated with user `%s`. User must '
                      'update information via '
                      '`/users/` endpoint.' % new_user.uid]
        })
