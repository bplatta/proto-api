from rest_framework.permissions import BasePermission

from fruitful_forums.forums.permissions.facts import ForumFacts


def participant_is_self(user, resource):
    """
    Simple check if user is participant
    :param user: APIUser object
    :param resource: ForumParticipant
    :return:
    """
    return user == resource.user


class ForumParticipantPermissions(BasePermission):
    """
    Permissions for the Forum participants sub-collection endpoint

    Assumes:
        - authentication happens first
        - parent_instance (Forum) set on view by SubCollectionMixin

    (admin can do all, unauthenticated can do nothing)

        GET:
            list: auth'ed can list PUBLIC Forums participants
            read:
                public forum - auth'ed can view participants
                private forum - only creator can view participants
        POST:
            create: only forum creator can create new participants
            update: only creator can update forum participants
        DELETE:
            delete: only creator can delete a participant
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        Facts = ForumFacts(request, view)

        if Facts.is_admin():
            return True

        method = request.method

        if method == 'GET':
            return (
                Facts.is_public_forum() or
                Facts.is_forum_participant() or
                Facts.has_forum_password())

        # Public forums can receive requests to join as participants
        # Private forums can also receive requests provided they contain
        #   the forum password
        elif method == 'POST':
            return True

        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on forum resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ForumFacts(
            request, view, obj,
            is_resource_owner=participant_is_self)

        if Facts.is_admin():
            return True

        method = request.method

        # Only forum owner can update anon users
        if method == 'POST' or method == 'DELETE':
            return Facts.is_forum_owner() or Facts.is_resource_owner()
        elif method == 'GET':
            return (
                Facts.is_public_forum() or
                Facts.is_forum_participant() or
                Facts.has_forum_password())
        return True
