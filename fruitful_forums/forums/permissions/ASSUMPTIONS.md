# Some notes about the permissions
1. Assumption is authentication has already occurred (by middleware). 
That is, non anonymous users make it this far. This will change if the 
API is exposed to the general public and data is made available 
without any authentication.

2. Method not supported errors handled by the view

3. Determines permissions based on 3 things:
 - Type of forum (public vs private)
 - Relationship between forum and authenticated user
  - Is the user the creator of the forum? -> all permissions
  - Is the user a participant of the forum? -> read permissions
 - If forum is private, is the forum password provided? -> read permissions