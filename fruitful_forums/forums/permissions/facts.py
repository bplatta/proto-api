from django.contrib.auth.hashers import check_password
from rest_framework import exceptions, status

from fruitful_forums.forums.models import Forum, ForumParticipant
from utils.api.base_permissions import BaseRequestFacts
from utils.exceptions import BaseAPIException


class ForumFacts(BaseRequestFacts):
    """
    Facts about Forum data given the request
    """

    @property
    def forum(self):
        return self.parent if self.is_sub_collection else self.resource

    def is_admin(self):
        return self.request.user.is_staff

    def is_forum_owner(self):
        return self.request.user == self.forum.creator_participant.user

    def is_public_forum(self):
        return self.forum.type == Forum.PUBLIC_FORUM

    def is_forum_participant(self):
        return ForumParticipant.objects.filter(
            user=self.request.user, forum=self.forum).exists()

    def is_resource_owner(self):
        return self._is_resource_owner(self.request.user, self.resource)

    def has_forum_password(self):

        if not self.request.query_params.get('password'):
            raise BaseAPIException(
                status_code=status.HTTP_404_NOT_FOUND,
                message='Forum with ID `%s` does not exist' % self.forum.uid
            )

        if not check_password(
                self.request.query_params['password'],
                self.forum.password):
            raise exceptions.PermissionDenied('Invalid password provided.')

        return True
