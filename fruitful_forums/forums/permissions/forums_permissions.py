from rest_framework.permissions import BasePermission

from fruitful_forums.forums.permissions.facts import ForumFacts


class ForumPermissions(BasePermission):
    """
    Permissions for the Forum collection endpoint
    (admin can do all, unauthenticated can do nothing)

    Assumes:
        - authentication happens first

        GET:
            list: auth'ed can list PUBLIC Forums
            read:
                public forum - auth'ed can view forum
                private forum - only creator can view forum
        POST:
            create: auth'ed can create forum
            update: only creator can update forum
        DELETE:
            delete: only owner can delete a forum

    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        # Any auth'ed user can List or Create forums
        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on forum resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ForumFacts(request, view, obj)

        method = request.method
        if method == 'GET':
            return (
                Facts.is_public_forum() or
                Facts.is_forum_participant() or
                Facts.has_forum_password())
        elif method == 'POST' or method == 'DELETE':
            return Facts.is_admin() or Facts.is_forum_owner()
        return True
