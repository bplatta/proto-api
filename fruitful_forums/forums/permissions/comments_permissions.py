from rest_framework.permissions import BasePermission

from fruitful_forums.forums.permissions.facts import ForumFacts


class ForumCommentsPermissions(BasePermission):
    """
    Permissions for comments of a forum

    Assumes:
        - authentication happens first
        - parent_instance (Forum) set on view by SubCollectionMixin


    permissions: (admin can do all, unauthenticated can do nothing)
        GET:
            list: auth'ed can list only PUBLIC Forums comments
            read: no supported
        POST:
            create: only forum participant can create comments
            update: only comment creator can update comment
        DELETE:
            delete: only comment creator can 'delete' a comment
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        Facts = ForumFacts(request, view)

        if Facts.is_admin():
            return True

        # Can list comments only if public forum or participant
        if request.method == 'GET':
            return (
                Facts.is_public_forum() or
                Facts.is_forum_participant() or
                Facts.has_forum_password())
        # Can create new comments only if forum participant
        elif request.method == 'POST':
            return Facts.is_forum_participant()

        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on forum resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ForumFacts(
            request, view, obj,
            is_resource_owner=(
                lambda user, res: user == res.creator_participant.user)
        )

        if Facts.is_admin():
            return True

        if request.method == 'POST' or request.method == 'DELETE':
            return Facts.is_resource_owner()
        elif request.method == 'GET':
            return Facts.is_public_forum() or Facts.is_forum_participant()
        return True
