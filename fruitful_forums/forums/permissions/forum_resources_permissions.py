from rest_framework.permissions import BasePermission

from fruitful_forums.forums.permissions.facts import ForumFacts


class ForumResourcePermissions(BasePermission):
    """
    Permissions for the Forum resources sub-collection endpoint

    Assumes:
        - authentication happens first
        - parent_instance (Forum) set on view by SubCollectionMixin

    (admin can do all, unauthenticated can do nothing)

        GET:
            list: auth'ed can list PUBLIC Forums resources
            read:
                unsupported, handled downstream
        POST:
            create: only forum creator can create new resources
            update: only creator can update forum resources
        DELETE:
            delete: only creator can delete a resource
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        Facts = ForumFacts(request, view)

        if Facts.is_admin():
            return True

        method = request.method

        # Can list resources only if forum participant
        if method == 'GET':
            return (
                Facts.is_public_forum() or
                Facts.is_forum_participant() or
                Facts.has_forum_password())
        # Can create new resources only if forum owner
        elif method == 'POST' or method == 'DELETE':
            return Facts.is_forum_owner()

        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on forum resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ForumFacts(request, view, obj)

        if Facts.is_admin():
            return True

        if request.method == 'DELETE':
            return Facts.is_forum_owner()
        # Other methods are not supported on objects.
        # ie. POST (update resource) and GET (read resource)
        # handled in view
        return True
