from django.apps import AppConfig


class ForumsConfig(AppConfig):
    name = 'fruitful_forums.forums'
    label = 'forums'
