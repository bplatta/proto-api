from rest_framework.permissions import IsAuthenticated

from fruitful_forums.forums.models import Forum, ForumResource
from fruitful_forums.forums.permissions.forum_resources_permissions import (
    ForumResourcePermissions)
from fruitful_forums.forums.serializers.forum_resource_serializer import (
    ForumResourcePOSTSerializer, ForumResourceSerializer
)
from utils.api.base_view import APIAllView
from utils.api.base_paginator import pagination_for_collection
from utils.mixins import SubcollectionMixin
from utils.exceptions import OperationNotSupported


class ForumResourcesView(APIAllView, SubcollectionMixin):
    """
    Main view for handling interactions with the Forums resources collection
    """
    main_model = ForumResource
    lookup_field = 'resource_number'
    lookup_url_kwarg = 'resource_number'
    parent_resource__model = Forum
    parent_lookup_field = 'uid'
    parent_lookup_url_kwarg = 'forum_id'

    http_method_names = ['get', 'post', 'delete']
    pagination_class = pagination_for_collection('resources')
    supported_operations = ['list', 'create', 'delete']

    permission_classes = (IsAuthenticated,)
    parent_permission_classes = (ForumResourcePermissions,)

    namespace = {
        'collection': 'forum-resources',
        'resource': 'forum-resource'
    }
    serializer_classes = {
        'post': {
            'default': ForumResourcePOSTSerializer
        },
        'get': {
            'default': ForumResourceSerializer
        }
    }

    def get_queryset(self):
        """
        Filter resources by parent forum
        :return: querset
        """
        return self.filter_queryset_by_parent(self._get_all_queryset())

    def get(self, request, forum_id, resource_number=None):

        if resource_number is not None:

            raise OperationNotSupported(
                operation='read',
                opt_message='`GET` request only allowed '
                            'on forum-resource collection.'
            )

        return self._get(request)

    def post(self, request, forum_id, resource_number=None):

        if resource_number is not None:

            raise OperationNotSupported(
                operation='update',
                opt_message='Please delete resource and create a new one.'
            )

        return self._post(request)

    def delete(self, request, forum_id, resource_number=None):
        return self._delete(request, resource_number=resource_number)
