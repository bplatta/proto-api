from django.db.models import Q
from rest_framework.permissions import IsAuthenticated

from fruitful_forums.forums.models import Forum, ForumParticipant
from fruitful_forums.forums.permissions.forums_permissions import (
    ForumPermissions)
from fruitful_forums.forums.serializers.forum_serializer import (
    ForumPOSTSerializer,
    ForumLISTSerializer,
    ForumREADSerializer,
)
from utils.api.base_view import APIAllView
from utils.api.base_paginator import pagination_for_collection


class ForumsView(APIAllView):
    """
    Main view for handling interactions with the Forums
    """
    main_model = Forum
    lookup_url_kwarg = 'forum_id'
    lookup_field = 'uid'
    permission_classes = (IsAuthenticated, ForumPermissions)
    pagination_class = pagination_for_collection('forums')
    http_method_names = ['get', 'post', 'delete']
    supported_operations = ['read', 'list', 'create', 'delete', 'update']

    namespace = {
        'collection': 'forums',
        'resource': 'forum'
    }

    serializer_classes = {
        'post': {
            'default': ForumPOSTSerializer
        },
        'get': {
            'default': ForumLISTSerializer,
            'collection': ForumLISTSerializer,
            'resource': ForumREADSerializer
        }
    }

    # this query is not quite the same as the one below but is the more
    # 'obvious' version of it
    list_q = """
        SELECT f.* FROM forums_forum f
            INNER JOIN forums_forumparticipant fp
                ON f.creator_participant_id=fp.id
        WHERE (
            f.type = 'public' OR
            fp.user_id = %s OR
            %s IN (
                SELECT user_id FROM forums_forumparticipant fp2
                WHERE fp2.forum_id = f.id
            )
        ) ORDER BY f.created_at"""

    def get_queryset(self):
        """
        Filter forums for public forums only or private if owned by user
        :return: QuerySet
        """
        query_OR_clause = Q(type=Forum.PUBLIC_FORUM)
        if not self.admin_from_webserver():
            user_id = self.request.user.id
            query_OR_clause |= Q(creator_participant__user=user_id)
            query_OR_clause |= Q(participants__user=user_id)

        query = Q(deleted=False) & query_OR_clause

        if self.lookup_url_kwarg in self.kwargs:
            query |= Q(uid=self.kwargs[self.lookup_url_kwarg])

        return Forum.objects.select_related('creator_participant__user')\
            .filter(query)\
            .order_by('uid', '-created_at')\
            .distinct('uid')

    def post(self, request, forum_id=None):
        return self._post(request, forum_id=forum_id)

    def get(self, request, forum_id=None):
        return self._get(request, forum_id=forum_id)

    def delete(self, request, forum_id=None):
        return self._delete(request, forum_id=forum_id)
