from fruitful_forums.forums.models import Comment, Forum
from fruitful_forums.forums.serializers.comment_serializer import (
    CommentLISTSerializer, CommentPOSTSerializer
)
from fruitful_forums.forums.permissions.comments_permissions import (
    ForumCommentsPermissions)
from utils.api.base_view import APIAllView
from utils.mixins import SubcollectionMixin


class CommentsView(APIAllView, SubcollectionMixin):
    """
    View for Forum Comments sub collection
    """
    main_model = Comment
    lookup_field = 'uid'
    lookup_url_kwarg = 'comment_id'
    supported_operations = ['list', 'create', 'update', 'delete']

    parent_resource__model = Forum
    parent_lookup_field = 'uid'
    parent_lookup_url_kwarg = 'forum_id'
    parent_permission_classes = (ForumCommentsPermissions,)

    namespace = {
        'collection': 'forum-comments',
        'resource': 'forum-comment'
    }

    serializer_classes = {
        'post': {
            'default': CommentPOSTSerializer
        },
        'get': {
            'default': CommentLISTSerializer
        }
    }

    def get_queryset(self):
        """
        Filter comments by parent forum
        :return: querset
        """
        return self.filter_queryset_by_parent(self._get_all_queryset())

    def post(self, request, forum_id, comment_id=None):
        return self._post(request, comment_id=comment_id)

    def get(self, request, forum_id, comment_id=None):
        return self._get(request, comment_id=comment_id)

    def delete(self, request, forum_id, comment_id=None):
        return self._delete(request, comment_id=comment_id)
