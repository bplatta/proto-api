from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from fruitful_forums.forums.models import ForumParticipant, Forum
from fruitful_forums.forums.permissions.forum_participants_permissions import (
    ForumParticipantPermissions)
from fruitful_forums.forums.serializers.forum_participant_serializer import (
    ForumParticipantPOSTSerializer,
    ForumParticipantGETSerializer
)
from utils.api.base_view import APIAllView
from utils.api.base_paginator import pagination_for_collection
from utils.mixins import SubcollectionMixin
from utils.exceptions import OperationNotSupported, BaseAPIException


CREATOR_PARTICIPANT_NUMBER = str(1)


class ForumParticipantsView(APIAllView, SubcollectionMixin):
    """
    View for Forum Participants sub collection
    """
    main_model = ForumParticipant
    lookup_field = 'participant_number'
    lookup_url_kwarg = 'participant_number'
    supported_operations = ['list', 'create', 'update', 'delete']
    parent_resource__model = Forum
    parent_lookup_field = 'uid'
    parent_lookup_url_kwarg = 'forum_id'
    pagination_class = pagination_for_collection('participants')
    permission_classes = (IsAuthenticated,)
    parent_permission_classes = (ForumParticipantPermissions,)

    namespace = {
        'collection': 'forum-participants',
        'resource': 'forum-participant'
    }

    serializer_classes = {
        'post': {
            'default': ForumParticipantPOSTSerializer
        },
        'get': {
            'default': ForumParticipantGETSerializer
        }
    }

    def get_queryset(self):
        """
        Filter participants by parent forum
        :return: querset
        """
        return self.filter_queryset_by_parent(self._get_all_queryset())

    def post(self, request, forum_id, participant_number=None):
        return self._post(request, participant_number=participant_number)

    def get(self, request, forum_id, participant_number=None):
        if participant_number is not None:
            raise OperationNotSupported(
                operation='read',
                opt_message='`GET` request only allowed '
                            'on forum-participant collection.')

        return self._get(request)

    def delete(self, request, forum_id, participant_number=None):
        if participant_number == CREATOR_PARTICIPANT_NUMBER:
            raise BaseAPIException(
                status_code=status.HTTP_400_BAD_REQUEST,
                message='Forum participant `%s` is the forum owner '
                        'and may not be deleted' % CREATOR_PARTICIPANT_NUMBER
            )

        return self._delete(
            request, participant_number=participant_number)
