from copy import deepcopy
from functools import partial
from django.contrib.auth.hashers import make_password
from django.db import transaction
from rest_framework import serializers

from fruitful_forums.forums.data_validators.forum_validators import (
    ForumTypeValidator)
from fruitful_forums.forums.models import (
    Forum, ForumParticipant, ForumData, ForumResource)
from fruitful_forums.forums.serializers.comment_serializer import (
    CommentLISTSerializer)
from fruitful_forums.forums.serializers.forum_participant_serializer import (
    ForumParticipantGETSerializer)
from fruitful_forums.forums.serializers.forum_resource_serializer import (
    ForumResourceSerializer, ForumResourcePOSTSerializer)

from utils.api.base_serializers import BasePostSerializer


def update_with_forum_id(forum_id, d):
    """
    Func for mapping resources with forum_id
    :param forum_id: pk int
    :param d: {}
    :return: {}
    """
    d.update(forum_id=forum_id)
    return d


class ForumPOSTSerializer(BasePostSerializer):
    """
    Serialize data for FORUM creation
    """
    topic = serializers.CharField(max_length=255)
    goal = serializers.CharField(max_length=510)
    max_participants = serializers.IntegerField(default=0, min_value=0)
    type = serializers.ChoiceField(choices=Forum.FORUM_TYPE_CHOICES)
    description = serializers.CharField(max_length=1000, required=False)
    url = serializers.URLField(required=False)
    is_expert_forum = serializers.BooleanField(default=False)
    forum_password = serializers.CharField(
        max_length=255, required=False, write_only=True)
    tags = serializers.ListField(
        child=serializers.CharField(max_length=30), required=False)

    # User information
    email = serializers.CharField(write_only=True, required=False)
    name = serializers.CharField(
        max_length=255, write_only=True, required=False)
    user_password = serializers.CharField(
        max_length=30, write_only=True, required=False)
    phone_number = serializers.CharField(
        max_length=255, required=False, write_only=True)

    # Optional forum data
    resources = ForumResourcePOSTSerializer(many=True, required=False)

    custom_validators = [ForumTypeValidator()]
    required_user_fields = ['email', 'name', 'user_password']
    invalid_update_fields = ['name', 'phone_number', 'user_password',
                             'email', 'resources']
    invalid_update_message = 'Please update (POST) the ' \
                             'relevant related forum resource.'

    @staticmethod
    def hash_password(password):
        """
        Hashes the forums password
        :param password: plaintext password str
        :return: hashed str
        """
        return make_password(password)

    def validate(self, data):
        """
        Additional data validation and creation
        :param data: {} from request
        :return: validated data {}
        """
        init_data = deepcopy(data)

        if 'email' in init_data:
            if not self.context['request'].user.is_staff:
                raise serializers.ValidationError({
                    'error': 'Only admin can create Forums on '
                             'behalf of anonymous users.'
                })
            # Validate anon-user fields provided for new forum
            missing_fields = [
                f for f in self.required_user_fields if f not in init_data]

            if len(missing_fields) > 0:
                raise serializers.ValidationError(
                    {f: 'This field is required if creating forum on '
                        'behalf of unauthenticated user.'
                     for f in missing_fields})
        else:
            init_data['api_user'] = self.context['request'].user

        return init_data

    @transaction.atomic
    def create(self, validated_data):
        """
        Create method for Forum data
        :param validated_data: {}
        :return: Forum instance
        """
        if 'forum_password' in validated_data:
            validated_data['password'] = self.hash_password(
                validated_data.pop('forum_password'))

        # Remove participant data
        participant_name = validated_data.pop('name', None)
        participant_email = validated_data.pop('email', None)
        participant_user = validated_data.pop('api_user', None)
        participant_password = validated_data.pop('user_password', None)
        if participant_password:
            participant_password = self.hash_password(participant_password)

        # Create first Participant
        creator = ForumParticipant.objects.create(
            forum=None,  # Associate with Forum after its creation
            password=participant_password,
            name=participant_name,
            user=participant_user,
            email=participant_email,
            participant_number=1)

        validated_data['creator_participant'] = creator

        # Remove resources data
        resources = validated_data.pop('resources', None)

        # Create new Forum
        new_forum = Forum.objects.create(**validated_data)

        # associate forum participant with the forum
        creator.forum = new_forum
        creator.save()

        if resources is not None:
            ForumResource.objects.bulk_create([
                ForumResource(**r) for r in [
                    update_with_forum_id(new_forum.id, d) for d in resources
                ]
            ])

        return new_forum

    def update(self, instance, validated_data):
        """
        Update method
        :param instance: Forum instance
        :param validated_data: request {}
        :return: Forum instance
        """
        instance.topic = validated_data.get('topic', instance.topic)
        instance.goal = validated_data.get('goal', instance.goal)
        instance.type = validated_data.get('type', instance.type)
        instance.tags = validated_data.get('tags', instance.tags)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.max_participants = validated_data.get(
            'max_participants', instance.max_participants)

        if 'forum_password' in validated_data:
            instance.password = self.hash_password(
                validated_data['forum_password'])
        # if the forum has been made public, remove the password
        elif validated_data.get('type', Forum.PUBLIC_FORUM):
            instance.password = None

        instance.save()
        return instance


class ForumDataPOSTSerializer(serializers.Serializer):
    """
    Manages creation of new Forum Data objects
    """
    forum_participant = serializers.PrimaryKeyRelatedField(
        queryset=ForumParticipant.objects.all(),
        required=False,
        allow_null=True
    )
    key = serializers.CharField(max_length=255)
    value = serializers.CharField(max_length=255)

    def get_parent_instance_from_view(self):
        return self.context['view'].parent_instance

    def validate(self, data):
        """
        Validate forum k-v data
        :param data: {}
        :return: {}
        """
        data['forum'] = self.get_parent_instance_from_view()
        return data

    def create(self, validated_data):
        """
        Create Forum Data object
        :param validated_data: {}
        :return: ForumData object
        """
        return ForumData.objects.create(**validated_data)

    def update(self, **kwargs):
        raise serializers.ValidationError(
            'Forum Data objects do not support update operations'
        )


class ForumDataSerializer(serializers.ModelSerializer):
    """
    Serializer for forum data
    """

    class Meta:
        model = ForumData
        fields = '__all__'


class ForumLISTSerializer(serializers.ModelSerializer):
    """
    Forum LIST call
    """
    id = serializers.CharField(source='uid')
    link = serializers.CharField(source='get_uid_url')
    created_by_name = serializers.CharField(source='get_creator_name')
    created_by_user_id = serializers.CharField(
        source='creator_participant.user.id')

    class Meta:
        model = Forum
        fields = (
            'created_at', 'topic', 'goal', 'description', 'created_by_name',
            'max_participants', 'type', 'url', 'tags',
            'id', 'link', 'created_by_user_id'
        )


class ForumREADSerializer(serializers.ModelSerializer):
    """
    Forum READ data
    """
    id = serializers.CharField(source='uid')
    link = serializers.CharField(source='get_uid_url')
    forum_resources = ForumResourceSerializer(many=True)
    comments = CommentLISTSerializer(many=True)
    participants = ForumParticipantGETSerializer(many=True)
    data = ForumDataSerializer(many=True)

    class Meta:
        model = Forum
        fields = (
            'created_at', 'topic', 'goal', 'description',
            'max_participants', 'type', 'url', 'comments', 'tags',
            'forum_resources', 'participants', 'data', 'link', 'id'
        )
