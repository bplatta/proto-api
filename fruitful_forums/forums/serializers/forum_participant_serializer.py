from django.db import transaction
from django.db.models import Max, Q
from django.db.utils import IntegrityError as DatabaseIntegrityError
from django.contrib.auth.hashers import make_password, check_password
from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueTogetherValidator

from fruitful_forums.forums.models import ForumParticipant, Forum
from utils.api.base_serializers import BasePostSerializer


class ForumParticipantPOSTSerializer(BasePostSerializer):
    """
    Manages creation of new Forum Participants
    """
    password = serializers.CharField(max_length=30, required=False)
    name = serializers.CharField(max_length=255, required=False)
    email = serializers.EmailField(required=False)
    forum_password = serializers.CharField(max_length=30, required=False)

    # fields required if creating an anonymous user
    required_anon_fields = ['email', 'name', 'password']
    num_req_anon_fields = len(required_anon_fields)

    @staticmethod
    def hash_password(password):
        """
        Hashes participant forum password
        """
        return make_password(password)

    @staticmethod
    def _validate_user_unique(forum_id, user_id):
        """
        Validate for user is unique among participants
        :param forum_id: pk of forum
        :param user_id: pk of user
        :return: None or raises
        """
        if ForumParticipant.objects.filter(
            forum_id=forum_id, user_id=user_id
        ).exists():
            raise serializers.ValidationError({
                'error': 'A participant already exists for user.'
            })

    @staticmethod
    def _validate_email_unique(forum_id, email):
        """
        Validate the email for user is unique among participants
        :param forum_id: pk of forum
        :param email: str - email address
        :return: None or raises
        """
        if ForumParticipant.objects.select_related('user').filter(
            Q(forum_id=forum_id),
            Q(email=email) | Q(user__email=email)
        ).exists():
            raise serializers.ValidationError({
                'email': 'A participant with that email already exists.'
            })

    @staticmethod
    def check_if_private(user, forum, data):
        """
        Validate forum password in post data. Typically this is handled
        by the permissions classes but here the forum password is passed
        in the request body and so we validate it here.
        :param user: APIUser object
        :param forum: Forum object
        :param data: {}
        :return: None or raises
        """
        if forum.type == Forum.PRIVATE_FORUM:
            # owner doesnt require password
            if user == forum.creator_participant.user:
                return None
            if 'forum_password' not in data:
                raise exceptions.PermissionDenied()
            if not check_password(data['forum_password'], forum.password):
                raise exceptions.PermissionDenied(
                    'Invalid password provided.')

    @staticmethod
    def next_participant_number(forum_id):
        """
        Gets participant count for this forum
        :return: int
        """
        data = ForumParticipant.objects.filter(
            forum_id=forum_id).aggregate(Max('participant_number'))
        max_num = data['participant_number__max']
        if max_num is None:
            return 1
        return int(max_num) + 1

    @staticmethod
    def check_allow_anon_participant_editing(user, forum):
        """
        Only allow admin or forum owner to create or update anonymous users
        :param user: APIUser object
        :param forum: Forum object
        :return: None or raises
        """
        if not (user.is_staff or user == forum.creator_participant.user):
            raise serializers.ValidationError({
                'error': 'Only forum owners may create anonymous users.'
            })

    def check_if_update_on_user(self):
        """
        Checks if you can update user attributes depending on if participant
        is associated with api user or not
        :return: None or raises
        """
        if self.instance and self.instance.user is not None:
            raise serializers.ValidationError({
                'error': 'Unable to update forum participant '
                         'associated with user `%s`. User must '
                         'update information via `/users/` endpoint.'
                         % self.instance.user.uid
            })

    def validate(self, data):
        """
        Validate the new participant
        :param data: {}
        :return: validated data {}
        """
        forum = self.get_parent_instance_from_view()
        forum_id = forum.id
        user = self.context['request'].user
        is_update = self.instance is not None

        # Validators that will raise
        self.check_if_private(user, forum, data)
        self.check_if_update_on_user()

        participant_data = {
            'forum_id': forum_id
        }

        # Fields required to create an anonymous forum participant
        anon_user_fields = [
            (f, data.get(f))
            for f in self.required_anon_fields
            if f in data]

        # Validate anonymous user field permissions and combinations
        if len(anon_user_fields) > 0:

            # Only forum owner and admin can create
            # anonymous users - will raise
            self.check_allow_anon_participant_editing(user, forum)

            if (not is_update and
                    len(anon_user_fields) != self.num_req_anon_fields):
                raise serializers.ValidationError({
                    'error': '`email`, `name` and `password` '
                             'are required for anonymous users.'
                })

            for field, val in anon_user_fields:
                participant_data[field] = val

            # gaurantee email uniqueness across
            # anonymous and non-anonymous users
            if 'email' in participant_data:
                self._validate_email_unique(
                    forum_id, participant_data['email'])
        else:
            if not is_update:
                participant_data['user_id'] = user.id

        return participant_data

    @transaction.atomic
    def create(self, validated_data):
        """
        Creates a Forum Participant
        :param validated_data: {}
        :return: ForumParticipant
        """
        validated_data['participant_number'] = self.next_participant_number(
            validated_data['forum_id'])

        forum = self.get_parent_instance_from_view()

        # check if there is a limit on forum participants
        if forum.max_participants != 0:
            if validated_data['participant_number'] > forum.max_participants:
                raise serializers.ValidationError({
                    'error': 'Max participants reached for forum.'
                })

        if 'password' in validated_data:
            validated_data['password'] = self.hash_password(
                validated_data['password']
            )

        try:
            return ForumParticipant.objects.create(**validated_data)
        except DatabaseIntegrityError as e:
            raise serializers.ValidationError({
                'error': str(e)
            })

    def update(self, instance, validated_data):
        """
        Update registered participant for the
        :param instance: ForumParticipant object
        :param validated_data: {}
        :return: ForumParticipant
        """
        if 'password' in validated_data:
            instance.password = self.hash_password(validated_data['password'])

        # _BUG_ - there is a race condition here as validation is
        # done in is_validate()
        instance.email = validated_data.get('email', instance.email)
        instance.name = validated_data.get('name', instance.name)

        instance.save()
        return instance


class ForumParticipantGETSerializer(serializers.ModelSerializer):
    """
    Serializes a Forum's participant list
    """
    name = serializers.CharField(
        source='get_name', read_only=True)

    class Meta:
        model = ForumParticipant
        fields = ('participant_number', 'name')
