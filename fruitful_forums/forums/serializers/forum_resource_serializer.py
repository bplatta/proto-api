from django.db.models import Max
from django.db import transaction
from rest_framework import serializers

from fruitful_forums.forums.models import ForumResource

MAX_RESOURCES_PER_FORUM = 10


class ForumResourceSerializer(serializers.ModelSerializer):
    """
    Serializer for forum resources
    """
    class Meta:
        model = ForumResource
        fields = ('resource_number', 'url', 'name', 'summary')


class ForumResourcePOSTSerializer(serializers.Serializer):
    """
    Manages creation of new Forum Resources
    """
    url = serializers.URLField(max_length=500)
    name = serializers.CharField(max_length=350)
    summary = serializers.CharField(max_length=500)

    def get_parent_instance_from_view(self):
        return self.context['view'].parent_instance

    @staticmethod
    def next_resource_number(forum_id):
        """
        Gets participant count for this forum
        :return: int
        """
        data = ForumResource.objects.filter(
            forum_id=forum_id).aggregate(Max('resource_number'))
        max_num = data['resource_number__max']
        if max_num is None:
            return 1
        return int(max_num) + 1

    @transaction.atomic
    def create(self, validated_data):
        """
        Create Forum Resource
        :param validated_data: {}
        :return: ForumResource object
        """
        # Set parent instance on data
        validated_data['forum'] = self.get_parent_instance_from_view()
        validated_data['resource_number'] = self.next_resource_number(
            validated_data['forum'].id)
        if validated_data['resource_number'] > MAX_RESOURCES_PER_FORUM:
            raise serializers.ValidationError({
                'error': 'Max number of Forum resources have '
                         'been added to forum (%s).' % MAX_RESOURCES_PER_FORUM
            })
        return ForumResource.objects.create(**validated_data)

    def update(self, **kwargs):
        raise serializers.ValidationError(
            'Forum Resources do not support update operations. '
            'Please delete and recreate.')
