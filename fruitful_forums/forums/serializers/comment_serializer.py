import uuid
from copy import deepcopy
from rest_framework import serializers

from fruitful_forums.forums.models import (
    Comment, ForumParticipant, CommentResource)
from utils.api.base_serializers import BasePostSerializer
from utils.exceptions import OperationNotSupported


class CommentLISTSerializer(serializers.ModelSerializer):
    """
    Serialize Comment data
    """
    # resources = ForumResourceSerializer(many=True)

    class Meta:
        model = Comment
        fields = (
            'id', 'text', 'created_at', 'creator_participant',
            'type', 'votes', 'parent'
        )


class CommentResources(serializers.Serializer):
    """
    Separate serializer for creating Comment resources
    """
    url = serializers.URLField(max_length=500)
    name = serializers.CharField(max_length=350)


class CommentPOSTSerializer(BasePostSerializer):
    """
    Manges create of Comment Sub-Resources
    """
    text = serializers.CharField()
    creator = serializers.PrimaryKeyRelatedField(
        queryset=ForumParticipant.objects.all())
    parent_id = serializers.PrimaryKeyRelatedField(
        queryset=Comment.objects.all(), required=False)
    type = serializers.ChoiceField(choices=Comment.COMMENT_TYPE_CHOICES)

    @classmethod
    def generate_uid(cls):
        """
        Generate Unique ID of length self.HASH_LENGTH
        :return: str
        """
        return str(uuid.uuid4())[:cls.DEFAULT_UID_HASH_LENGTH]

    def validate(self, data):
        """
        Comment validation
        :param data: {}
        :return: {}
        """
        init_data = deepcopy(data)
        init_data['uid'] = self.generate_uid()
        init_data['forum'] = self.get_parent_instance_from_view()

        return init_data

    def create(self, validated_data):
        """
        Creates Forum Comment
        :param validated_data: {}
        :return: Comment Object
        """
        comment = Comment.objects.create(**validated_data)

        if validated_data.get('resources'):
            CommentResource.objects.bulk_create([
                CommentResource(
                    url=resource['url'],
                    name=resource['name'],
                    comment=comment
                ) for resource in validated_data['resources']
            ])

        return comment

    def update(self, instance, validated_data):
        raise OperationNotSupported(
            'update', 'Comments cant be updated right now!')
