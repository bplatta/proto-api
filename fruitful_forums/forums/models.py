import uuid

from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField

FORUM_UID_LENGTH = 18
COMMENT_UID_LENGTH = 18


###############
# Forums Data #
###############

def generate_uuid_func(name, length):
    """
    Creates a function for generating UUID of certain length, setting func name
    :param name: str
    :param length: int
    :return: func
    """
    def gen_uuid():
        return str(uuid.uuid4())[:length]
    gen_uuid.__name__ = name
    return gen_uuid


forum_uid = generate_uuid_func('forum_uid', FORUM_UID_LENGTH)
comment_uid = generate_uuid_func('comment_uid', COMMENT_UID_LENGTH)


class Forum(models.Model):
    """
    Main model for Forum objects
    """
    UID_LENGTH = 24

    PUBLIC_FORUM = 'public'
    PRIVATE_FORUM = 'private'
    ENCRYPTED_FORUM = 'encrypted'  # Not yet supported
    FORUM_TYPE_CHOICES = (
        (PUBLIC_FORUM, PUBLIC_FORUM),
        (PRIVATE_FORUM, PRIVATE_FORUM)
    )

    uid = models.CharField(
        unique=True, editable=False, default=forum_uid, max_length=50)
    created_at = models.DateTimeField(auto_now=True)
    creator_participant = models.ForeignKey(
        'ForumParticipant', related_name='creator')
    topic = models.CharField(max_length=255)
    url = models.URLField(null=True, blank=True)
    goal = models.CharField(max_length=510)
    description = models.CharField(max_length=1000)
    deleted = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    password = models.CharField(max_length=255, null=True, blank=True)
    type = models.CharField(
        max_length=255, choices=FORUM_TYPE_CHOICES, default=PUBLIC_FORUM)
    max_participants = models.IntegerField(default=0)
    is_expert_forum = models.BooleanField(default=False)
    tags = ArrayField(
        models.CharField(max_length=30),
        blank=True,
        default=list
    )

    def __str__(self):
        return 'Forum %s: %s' % (self.uid, self.topic)

    @property
    def is_private(self):
        return self.type == self.PRIVATE_FORUM

    def get_uid_url(self):
        return '/forums/%s' % self.uid

    def total_participants(self):
        return self.participants.count()

    def total_resources(self):
        return self.forum_resources.count()

    def get_creator_name(self):
        return self.creator_participant.get_name()


class ForumParticipant(models.Model):
    """
    Forum participant model
    Notes:
        user - participants need not have an associated profile, i.e.
               the user is one-off for that forum
        password - required if no profile generated
    """
    class Meta:
        unique_together = (
            ('forum', 'user'),
            ('forum', 'email'),
            ('forum', 'participant_number')
        )

    def __str__(self):
        return 'Forum UID `%s`. Participant: %s' % (
            self.forum.uid, self.get_name())

    # Allow FK to be blank to allow creation of first user (creator) before
    # the forum is created. ole chicken and egg scenario
    forum = models.ForeignKey(
        Forum, null=True, blank=True, related_name='participants')
    participant_number = models.IntegerField(default=1)
    password = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=255, null=True)
    is_banned = models.BooleanField(default=False)
    email = models.EmailField(null=True, blank=True)
    # Can optionally be associate with a created user
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)

    def get_name(self):
        if self.user:
            return self.user.get_full_name()
        return self.name

    def get_email(self):
        if self.user:
            return self.user.email
        return self.email


class ForumData(models.Model):
    """
    k-v store for Forum information. This will
    store things like upvotes or downvotes
    """
    forum = models.ForeignKey(Forum, related_name='data')
    forum_participant = models.ForeignKey(
        ForumParticipant, null=True, blank=True)
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255)


class ForumResource(models.Model):
    """
    Resources associated with a forum, site as a link to research
    paper or yelp search
    """
    forum = models.ForeignKey(Forum, related_name='forum_resources')
    resource_number = models.IntegerField(default=1)
    url = models.URLField(max_length=500)
    name = models.CharField(max_length=350)
    summary = models.CharField(max_length=500, default='')


#################
# Comments Data #
#################

class Comment(models.Model):
    """
    Main comment model
    Notes:
        type
    """
    FACT = 'fact'
    OPINION = 'opinion'
    THEORY = 'theory'
    SUGGESTION = 'suggestion'

    COMMENT_TYPE_CHOICES = (
        (FACT, FACT),
        (OPINION, OPINION),
        (THEORY, THEORY),
        (SUGGESTION, SUGGESTION)
    )

    def __str__(self):
        return 'Comment %s' % self.id

    uid = models.CharField(
        unique=True, editable=False, default=comment_uid, max_length=50)
    text = models.TextField()
    parent = models.ForeignKey('self', null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    creator_participant = models.ForeignKey(ForumParticipant)
    forum = models.ForeignKey(
        Forum, on_delete=models.CASCADE, related_name='comments')
    type = models.CharField(
        max_length=255, choices=COMMENT_TYPE_CHOICES, default=OPINION)
    votes = models.IntegerField(default=0)
    deleted = models.BooleanField(default=0)
    locked = models.BooleanField(default=0)


class CommentResource(models.Model):
    """
    Resources associated with a forum, site as a link to research
    paper or yelp search
    """
    comment = models.ForeignKey(Comment, related_name='resources')
    url = models.URLField(max_length=500)
    name = models.CharField(max_length=350)


class CommentData(models.Model):
    """
    k-v store for Comment information. This will
    store things like upvotes or downvotes
    """
    VALID_KEYS = ()

    comment = models.ForeignKey(Comment, related_name='data')
    forum_participant = models.ForeignKey(ForumParticipant)
    key = models.CharField(max_length=255, db_index=True)
    value = models.CharField(max_length=255)
