from rest_framework.serializers import ValidationError
from fruitful_forums.forums.models import Forum

from utils.api.validators import PivotConditionValidator


class ForumTypeValidator(PivotConditionValidator):
    for_field = 'type'

    @classmethod
    def _if(cls, data):
        """
        Checks if PRIVATE FORUM for type passed
        :param data:
        :return: Boolean
        """

        return (
            cls.for_field in data and
            data[cls.for_field] == Forum.PRIVATE_FORUM
        )

    @staticmethod
    def _then(data):
        """
        Validates password passed as well
        :param data: {}
        :return: raise or None
        """

        if not data.get('forum_password'):
            raise ValidationError({
                'forum_password': 'A `forum_password` must be provided if '
                                  'forum type is private'
            })
