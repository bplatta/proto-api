from django.conf.urls import url

from fruitful_forums.forums.views.forums_view import ForumsView
from fruitful_forums.forums.views.forum_participants_view import (
    ForumParticipantsView)
from fruitful_forums.forums.views.forum_resources_view import (
    ForumResourcesView)
from fruitful_forums.forums.views.comments_view import CommentsView

NAMESPACE = 'Forums'

urlpatterns = [

    url(  # Forums collection
        r'^$',
        ForumsView.as_view(),
        name=ForumsView.namespace['collection']
    ),
    url(  # Forums resource
        r'^(?P<forum_id>[0-9a-z-]+)/$',
        ForumsView.as_view(),
        name=ForumsView.namespace['resource']
    ),

    url(  # Forum Participants
        r'^(?P<forum_id>[0-9a-z-]+)/participants/$',
        ForumParticipantsView.as_view(),
        name=ForumParticipantsView.namespace['collection']
    ),
    url(  # Forum Participant
        r'^(?P<forum_id>[0-9a-z-]+)/participants/'
        r'(?P<participant_number>\d+)/$',
        ForumParticipantsView.as_view(),
        name=ForumParticipantsView.namespace['resource']
    ),

    url(  # Comments collection
        r'^(?P<forum_id>[0-9a-z-]+)/comments/$',
        CommentsView.as_view(),
        name=CommentsView.namespace['collection']
    ),
    url(  # Comments resource
        r'^(?P<forum_id>[0-9a-z-]+)/comments/(?P<comment_id>\d+)/$',
        CommentsView.as_view(),
        name=CommentsView.namespace['resource']
    ),

    # MVP first - then I'll add these features in
    #
    # url(  # Forum Data
    #     r'^(?P<forum_id>[0-9a-z-]+)/data/$',
    #     ForumDataView.as_view(),
    #     name=ForumDataView.namespace['collection']
    # ),
    # url(  # Forum Data
    #     r'^(?P<forum_id>[0-9a-z-]+)/data/(?P<forum_data_id>\d+)/$',
    #     ForumDataView.as_view(),
    #     name=ForumDataView.namespace['resource']
    # ),

    url(  # Forum Resources
        r'^(?P<forum_id>[0-9a-z-]+)/resources/$',
        ForumResourcesView.as_view(),
        name=ForumResourcesView.namespace['collection']
    ),
    url(  # Forum Resource
        r'^(?P<forum_id>[0-9a-z-]+)/resources/(?P<resource_number>\d+)/$',
        ForumResourcesView.as_view(),
        name=ForumResourcesView.namespace['resource']
    )
]
