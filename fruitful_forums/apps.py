from django.apps import AppConfig


class FruitfulforumsConfig(AppConfig):
    name = 'fruitful_forums'
    label = 'fruitful_forums'
