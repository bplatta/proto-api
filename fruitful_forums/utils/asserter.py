from django.contrib.auth.hashers import check_password
from fruitful_forums.forums.models import Forum


class TestAssertionHelper(object):

    @staticmethod
    def assert_list_forum_data_structure(
            test_case, response_data, min_expected_count=None):
        """
        Asserts on the data structure of the list of forums returned
        :param test_case: unittest.TestCase instance
        :param min_expected_count: int or None
        :param response_data: {}
        """

        expected_fields = [
            'created_at', 'topic', 'goal', 'description', 'created_by_name',
            'max_participants', 'type', 'url', 'id', 'tags',
            'link', 'created_by_user_id'
        ]

        test_case.assertIn('count', response_data)
        test_case.assertIn('forums', response_data)

        if min_expected_count is not None:
            test_case.assertGreaterEqual(
                response_data['count'], min_expected_count)

        for forum in response_data['forums']:
            for field in expected_fields:
                test_case.assertIn(field, forum)

    #
    # Mappers that correspond to the relevant serializers
    #
    @staticmethod
    def map_participant_data(participant):
        return {
            'name': (
                participant['user'].get_full_name()
                if 'user' in participant else participant['name'])
        }

    @staticmethod
    def map_comment_data(comment):
        return comment

    @staticmethod
    def map_resource_data(resource):
        return resource

    @staticmethod
    def map_forum_kv_data(kv_data):
        return kv_data

    @staticmethod
    def assert_read_forum(
            test_case, forum_data, participant_data=[], comment_data=[],
            resources_data=[], kv_data=[]):
        """
        Build function that asserts on the read data of a forum
        :param test_case: unittest.TestCase instance
        :param forum_data: {}
        :param participant_data: <dict>[]
        :param comment_data: <dict>[]
        :param resources_data: <dict>[]
        :param kv_data: <dict>[]
        :return: function (response.data) => assertions
        """

        direct_comparison_fields = [
            'description', 'topic', 'type', 'max_participants', 'goal']

        def check(response_data):

            for f in direct_comparison_fields:
                test_case.assertEqual(response_data[f], forum_data[f])

            test_case.assertEqual(response_data['id'], forum_data['uid'])
            test_case.assertEqual(
                response_data['link'], '/forums/%s' % forum_data['uid'])
            test_case.assertEqual(
                response_data['tags'], forum_data.get('tags', []))

            # assert forum data contains all expected participants
            forum_participants = [
                p['name'] for p in response_data['participants']]
            for participant in participant_data:
                if 'name' not in participant:
                    test_case.assertIn(
                        participant['user'].get_full_name(),
                        forum_participants)
                else:
                    test_case.assertIn(participant['name'], forum_participants)

        return check

    @staticmethod
    def assert_post_forum(test_case, forum_data):
        """
        Creates assertion method for posting new forum
        :param test_case: unittest instance\
        :param forum_data: dict
        :return: func
        """
        def check(new_forum_obj, participant=None):
            test_case.assertEqual(forum_data['topic'], new_forum_obj.topic)
            test_case.assertEqual(forum_data['goal'], new_forum_obj.goal)
            test_case.assertEqual(forum_data['type'], new_forum_obj.type)
            test_case.assertEqual(
                forum_data.get('tags', []), new_forum_obj.tags)
            test_case.assertEqual(
                forum_data['max_participants'], new_forum_obj.max_participants)
            test_case.assertEqual(
                forum_data['description'], new_forum_obj.description)

            if forum_data['type'] == Forum.PRIVATE_FORUM:
                test_case.assertTrue(
                    check_password(
                        forum_data['forum_password'],
                        new_forum_obj.password
                    )
                )

            if 'email' in forum_data and participant:
                test_case.assertEqual(forum_data['email'], participant.email)
                test_case.assertEqual(
                    forum_data['name'], participant.get_name())
                test_case.assertTrue(
                    check_password(
                        forum_data['user_password'],
                        participant.password
                    )
                )

            if 'resources' in forum_data:
                test_case.assertEqual(
                    len(forum_data['resources']),
                    new_forum_obj.forum_resources.count()
                )
                resources = [
                    (r['name'], r['url'], r['summary'])
                    for r in forum_data['resources']]
                created_resources = [
                    (r.name, r.url, r.summary)
                    for r in new_forum_obj.forum_resources.all()]
                for resource in resources:
                    test_case.assertIn(resource, created_resources)

        return check
