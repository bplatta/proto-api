import logging
from copy import copy
from django.db import transaction
from django.contrib.auth.hashers import make_password

from fruitful_forums.forums.models import (
    Forum, ForumParticipant, ForumResource)
from utils.testing.utils import Randomizer
from utils.testing.test_data_manager import TestCaseDataManager

logger = logging.getLogger(__name__)


class ForumsDataManager(object):
    """
    TestCase helpers for fruitful forums tests
    """
    randomizer = Randomizer()

    def __init__(self):
        """
        To be used per TestCase cls so they can be run in parallel
        """
        self.data = TestCaseDataManager(cache_settings={'forum': list})

    def cleanup(self):
        logger.info('\n= Cleaning up Forum Data =')
        self.data.cleanup_data()

    def total_forums(self):
        return self.data.count('forum')

    def total_forums__public(self):
        return sum([1 for f in self.data.forum_cache()
                    if f['type'] == Forum.PUBLIC_FORUM])

    ########################################
    # Forum data creation helpers #
    ########################################

    def get_forum(self, uid):
        return Forum.objects.get(uid=uid)

    def get_any_forum(self):
        return self.data.get_forum()

    def assure_forums(self, user, min_count):
        """
        Helper function that creates forums if enough dont exist already
        :param min_count: int
        :return:
        """
        total_public_forums = self.total_forums__public()
        if total_public_forums < min_count:
            self.create_public_forums(user, (min_count - total_public_forums))
        return True

    def create_public_forums(self, user, count):
        for _ in range(count):
            self.create_single_forum__public(user)

    def create_single_forum__public(self, user, overrides={}):
        return self.create_forum(
            self.rand_forum_data(overrides=overrides),
            self.rand_participant_data(user)
        )

    def create_single_forum__private(self, user):
        return self.create_forum(
            self.rand_forum_data(overrides={'type': Forum.PRIVATE_FORUM}),
            self.rand_participant_data(user)
        )

    def rand_participant_data(self, user=None, participant_number=1):

        data = {
            'participant_number': participant_number
        }
        if user:
            data.update({
                'user': user
            })
        else:
            data.update({
                'name': self.randomizer.generate_field(str),
                'email': self.randomizer.generate_field('email'),
                'password': self.randomizer.generate_field(str)
            })

        return data

    def rand_participant_post_data(self):
        """
        Generate random POST args for new participant
        :return: {}
        """
        return {
            'password': self.randomizer.generate_field(str),
            'name': self.randomizer.generate_field(str),
            'email': self.randomizer.generate_field('email')
        }

    def rand_forum_post_data(
            self, anon_user=False, forum_type=Forum.PUBLIC_FORUM,
            with_resources=False):
        """
        Generate random POST request args
        :param anon_user: bool - should add params anonymous forum creator
        :param forum_type: str - type of forum
        :param with_resources: bool - include random resources
        :return: {}
        """
        forum_data = self.rand_forum_data(overrides={
            'type': forum_type,
            'tags': [self.randomizer.generate_field(str)]
        })
        forum_data['url'] = self.randomizer.generate_field('url')

        if 'password' in forum_data:
            forum_data['forum_password'] = forum_data.pop('password')

        if anon_user is True:
            forum_data.update({
                'email': self.randomizer.generate_field('email'),
                'name': self.randomizer.generate_field(str),
                'user_password': self.randomizer.generate_field(str)
            })

        if with_resources:
            # Add a couple resources
            forum_data['resources'] = [
                self.rand_forum_resource_data(resource_number=None),
                self.rand_forum_resource_data(resource_number=None),
            ]

        return forum_data

    def rand_forum_data(self, overrides={}):
        data = {
            'topic': self.randomizer.generate_field(str),
            'goal': self.randomizer.generate_field(str),
            'description': self.randomizer.generate_field(str),
            'type': Forum.PUBLIC_FORUM,
            'url': self.randomizer.generate_field('url'),
            'max_participants': self.randomizer.generate_field(
                int, min_value=50, max_value=100),
            'tags': []
        }

        data.update(overrides)

        if data['type'] == Forum.PRIVATE_FORUM:
            data['password'] = self.randomizer.generate_field(str)

        return data

    @transaction.atomic
    def create_forum(self, forum_data, participant_data):
        forum_password = None
        participant = copy(participant_data)
        if 'password' in participant_data:
            participant['password'] = make_password(
                participant.pop('password'))
        if 'password' in forum_data:
            forum_password = forum_data.pop('password')
            forum_data['password'] = make_password(forum_password)

        creator = ForumParticipant.objects.create(**participant)

        forum_data['creator_participant'] = creator
        new_forum = Forum.objects.create(**forum_data)

        creator.forum = new_forum
        creator.save()

        logger.info('+F')
        self.data.queue_for_delete(new_forum)

        # only set forum in cache if its a public forum
        if forum_data['type'] != 'private':
            self.data.set_forum(forum_data)

        forum_data['obj'] = new_forum
        forum_data['uid'] = new_forum.uid
        participant_data['id'] = creator.id
        # reset forum password
        forum_data['password'] = forum_password
        return forum_data, participant_data

    def rand_forum_resource_data(self, resource_number=1):
        """
        Generate random data for forum resource
        :param resource_number: None or integer
        :return:
        """
        data = {
            'url': self.randomizer.generate_field('url'),
            'name': self.randomizer.generate_field(str),
            'summary': self.randomizer.generate_field(str)
        }
        # All no resource number for post requests
        if resource_number is not None:
            data['resource_number'] = resource_number

        return data

    def create_forum_resource(self, forum_uid_or_forum):
        """
        Create new forum resouce for forum
        :param forum_uid_or_forum: str or Forum object
        :return: {} of resource data created
        """
        forum = (
            forum_uid_or_forum if type(forum_uid_or_forum) is not str
            else self.get_forum(forum_uid_or_forum))

        resource_data = self.rand_forum_resource_data(
            resource_number=(forum.total_resources() + 1))
        resource_data['forum'] = forum
        logger.info('+FR')
        new_resource = ForumResource.objects.create(**resource_data)
        self.data.queue_for_delete(new_resource)
        resource_data['id'] = new_resource.id
        return resource_data

    @transaction.atomic
    def create_forum_participant(self, forum_uid_or_forum, user=None):
        """
        Create a new forum participant for a forum
        :param forum_uid_or_forum: forum UID or forum
        :param user: APIUser object or None
        :return: {}
        """
        forum = (
            forum_uid_or_forum if type(forum_uid_or_forum) is not str
            else self.get_forum(forum_uid_or_forum))

        participant_data = self.rand_participant_data(
            user=user, participant_number=(forum.total_participants() + 1))

        participant = copy(participant_data)
        if 'password' in participant_data:
            participant['password'] = make_password(
                participant.pop('password'))

        participant['forum'] = forum
        logger.info('+FP')
        new_participant = ForumParticipant.objects.create(**participant)
        self.data.queue_for_delete(new_participant)
        participant_data['id'] = new_participant.id
        return participant_data
