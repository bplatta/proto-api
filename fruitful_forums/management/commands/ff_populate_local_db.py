from random import choice

from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command

from fruitful_forums.utils.forums_data_manager import ForumsDataManager
from utils.testing.test_auth_manager import TestCaseAuthManager


class Command(BaseCommand):
    help = 'Populates local database with fruitful forums data'

    def add_arguments(self, parser):

        parser.add_argument(
            '--users',
            dest='num_users',
            default=5,
            help='Number of users to create',
        )

        parser.add_argument(
            '--forums',
            dest='num_forums',
            default=100,
            help='Number of forums to create',
        )

        parser.add_argument(
            '--private',
            dest='num_private_forums',
            default=50,
            help='Number of forums to create',
        )

    def random_user(self, users):
        return choice(list(users.values()))[0]

    def random_tags(self, randomizer):
        def n_random_tags(n):
            return [randomizer.generate_field(str) for _ in range(n)]
        return n_random_tags

    def handle(self, *args, **options):
        """
        Handle creation of local data
        :param args:
        :param options:
        :return:
        """
        auth = TestCaseAuthManager()
        forum_data = ForumsDataManager()
        random_tags = self.random_tags(forum_data.randomizer)

        num_users = options['num_users']
        num_forums = options['num_forums']
        num_private_forums = options['num_private_forums']

        users = {}
        forums = {}

        for i in range(num_users):
            users['%s' % i] = auth.new_user()

        for j in range(num_forums):
            forums['%s' % j] = forum_data.create_single_forum__public(
                self.random_user(users), overrides={
                    'tags': random_tags(2)
                })

        for k in range(num_private_forums):
            forums['p%s' % k] = forum_data.create_single_forum__private(
                self.random_user(users))

        self.stdout.write('%s Users created' % num_users)
        self.stdout.write(
            '%s Forums created' % str(num_forums + num_private_forums))
