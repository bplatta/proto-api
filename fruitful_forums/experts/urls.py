from django.conf.urls import url

from fruitful_forums.experts.views.expert_claims_view import ExpertClaimsView
from fruitful_forums.experts.views.claim_evidence_view import (
    ExpertClaimEvidenceView)

NAMESPACE = 'Experts'

urlpatterns = [
    # Expert claims
    url(r'^$', ExpertClaimsView.as_view(),
        name=ExpertClaimsView.namespace['collection']),
    url(r'^(?P<expert_claim_id>\d+)/$', ExpertClaimsView.as_view(),
        name=ExpertClaimsView.namespace['resource']),

    # Expert Claim Evidence sub-collection
    # url(r'^(?P<expert_claim_id>\d+)/evidence/$',
    #     ExpertClaimEvidenceView.as_view(),
    #     name=ExpertClaimEvidenceView.namespace['collection']),
    # url(r'^(?P<expert_claim_id>\d+)/evidence/'
    #     r'(?P<claim_evidence_id>\d+)/$',
    #     ExpertClaimEvidenceView.as_view(),
    #     name=ExpertClaimEvidenceView.namespace['resource'])

]
