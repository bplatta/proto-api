from utils.api.base_view import APIAllView

from fruitful_forums.experts.models import ExpertClaimEvidence, ExpertClaim
from fruitful_forums.experts.permissions import ExpertClaimEvidencePermissions
from fruitful_forums.experts.serializers.claim_evidence_serializer import (
    ExpertClaimEvidencePOSTSerializer,
    ExpertClaimEvidenceGETSerializer
)
from utils.mixins import SubcollectionMixin


class ExpertClaimEvidenceView(APIAllView, SubcollectionMixin):
    """
    Main view for handling expert claim evidence data
    """
    main_model = ExpertClaimEvidence
    lookup_url_kwarg = 'claim_evidence_id'
    http_method_names = ['get', 'post', 'delete']
    supported_operations = ['read', 'list', 'create', 'delete', 'update']

    parent_lookup_url_kwarg = 'expert_claim_id'
    parent_resource__model = ExpertClaim
    parent_permission_classes = (ExpertClaimEvidencePermissions,)

    namespace = {
        'collection': 'claim-evidences',
        'resource': 'claim-evidence'
    }

    queryset = ExpertClaimEvidence.objects.all()
    serializer_classes = {
        'post': {
            'default': ExpertClaimEvidencePOSTSerializer
        },
        'get': {
            'default': ExpertClaimEvidenceGETSerializer
        }
    }

    def post(self, request, expert_claim_id=None, claim_evidence_id=None):
        return self._post(request, expert_claim_id=expert_claim_id)

    def get(self, request, expert_claim_id=None, claim_evidence_id=None):
        return self._get(request, expert_claim_id=expert_claim_id)

    def delete(self, request, expert_claim_id=None, claim_evidence_id=None):
        return self._delete(request, expert_claim_id=expert_claim_id)
