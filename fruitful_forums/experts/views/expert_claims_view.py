from rest_framework.permissions import IsAuthenticated

from utils.api.base_view import APIAllView

from fruitful_forums.experts.models import ExpertClaim
from fruitful_forums.experts.serializers.expert_claims_serializer import (
    ExpertClaimPOSTSerializer, ExpertClaimGETSerializer)
from fruitful_forums.experts.permissions import ExpertClaimPermissions


class ExpertClaimsView(APIAllView):
    """
    Main view for handling expert claim data
    """
    main_model = ExpertClaim
    lookup_url_kwarg = 'expert_claim_id'
    http_method_names = ['get', 'post', 'delete']
    supported_operations = ['read', 'list', 'create', 'delete']
    permission_classes = (IsAuthenticated, ExpertClaimPermissions)

    namespace = {
        'collection': 'expert-claims',
        'resource': 'expert-claim'
    }

    queryset = ExpertClaim.objects.all()
    serializer_classes = {
        'post': {
            'default': ExpertClaimPOSTSerializer
        },
        'get': {
            'default': ExpertClaimGETSerializer
        }
    }

    def post(self, request, expert_claim_id=None):
        return self._post(request, expert_claim_id=expert_claim_id)

    def get(self, request, expert_claim_id=None):
        return self._get(request, expert_claim_id=expert_claim_id)

    def delete(self, request, expert_claim_id=None):
        return self._delete(request, expert_claim_id=expert_claim_id)
