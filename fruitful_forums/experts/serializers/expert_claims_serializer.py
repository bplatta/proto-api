from rest_framework import serializers

from fruitful_forums.experts.models import ExpertClaim, ExpertClaimEvidence
from utils.exceptions import OperationNotSupported


class ExpertClaimPOSTSerializer(serializers.Serializer):
    """
    Serialize POST requests for expert claims - nice and simple
    """
    expertise = serializers.CharField(max_length=255)

    def validate(self, data):
        """
        Validate and augment with request profile
        :param data: {}
        :return: {}
        """
        data['user'] = self.context['request'].user
        return data

    def create(self, validated_data):
        return ExpertClaim.objects.create(**validated_data)

    def update(self, instance, validated_data):
        raise OperationNotSupported(
            operation='update',
            opt_message='Updates not allowed on expert claims. Can delete '
                        'claim %s and create a new one with a '
                        'POST request' % instance.id
        )


class ExpertClaimGETSerializer(serializers.ModelSerializer):
    """
    Expert claim serializer for GET requests
    """

    class Meta:
        model = ExpertClaim
        fields = '__all__'

    def create(self, validated_data):
        raise OperationNotSupported(operation='create')

    def update(self, instance, validated_data):
        raise OperationNotSupported(operation='update')
