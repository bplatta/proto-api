from rest_framework import serializers

from fruitful_forums.experts.models import ExpertClaimEvidence
from utils.api.base_serializers import BasePostSerializer
from utils.exceptions import OperationNotSupported


class ExpertClaimEvidencePOSTSerializer(BasePostSerializer):
    """
    Serialize POST requests for expert claim evidence - nice and simple
    """
    url = serializers.URLField(max_length=255)
    description = serializers.CharField(max_length=500)

    def validate(self, data):
        """
        Validate and set evidence data
        :return: {}
        """
        data['claim'] = self.get_parent_instance_from_view()
        return data

    def create(self, validated_data):
        return ExpertClaimEvidence.objects.create(**validated_data)

    def update(self, instance, validated_data):
        raise OperationNotSupported(
            operation='update',
            opt_message='Updates not allowed on expert claim evidence. '
                        'Can delete claim %s evidence and create a '
                        'new one with a POST request' % instance.id
        )


class ExpertClaimEvidenceGETSerializer(serializers.ModelSerializer):
    """
    Expert claim evidence serializer for GET requests
    """

    class Meta:
        model = ExpertClaimEvidence
        fields = '__all__'

    def create(self, validated_data):
        raise OperationNotSupported(operation='create')

    def update(self, instance, validated_data):
        raise OperationNotSupported(operation='update')
