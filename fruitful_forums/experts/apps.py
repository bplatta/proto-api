from django.apps import AppConfig


class ExpertsConfig(AppConfig):
    name = 'fruitful_forums.experts'
    label = 'experts'
