from rest_framework.permissions import BasePermission

from utils.api.base_permissions import BaseRequestFacts


class ExpertClaimFacts(BaseRequestFacts):
    """
    Facts about expert claim data given the request
    """
    @property
    def claim(self):
        return self.resource if not self.is_sub_collection else self.parent

    def is_admin(self):
        return self.request.user.is_staff

    def is_claim_owner(self):
        return self.request.user.profile == self.claim.user_profile


class ExpertClaimPermissions(BasePermission):
    """
    Permissions for the Expert claims collection endpoint
    (admin can do all, unauthenticated can do nothing)

    Assumes:
        - authentication happens first

        GET:
            list: auth'ed can list all expert claims
        POST:
            create: auth'ed can create an expert claim
        DELETE:
            delete: only owner can delete an expert claim
    """

    def has_permission(self, request, view):
        """
        Permissions on collection for supported methods
        :param request: request object
        :param view: view class
        :return: boolean
        """
        # Any auth'ed user can List or Create claims
        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on forum resources
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ExpertClaimFacts(request, view, obj)

        if Facts.is_admin():
            return True

        if request.method == 'POST' or request.method == 'DELETE':
            return Facts.is_claim_owner()
        return True


class ExpertClaimEvidencePermissions(BasePermission):
    """
    Permissions for the Expert claim evidence sub collection endpoint
    (admin can do all, unauthenticated can do nothing)

    Assumes:
        - authentication happens first
        - parent_instance (ExpertClaim) set on view by SubCollectionMixin

        GET:
            list: auth'ed can list all expert claim evidence
        POST:
            create: only owner of claim can create new evidence
            update: only owner of claim can update evidence
        DELETE:
            delete: only owner can delete an expert claim evidence
    """

    def has_permission(self, request, view):
        """
        Permissions on collection of claim evidence
        :param request: request object
        :param view: view class
        :return: boolean
        """
        Facts = ExpertClaimFacts(request, view)

        if Facts.is_admin():
            return True

        if request.method == 'GET':
            return True
        elif request.method == 'POST':
            return Facts.is_claim_owner()
        return True

    def has_object_permission(self, request, view, obj):
        """
        Permissions on expert claim evidence
        :param request: request object
        :param view: view class
        :param obj: forum object
        :return: boolean
        """
        Facts = ExpertClaimFacts(request, view, obj)

        if Facts.is_admin():
            return True

        if request.method == 'POST' or request.method == 'DELETE':
            return Facts.is_claim_owner()
        return True
