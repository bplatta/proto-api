from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from users.models import APIUser


################
# Experts Data #
################

class ExpertClaim(models.Model):
    """
    Model for claims made by the user
    """
    user = models.ForeignKey(APIUser)
    expertise = models.CharField(max_length=255)
    admin_verified = models.BooleanField(default=False)
    likelihood = models.IntegerField(default=0, validators=[
        MinValueValidator(0),
        MaxValueValidator(100)
    ])


class ExpertClaimEvidence(models.Model):
    """
    Model for Evidence to subtantiate UserClaim's
    """
    claim = models.ForeignKey(ExpertClaim)
    url = models.URLField(max_length=255)
    description = models.CharField(max_length=500)
